
sailor.png
size: 256,128
format: RGBA8888
filter: Linear,Linear
repeat: none
L_arm_low
  rotate: true
  xy: 173, 36
  size: 14, 36
  orig: 16, 38
  offset: 1, 1
  index: -1
L_arm_up
  rotate: false
  xy: 127, 38
  size: 44, 27
  orig: 46, 29
  offset: 1, 1
  index: -1
L_leg_lower
  rotate: true
  xy: 2, 9
  size: 24, 55
  orig: 26, 57
  offset: 1, 1
  index: -1
L_leg_up
  rotate: false
  xy: 70, 67
  size: 45, 59
  orig: 47, 61
  offset: 1, 1
  index: -1
R_arm_low
  rotate: false
  xy: 81, 3
  size: 14, 36
  orig: 16, 38
  offset: 1, 1
  index: -1
R_arm_up
  rotate: false
  xy: 173, 52
  size: 44, 27
  orig: 46, 29
  offset: 1, 1
  index: -1
R_leg_lower
  rotate: true
  xy: 70, 41
  size: 24, 55
  orig: 26, 57
  offset: 1, 1
  index: -1
R_leg_up
  rotate: false
  xy: 117, 67
  size: 45, 59
  orig: 47, 61
  offset: 1, 1
  index: -1
head-back
  rotate: false
  xy: 164, 81
  size: 40, 45
  orig: 42, 47
  offset: 1, 1
  index: -1
head-front
  rotate: false
  xy: 206, 81
  size: 40, 45
  orig: 42, 47
  offset: 1, 1
  index: -1
neck
  rotate: false
  xy: 59, 2
  size: 20, 31
  orig: 22, 33
  offset: 1, 1
  index: -1
shirt
  rotate: false
  xy: 2, 35
  size: 66, 91
  orig: 68, 93
  offset: 1, 1
  index: -1
