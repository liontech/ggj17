// Shader created with Shader Forge v1.32 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.32;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False;n:type:ShaderForge.SFN_Final,id:3138,x:32719,y:32712,varname:node_3138,prsc:2|emission-104-RGB,voffset-2267-OUT;n:type:ShaderForge.SFN_Code,id:2267,x:31797,y:33149,varname:node_2267,prsc:2,code:ZgBsAG8AYQB0ADMAIAByAGkAZwBoAHQAIAA9ACAAZgBsAG8AYQB0ADMAKAAxAC4AMAAsACAAMAAuADAALAAgADAALgAwACkAOwAKAGYAbABvAGEAdAAzACAAZgBvAHIAdwBhAHIAZAAgAD0AIABmAGwAbwBhAHQAMwAoADAALgAwACwAIAAwAC4AMAAsACAAMQAuADAAKQA7AAoAZgBsAG8AYQB0ADMAIAB1AHAAIAA9ACAAZgBsAG8AYQB0ADMAKAAwAC4AMAAsACAAMQAuADAALAAgADAALgAwACkAOwAKAAoAZgBsAG8AYQB0ACAAaABvAHIAQgBlAG4AZABGAGEAYwB0AG8AcgAgAD0AIAAuADIAIAArACAALgA4ACAAKgAgAFUAVgAuAHgAOwAKAGYAbABvAGEAdAAgAHYAZQByAHQAQgBlAG4AZABGAGEAYwB0AG8AcgAgAD0AIABzAGkAbgAoACgAVQBWAC4AeQAgACoAIAAuADkAKQAgACoAIABQAEkAKQA7AAoACgBmAGwAbwBhAHQAIABiAGUAbgBkAEEAbQBwAGwAaQB0AHUAZABlACAAPQAgAC4ANQAgACoAIABNAGEAcwB0AEwAZQBuAGcAdABoADsACgBmAGwAbwBhAHQAMwAgAGIAZQBuAGQATwBmAGYAcwBlAHQAIAA9ACAAcgBpAGcAaAB0ACAAKgAgAEIAZQBuAGQAIAAqACAAaABvAHIAQgBlAG4AZABGAGEAYwB0AG8AcgAgACoAIAB2AGUAcgB0AEIAZQBuAGQARgBhAGMAdABvAHIAIAAqACAAYgBlAG4AZABBAG0AcABsAGkAdAB1AGQAZQA7AAoACgBmAGwAbwBhAHQAMwAgAGIAbwBvAG0AVgBlAGMAdABvAHIAIAA9ACAAZgBsAG8AYQB0ADMAKAAwAC4AMAAsACAAMAAuADAALAAgAEIAbwBvAG0ATABlAG4AZwB0AGgAKQA7AAoAZgBsAG8AYQB0ADMAIABjAHIAYQBuAGUAVgBlAGMAdABvAHIAIAA9ACAAZgBsAG8AYQB0ADMAKAAwAC4AMAAsACAAMAAuADAALAAgAEMAcgBhAG4AZQBMAGUAbgBnAHQAaAApADsACgBmAGwAbwBhAHQAMwAgAGIAbwBvAG0AQwByAGEAbgBlAE8AZgBmAHMAZQB0ACAAPQAgACgAZgBvAHIAdwBhAHIAZAAgAC0AIABsAGUAcgBwACgAYgBvAG8AbQBWAGUAYwB0AG8AcgAsACAAYwByAGEAbgBlAFYAZQBjAHQAbwByACwAIABVAFYALgB5ACkAKQAgACoAIABVAFYALgB4ADsACgAKAGYAbABvAGEAdAAzACAAbQBhAHMAdABWAGUAYwB0AG8AcgAgAD0AIABmAGwAbwBhAHQAMwAoADAALgAwACwAIABNAGEAcwB0AEwAZQBuAGcAdABoACwAIAAwAC4AMAApADsACgBmAGwAbwBhAHQAMwAgAGgAbwBpAHMAdABPAGYAZgBzAGUAdAAgAD0AIAAtAHUAcAAgACoAIABVAFYALgB5ACAAKwAgACgALgAxACAAKwAgAC4AOQAgACoAIABIAG8AaQBzAHQAKQAgACoAIABtAGEAcwB0AFYAZQBjAHQAbwByACAAKgAgAFUAVgAuAHkAOwAKAGYAbABvAGEAdAAgAGgAbwBpAHMAdABGAG8AbABkAEMAbwB1AG4AdAAgAD0AIAAzAC4AMAA7AAoAZgBsAG8AYQB0ACAAaABvAGkAcwB0AEYAbwBsAGQAQQBtAHAAbABpAHQAdQBkAGUAIAA9ACAAIAAuADAANgAgACoAIABNAGEAcwB0AEwAZQBuAGcAdABoADsACgBmAGwAbwBhAHQAMwAgAGgAbwBpAHMAdABGAG8AbABkAE8AZgBmAHMAZQB0ACAAPQAgAHIAaQBnAGgAdAAgACoAIABzAGkAbgAoAFUAVgAuAHkAIAAqACAAUABJACAAKgAgAGgAbwBpAHMAdABGAG8AbABkAEMAbwB1AG4AdAApACAAKgAgACgAMQAuADAAIAAtACAASABvAGkAcwB0ACkAIAAqACAAaABvAGkAcwB0AEYAbwBsAGQAQQBtAHAAbABpAHQAdQBkAGUAOwAKAAoAZgBsAG8AYQB0ACAAdwBpAG4AZABBAG0AcABsAGkAdAB1AGQAZQAgAD0AIAAuADAANQAgACoAIABNAGEAcwB0AEwAZQBuAGcAdABoADsACgBmAGwAbwBhAHQAIAB3AGkAbgBkAEIAZQBuAGQAQwBvAHUAbgB0ACAAPQAgADIALgAwADsACgBmAGwAbwBhAHQAIAB3AGkAbgBkAFMAcABlAGUAZAAgAD0AIAAyAC4ANQA7AAoAZgBsAG8AYQB0ADMAIAB3AGkAbgBkAE8AZgBmAHMAZQB0ACAAPQAgAHMAaQBuACgAVQBWAC4AeQAgACoAIABQAEkAKQAgACoAIAByAGkAZwBoAHQAIAAqACAACgAJACAAIAAgACAAcwBpAG4AKABVAFYALgB5ACAAKgAgAFAASQAgACoAIAB3AGkAbgBkAEIAZQBuAGQAQwBvAHUAbgB0ACAAKwAgAFQAaQBtAGUAIAAqACAAdwBpAG4AZABTAHAAZQBlAGQAIAArACAAVQBWAC4AeAAgACoAIABQAEkAIAAqACAAMgApACAAKgAgAHcAaQBuAGQAQQBtAHAAbABpAHQAdQBkAGUAIAAqAAoACQAgACAAIAAgACgAMQAgAC0AIABhAGIAcwAoAEIAZQBuAGQAKQApACAAKgAgAEgAbwBpAHMAdAA7AAoACgAKAGYAbABvAGEAdAAzACAAdABvAHQAYQBsAE8AZgBmAHMAZQB0ACAAPQAgAGIAZQBuAGQATwBmAGYAcwBlAHQAIAArACAAYgBvAG8AbQBDAHIAYQBuAGUATwBmAGYAcwBlAHQAIAArACAAaABvAGkAcwB0AE8AZgBmAHMAZQB0ACAAKwAgAGgAbwBpAHMAdABGAG8AbABkAE8AZgBmAHMAZQB0ACAAKwAgAHcAaQBuAGQATwBmAGYAcwBlAHQAOwAKAAoAcgBlAHQAdQByAG4AIAB0AG8AdABhAGwATwBmAGYAcwBlAHQAOwA=,output:2,fname:VertexOffset,width:762,height:526,input:0,input:0,input:0,input:0,input:0,input:0,input:1,input:0,input_1_label:Bend,input_2_label:Hoist,input_3_label:MastLength,input_4_label:CraneLength,input_5_label:BoomLength,input_6_label:Time,input_7_label:UV,input_8_label:PI|A-7091-OUT,B-2375-OUT,C-7249-OUT,D-7791-OUT,E-7578-OUT,F-5018-T,G-2380-UVOUT,H-5753-OUT;n:type:ShaderForge.SFN_TexCoord,id:2380,x:31517,y:33682,varname:node_2380,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:7249,x:31517,y:33296,ptovrint:False,ptlb:MastLength,ptin:_MastLength,varname:node_7249,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Slider,id:7091,x:31360,y:33106,ptovrint:False,ptlb:Bend,ptin:_Bend,varname:node_7091,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-1,cur:1,max:1;n:type:ShaderForge.SFN_Slider,id:2375,x:31360,y:33202,ptovrint:False,ptlb:Hoist,ptin:_Hoist,varname:node_2375,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Pi,id:5753,x:31550,y:33827,varname:node_5753,prsc:2;n:type:ShaderForge.SFN_Time,id:5018,x:31517,y:33554,varname:node_5018,prsc:2;n:type:ShaderForge.SFN_ValueProperty,id:7791,x:31517,y:33392,ptovrint:False,ptlb:CraneLength,ptin:_CraneLength,varname:node_7791,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:0.3;n:type:ShaderForge.SFN_ValueProperty,id:7578,x:31517,y:33477,ptovrint:False,ptlb:BoomLength,ptin:_BoomLength,varname:node_7578,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Tex2d,id:104,x:32164,y:32707,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_104,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:f37b343f13dbe4e26945ad425c1f07ea,ntxv:0,isnm:False;proporder:7249-7091-2375-7791-7578-104;pass:END;sub:END;*/

Shader "Fishy Oceans/Sail" {
    Properties {
        _MastLength ("MastLength", Float ) = 1
        _Bend ("Bend", Range(-1, 1)) = 1
        _Hoist ("Hoist", Range(0, 1)) = 1
        _CraneLength ("CraneLength", Float ) = 0.3
        _BoomLength ("BoomLength", Float ) = 1
        _Texture ("Texture", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            float3 VertexOffset( float Bend , float Hoist , float MastLength , float CraneLength , float BoomLength , float Time , float2 UV , float PI ){
            float3 right = float3(1.0, 0.0, 0.0);
            float3 forward = float3(0.0, 0.0, 1.0);
            float3 up = float3(0.0, 1.0, 0.0);
            
            float horBendFactor = .2 + .8 * UV.x;
            float vertBendFactor = sin((UV.y * .9) * PI);
            
            float bendAmplitude = .5 * MastLength;
            float3 bendOffset = right * Bend * horBendFactor * vertBendFactor * bendAmplitude;
            
            float3 boomVector = float3(0.0, 0.0, BoomLength);
            float3 craneVector = float3(0.0, 0.0, CraneLength);
            float3 boomCraneOffset = (forward - lerp(boomVector, craneVector, UV.y)) * UV.x;
            
            float3 mastVector = float3(0.0, MastLength, 0.0);
            float3 hoistOffset = -up * UV.y + (.1 + .9 * Hoist) * mastVector * UV.y;
            float hoistFoldCount = 3.0;
            float hoistFoldAmplitude =  .06 * MastLength;
            float3 hoistFoldOffset = right * sin(UV.y * PI * hoistFoldCount) * (1.0 - Hoist) * hoistFoldAmplitude;
            
            float windAmplitude = .05 * MastLength;
            float windBendCount = 2.0;
            float windSpeed = 2.5;
            float3 windOffset = sin(UV.y * PI) * right * 
            	    sin(UV.y * PI * windBendCount + Time * windSpeed + UV.x * PI * 2) * windAmplitude *
            	    (1 - abs(Bend)) * Hoist;
            
            
            float3 totalOffset = bendOffset + boomCraneOffset + hoistOffset + hoistFoldOffset + windOffset;
            
            return totalOffset;
            }
            
            uniform float _MastLength;
            uniform float _Bend;
            uniform float _Hoist;
            uniform float _CraneLength;
            uniform float _BoomLength;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_5018 = _Time + _TimeEditor;
                v.vertex.xyz += VertexOffset( _Bend , _Hoist , _MastLength , _CraneLength , _BoomLength , node_5018.g , o.uv0 , 3.141592654 );
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float4 _Texture_var = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 emissive = _Texture_var.rgb;
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma only_renderers d3d9 d3d11 glcore gles n3ds wiiu 
            #pragma target 3.0
            uniform float4 _TimeEditor;
            float3 VertexOffset( float Bend , float Hoist , float MastLength , float CraneLength , float BoomLength , float Time , float2 UV , float PI ){
            float3 right = float3(1.0, 0.0, 0.0);
            float3 forward = float3(0.0, 0.0, 1.0);
            float3 up = float3(0.0, 1.0, 0.0);
            
            float horBendFactor = .2 + .8 * UV.x;
            float vertBendFactor = sin((UV.y * .9) * PI);
            
            float bendAmplitude = .5 * MastLength;
            float3 bendOffset = right * Bend * horBendFactor * vertBendFactor * bendAmplitude;
            
            float3 boomVector = float3(0.0, 0.0, BoomLength);
            float3 craneVector = float3(0.0, 0.0, CraneLength);
            float3 boomCraneOffset = (forward - lerp(boomVector, craneVector, UV.y)) * UV.x;
            
            float3 mastVector = float3(0.0, MastLength, 0.0);
            float3 hoistOffset = -up * UV.y + (.1 + .9 * Hoist) * mastVector * UV.y;
            float hoistFoldCount = 3.0;
            float hoistFoldAmplitude =  .06 * MastLength;
            float3 hoistFoldOffset = right * sin(UV.y * PI * hoistFoldCount) * (1.0 - Hoist) * hoistFoldAmplitude;
            
            float windAmplitude = .05 * MastLength;
            float windBendCount = 2.0;
            float windSpeed = 2.5;
            float3 windOffset = sin(UV.y * PI) * right * 
            	    sin(UV.y * PI * windBendCount + Time * windSpeed + UV.x * PI * 2) * windAmplitude *
            	    (1 - abs(Bend)) * Hoist;
            
            
            float3 totalOffset = bendOffset + boomCraneOffset + hoistOffset + hoistFoldOffset + windOffset;
            
            return totalOffset;
            }
            
            uniform float _MastLength;
            uniform float _Bend;
            uniform float _Hoist;
            uniform float _CraneLength;
            uniform float _BoomLength;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float4 node_5018 = _Time + _TimeEditor;
                v.vertex.xyz += VertexOffset( _Bend , _Hoist , _MastLength , _CraneLength , _BoomLength , node_5018.g , o.uv0 , 3.141592654 );
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
