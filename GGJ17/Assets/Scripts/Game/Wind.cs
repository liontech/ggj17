﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wind : MonoBehaviour {

    public Vector3 Direction { get; private set; }

    [SerializeField] private float directionChangeInterval;

    private Vector3 targetDirection;

    private IEnumerator Start() {
        while (true) {
            targetDirection = MathHelper.RandomVector3().Flattened().normalized;
            yield return new WaitForSeconds(directionChangeInterval);
        }
    }

    private void Update() {
        Direction = Vector3.Lerp(Direction, targetDirection, .01f);
        transform.forward = Direction;
    }

}
