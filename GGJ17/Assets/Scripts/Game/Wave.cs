﻿using DG.Tweening;
using IoCPlus.Internal;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wave : MonoBehaviour {

    public Action<Wave> OnDestroyCallback;
    public bool HasTriggeredBoat;

    [Header("Shape")]
    [SerializeField] private AnimationCurve xCurve;
    [SerializeField] private AnimationCurve zCurve;
    [SerializeField] private AnimationCurve breakerCurve;
    [SerializeField] private float width;
    [SerializeField] private float length;
    [SerializeField] private float height;
    [SerializeField] private float breakerOffset;

    [Header("Behaviour")]
    [SerializeField] private float speed;
    [SerializeField] private float startDistance;
    [SerializeField] private float breakInFrontOfPlayerOffset;

    private List<Breaker> breakerPrefabs;

    private float heightFactor;

    private float age;
    private float duration;

    public float GetAltitude(Vector3 position) {
        Vector3 localPosition = transform.InverseTransformPoint(position.Flattened());

        if (Mathf.Abs(localPosition.x) > 1.0f * width) { return 0.0f; }
        if (Mathf.Abs(localPosition.z) > 1.0f * length) { return 0.0f; }
        
        float forwardProgress = (localPosition.z + .5f * length) / length;
        float sideProgress = (localPosition.x + .5f * width) / width;

        return GetNormalizedAltitude(forwardProgress, sideProgress) * height;
    }

    private void Awake() {
        breakerPrefabs = new List<Breaker>(Resources.LoadAll<Breaker>("Breakers"));
        InitiateMovement();
        StartCoroutine("SpawnBreakersOverTime");
    }

    private void InitiateMovement() {
        Boat boat = FindObjectOfType<Boat>();
        Vector3 boatPosition = boat.transform.position;

        transform.position = boatPosition + new Vector3(UnityEngine.Random.Range(-2.0f, 2.0f), 0.0f, 1.0f).normalized * startDistance + breakInFrontOfPlayerOffset * Vector3.forward;

        Vector3 myPosition = transform.position;
        Vector3 targetPosition = boatPosition + (boatPosition - myPosition).Flattened() * .2f + breakInFrontOfPlayerOffset * Vector3.forward;

        transform.forward = myPosition - boatPosition;

        float distance = Vector3.Distance(myPosition, targetPosition);
        duration = distance / speed;

        transform.DOMove(targetPosition, duration)
                 .SetEase(Ease.InOutSine)
                 .OnComplete(() => {
                     Destroy(gameObject);
                 });
    }

    private void OnDestroy() {
        StopAllCoroutines();
        if (OnDestroyCallback != null) {
            OnDestroyCallback(this);
        }
    }

    private void Update() {
        age += Time.deltaTime;
        if (age / duration < .5f) {
            heightFactor = EasingHelper.EaseInOutSine(age / (duration * .5f));
        } else {

            heightFactor = 1.0f - EasingHelper.EaseInOutSine((age - duration * .5f) / (duration * .5f));
        }
    }

    private IEnumerator SpawnBreakersOverTime() {
        while (true) {
            int subDivs = Mathf.CeilToInt(width * .2f);
            for (int x = 0; x < subDivs; x++) {
                for (int z = 0; z < subDivs; z++) {
                    float xProgress = (float)x / subDivs;
                    float zProgress = (float)z / subDivs;
                    float normalizedAltitude = GetNormalizedAltitude(zProgress, xProgress);
                    if (normalizedAltitude + breakerCurve.Evaluate(age / duration) > 1.0f) {
                        if (UnityEngine.Random.value < .2f) {
                            Breaker breaker = Instantiate(breakerPrefabs.GetRandom(), transform);

                            xProgress += .5f * UnityEngine.Random.Range(-1.0f / subDivs, 1.0f / subDivs);
                            zProgress += .5f * UnityEngine.Random.Range(-1.0f / subDivs, 1.0f / subDivs);
                            normalizedAltitude = GetNormalizedAltitude(zProgress, xProgress);

                            float xPosition = width * (-.5f + xProgress);
                            float zPosition = length * (-.5f + zProgress) + breakerOffset;
                            float yPosition = normalizedAltitude * height;

                            breaker.transform.localPosition = new Vector3(xPosition, yPosition, zPosition);
                            breaker.GetComponent<Drifter>().Drift();
                        }
                    }
                }
            }
            yield return new WaitForSeconds(UnityEngine.Random.Range(0.1f, .2f));
        }
    }

    private float GetNormalizedAltitude(float forwardProgress, float sideProgress) {
        float xAltitude = xCurve.Evaluate(sideProgress);
        float zAltitude = zCurve.Evaluate(forwardProgress);
        float altitude = xAltitude * zAltitude * heightFactor;

        return altitude;
    }

}
