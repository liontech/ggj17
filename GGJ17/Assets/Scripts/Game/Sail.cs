﻿using UnityEngine;

public class Sail : MonoBehaviour {

    [SerializeField] private Renderer sailRenderer;

	public void SetBend(float bend) {
        sailRenderer.material.SetFloat("_Bend", bend);
    }

}
