﻿using UnityEngine;

public class Steer : MonoBehaviour {

	[SerializeField] private Boat boat;

    [SerializeField] private float maxRotation;

    private void Update() {
        float rotation = -boat.SteerFactor * maxRotation;
        transform.localRotation = Quaternion.Euler(0.0f, rotation, 0.0f);

    }

}
