﻿using System;
using UnityEngine;

public class Boat : MonoBehaviour {

    public float SteerFactor { get { return steerFactor; } }

    [Header("Steering")]
    [SerializeField] private float steerAccelerationSpeed;
    [SerializeField] private float steerDeccelerationSpeed;
    [SerializeField] private float steerSpeedMax;
    [SerializeField] private float steerRotationMax;

    [Header("Sailing")]
    [SerializeField] private float speedMin;
    [SerializeField] private float speedMax;

    [Header("Balancing")]
    [SerializeField] private float breakerDegreesSafeZone;

    [Header("References")]
    [SerializeField] private Transform steerTransform;
    [SerializeField] private Boom boom;
    [SerializeField] private Rope rope;
    [SerializeField] private Capsizer capsizer;
    [SerializeField] private TriggerDetector breakerDetector;

    private float steerFactor;
    private bool steeredLastFrame;

    private float steerRotation;

	public void Steer(float steer) {
        steerFactor += steer * Time.deltaTime * steerAccelerationSpeed;
        steerFactor = Mathf.Clamp(steerFactor, -1.0f, 1.0f);
        if (Mathf.Abs(steer) > .1f) {
            steeredLastFrame = true;
        }
    }

    public void PullRope() {
        rope.Pull();
    }

    public void Capsize(bool toRight) {
        capsizer.Capsize(toRight);
    }

    private void Awake() {
        breakerDetector.OnTrigger += OnBreakerTrigger;
    }

    private void Update() {
        UpdateSteering();
        UpdateMovement();
    }

    private void UpdateMovement() {
        float speed = Mathf.Lerp(speedMin, speedMax, Mathf.Abs(boom.Power));
        transform.Translate(steerTransform.forward.Flattened().normalized * speed * Time.deltaTime);
    }

    private void UpdateSteering() {
        if (!steeredLastFrame) {
            steerFactor = Mathf.MoveTowards(steerFactor, 0.0f, Time.deltaTime * steerDeccelerationSpeed);
        }
        steeredLastFrame = false;
        steerRotation += Time.deltaTime * steerFactor * steerSpeedMax;
        steerRotation = Mathf.Clamp(steerRotation, -steerRotationMax, steerRotationMax);
        steerTransform.localRotation = Quaternion.Euler(0.0f, steerRotation, 0.0f);
    }

    private void OnBreakerTrigger(GameObject breakerObject) {
        Wave wave = breakerObject.GetComponentInParent<Wave>();
        if (wave.HasTriggeredBoat) { return; }
        wave.HasTriggeredBoat = true;

        float degrees = 90.0f * (Vector3.Dot(wave.transform.forward, -steerTransform.forward) + 1.0f);
        if (degrees < breakerDegreesSafeZone) { return; }

        bool isRight = Vector3.Cross(wave.transform.forward, (-steerTransform.forward).Flattened()).y < 0.0f;

        capsizer.Capsize(isRight);
    }

}
