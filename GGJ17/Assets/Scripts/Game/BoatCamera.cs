﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatCamera : MonoBehaviour {
    
    [SerializeField] private Transform boatDrifter;

    private float offsetY;

    private void Awake() {
        offsetY = transform.position.y - boatDrifter.position.y;
    }

	private void Update () {
        Vector3 position = transform.position;
        position.y = boatDrifter.position.y + offsetY;
        transform.position = Vector3.Lerp(transform.position, position, .1f);
    }
}
