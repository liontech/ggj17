﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Sea : MonoBehaviour {

    [Header("Mesh")]
    [SerializeField] private int subdivisionCount;
    [SerializeField] private float scale;

    [Header("Waves")]
    [SerializeField] private float normalWavesHeight;
    [SerializeField] private float normalWavesLength;

    [Header("References")]
    [SerializeField] private MeshFilter meshFilter;
    [SerializeField] private MeshCollider meshCollider;
    [SerializeField] private Storm storm;
    [SerializeField] private Boat boat;

    private Vector3[] vertices;

    public float GetAltitudeAtPosition(float x, float z) {
        float normalWave = normalWavesHeight * (Mathf.Sin(x / normalWavesLength + Time.timeSinceLevelLoad) + Mathf.Cos(z / normalWavesLength + Time.timeSinceLevelLoad));

        float wavesAltitude = 0.0f;
        foreach (Wave wave in storm.Waves) {
            wavesAltitude += wave.GetAltitude(new Vector3(x, 0.0f, z));
        }

        return normalWave + wavesAltitude;
    }

    public Quaternion GetRotationAtPosition(Transform transform, float refOffset = .01f) {

        Vector3 position = transform.position;
        Vector3 transForward = transform.forward;
        Vector3 transRight = transform.right;

        Vector3 left = position - transRight * refOffset;
        Vector3 right = position + transRight * refOffset;
        Vector3 forward = position + transForward * refOffset;
        Vector3 back = position - transForward * refOffset;

        left = new Vector3(left.x, GetAltitudeAtPosition(left.x, left.z), left.z);
        right = new Vector3(right.x, GetAltitudeAtPosition(right.x, right.z), right.z);
        forward = new Vector3(forward.x, GetAltitudeAtPosition(forward.x, forward.z), forward.z);
        back = new Vector3(back.x, GetAltitudeAtPosition(back.x, back.z), back.z);

        Vector3 horizontal = Vector3.Cross((right - left).normalized, transForward);
        Vector3 vertical = Vector3.Cross((forward - back).normalized, transRight);

        float rotationX = Mathf.Atan2(vertical.z, 1.0f) * Mathf.Rad2Deg;
        float rotationZ = Mathf.Atan2(horizontal.x, 1.0f) * Mathf.Rad2Deg;

        Quaternion rotation = Quaternion.Euler(rotationX, 0.0f, rotationZ);
        return rotation;
    }

    private void Awake() {
        GenerateMesh();
    }

    private void Update() {
        FollowBoat();
        UpdateMesh();
    }

    private void FollowBoat() {
        float subDivLength = scale / subdivisionCount;
        while (boat.transform.position.z > transform.position.z + subDivLength) {
            transform.Translate(Vector3.forward * subDivLength);
        }
        while (boat.transform.position.x > transform.position.x + subDivLength) {
            transform.Translate(Vector3.right * subDivLength);
        }
        while (boat.transform.position.x < transform.position.x - subDivLength) {
            transform.Translate(Vector3.left * subDivLength);
        }
    }

    private void UpdateMesh() {
        Mesh mesh = meshFilter.mesh;
        
        for (int x = 0; x <= subdivisionCount; x++) {
            for (int z = 0; z <= subdivisionCount; z++) {
                int index = z + x * subdivisionCount;
                vertices[index].y = GetAltitudeAtPosition(vertices[index].x + transform.position.x, vertices[index].z + transform.position.z);
            }
        }

        mesh.vertices = vertices;
        mesh.RecalculateNormals();

        meshCollider.sharedMesh = mesh;
    }

    private void GenerateMesh() {
        meshFilter.mesh = new Mesh();
        Mesh mesh = meshFilter.mesh;

        List<Vector3> vertices = new List<Vector3>();

        Vector3 bottomLeft = new Vector3(-scale * .5f, 0.0f, -scale * .5f);
        float subdivisionLength = scale / subdivisionCount;

        for (int x = 0; x <= subdivisionCount; x++) {
            for (int z = 0; z <= subdivisionCount; z++) {

                Vector3 vertex = bottomLeft + subdivisionLength * new Vector3(x, 0.0f, z);
                vertices.Add(vertex);

            }
        }

        List<int> triangles = new List<int>();

        for (int x = 0; x < subdivisionCount; x++) {
            for (int z = 0; z < subdivisionCount; z++) {
                int baseIndex = x + z * (subdivisionCount + 1);
                int lowLeft = baseIndex;
                int lowRight = baseIndex + 1;
                int topLeft = baseIndex + (subdivisionCount + 1);
                int topRight = baseIndex + (subdivisionCount + 1) + 1;

                triangles.Add(lowLeft);
                triangles.Add(lowRight);
                triangles.Add(topLeft);

                triangles.Add(lowRight);
                triangles.Add(topRight);
                triangles.Add(topLeft);
            }
        }

        mesh.SetVertices(vertices);
        mesh.triangles = triangles.ToArray();

        this.vertices = vertices.ToArray();
        meshCollider.sharedMesh = mesh;
    }

}
