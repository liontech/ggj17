﻿using UnityEngine;

public class Sailor : MonoBehaviour {

    private Camera mainCamera;

    private void Awake() {
        mainCamera = FindObjectOfType<Camera>();
    }

	private void Update () {
        transform.LookAt(mainCamera.transform.position, Vector3.up);
    }

}
