﻿using System.Collections.Generic;
using UnityEngine;

public class BoatInput : MonoBehaviour {

    [SerializeField] private List<KeyCode> steerLeftKeys;
    [SerializeField] private List<KeyCode> steerRightKeys;

    [SerializeField] private Boat boat;

    private void Update() {
        if (InputHelper.QueryAnyKeyState(steerLeftKeys, KeyState.Hold)) { boat.Steer(-1.0f); }
        if (InputHelper.QueryAnyKeyState(steerRightKeys, KeyState.Hold)) { boat.Steer(1.0f); }

        boat.Steer(Input.GetAxisRaw("Horizontal"));
        
        if (Input.GetButtonDown("PullRope")) {
            boat.PullRope();
        }
    }

}
