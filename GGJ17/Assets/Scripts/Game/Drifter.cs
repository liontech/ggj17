﻿using UnityEngine;

public class Drifter : MonoBehaviour {

    [SerializeField] private bool updateAltitude;
    [SerializeField] private bool updateRotation;
    [SerializeField] private float driftSize = .2f;

    private Sea sea;

    public void Drift() {
        if (updateAltitude) {
            Vector3 position = transform.position;
            position.y = sea.GetAltitudeAtPosition(position.x, position.z);
            transform.position = position;
        }

        if (updateRotation) {
            transform.localRotation = sea.GetRotationAtPosition(transform.parent, driftSize);
        }
    }

    private void Awake() {
        sea = FindObjectOfType<Sea>();
    }

    private void Update() {
        Drift();
    }
	
}
