﻿using UnityEngine;

public class Boom : MonoBehaviour {

    public float Power { get { return power; } }

    [Header("References")]
    [SerializeField] private Rope rope;
    [SerializeField] private Wind wind;
    [SerializeField] private Sail sail;

    [Header("Balancing")]
    [SerializeField] private float maxRotationSpeed;

    private const float DEFAULT_ROTATION = 90.0f;

    private float power;

    private void Update() {
        power = Vector3.Dot(-transform.forward, wind.Direction);
        sail.SetBend(-power);

        Vector3 euler = transform.localEulerAngles;
        euler.y += power * Time.deltaTime * maxRotationSpeed;
        transform.localRotation = Quaternion.Euler(euler);
        
        while (rope.CurrentLength > rope.TotalLength) {
            if (rope.IsHolding) {
                euler.y = Mathf.MoveTowards(euler.y, DEFAULT_ROTATION, .01f);
                transform.localRotation = Quaternion.Euler(euler);
                if (euler.y == DEFAULT_ROTATION) { break; }
            } else {
                rope.ExtendIfNeeded();
            }
        }
    }

}
