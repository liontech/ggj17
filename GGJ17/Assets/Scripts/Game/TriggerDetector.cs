﻿using System;
using UnityEngine;

public class TriggerDetector : MonoBehaviour {

    public Action<GameObject> OnTrigger;

	private void OnTriggerEnter(Collider collider) {
        if (OnTrigger != null) {
            OnTrigger(collider.gameObject);
        }
    }

}
