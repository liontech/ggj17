﻿using DG.Tweening;
using UnityEngine;

public class Capsizer : MonoBehaviour {

    [SerializeField] private float maxRotation;

	public void Capsize(bool toRight) {
        float capsizeRotation = toRight ? -maxRotation : maxRotation;
        transform.DOKill();
        transform.DOLocalRotate(new Vector3(0.0f, 0.0f, capsizeRotation), 1.0f)
                 .OnComplete(() => {
                     transform.DOLocalRotate(Vector3.zero, 1.0f);
                 });
    }

}
