﻿using DG.Tweening;
using UnityEngine;

public class Breaker : MonoBehaviour {

    [SerializeField] private float duration;
    [SerializeField] private float rotationMax;

    private float age;

	private void Awake() {
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, duration * .5f)
                 .SetEase(Ease.InOutSine)
                 .OnComplete(() => {
                     transform.DOScale(Vector3.zero, duration * .5f)
                    .SetEase(Ease.InOutSine);
                 });

        transform.DOLocalRotate(rotationMax * MathHelper.RandomVector3(), duration)
                 .SetEase(Ease.InOutSine);
    }

    private void Update() {
        age += Time.deltaTime;
        if (age >= duration) {
            transform.DOKill();
            Destroy(gameObject);
        }
    }

    private void OnDestroy() {
        transform.DOKill();
    }
    
}
