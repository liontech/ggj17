﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Storm : MonoBehaviour {

    public IEnumerable<Wave> Waves { get { return waves; } }

    private readonly List<Wave> waves = new List<Wave>();

    [SerializeField] private float intervalMin;
    [SerializeField] private float intervalMax;

    private List<Wave> wavePrefabs;

    private void Awake() {
        wavePrefabs = new List<Wave>(Resources.LoadAll<Wave>("Waves"));
    }

    private IEnumerator Start() {
        while (true) {
            Wave prefab = wavePrefabs.GetRandom();
            Wave wave = Instantiate(prefab);

            wave.OnDestroyCallback += OnWaveDestroy;
            waves.Add(wave);

            float interval = Random.Range(intervalMin, intervalMax);
            yield return new WaitForSeconds(interval);
        }
    }

    private void OnWaveDestroy(Wave wave) {
        waves.RemoveAll(x => x == wave);
    }

}
