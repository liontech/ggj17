﻿using UnityEngine;

public class Rope : MonoBehaviour {

    public float CurrentLength { get { return Vector3.Distance(ropeEnd.position, transform.position); } }
    public bool IsHolding { get; private set; }
    public float TotalLength { get { return totalLength; } }
    public float MaxLength { get { return maxLength; } }

    [SerializeField] private Transform ropeEnd;
    [SerializeField] private Transform visual;
    [SerializeField] private float totalLength;
    [SerializeField] private float minLength;
    [SerializeField] private float maxLength;
    [SerializeField] private float pullSpeed;
    [SerializeField] private float timeBetweenPullTap;

    private float timeSinceLastPull;
    private float targetTotalLength;

    public void ExtendIfNeeded() {
        if (CurrentLength > totalLength) {
            totalLength = CurrentLength;
        }
    }

    public void Pull() {
        if (timeSinceLastPull > timeBetweenPullTap) {
            timeSinceLastPull = 0.0f;
            targetTotalLength = totalLength;
            return;
        }
        targetTotalLength = Mathf.MoveTowards(targetTotalLength, minLength, pullSpeed);
    }

	private void Update() {
        transform.LookAt(ropeEnd);

        if (timeSinceLastPull < timeBetweenPullTap) {
            totalLength = Mathf.MoveTowards(totalLength, targetTotalLength, .05f);
        }

        float distance = Vector3.Distance(ropeEnd.position, transform.position);
        visual.localScale = new Vector3(1.0f, distance, 1.0f);

        IsHolding = Input.GetButton("PullRope") || TotalLength >= MaxLength || timeSinceLastPull < timeBetweenPullTap;

        timeSinceLastPull += Time.deltaTime;
    }

}
