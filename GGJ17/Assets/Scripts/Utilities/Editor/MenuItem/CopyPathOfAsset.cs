﻿using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Craft.Editors.MenuItems {
    public class CopyPathOfAsset  {
        [MenuItem("Assets/Copy Asset Path/Copy Full Path")]
        public static void CopyFullAssetPath() {
            Object selected = Selection.activeObject;
            GUIUtility.systemCopyBuffer = AssetDatabase.GetAssetPath(selected);
        }

        [MenuItem("Assets/Copy Asset Path/Without resources and extension")]
        public static void CopyWithoutResourcesAndExtension() {
            Object selected = Selection.activeObject;
            string path = AssetDatabase.GetAssetPath(selected);
            path = path.Replace("Assets/Resources/", "");
            int indexOfPoint = path.LastIndexOf(".");
            path = path.Remove(indexOfPoint, path.Length - indexOfPoint);
            GUIUtility.systemCopyBuffer = path;
        }
    }

}