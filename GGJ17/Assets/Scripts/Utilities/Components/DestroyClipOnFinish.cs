﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class DestroyClipOnFinish : MonoBehaviour {
    private AudioSource source;

    private void Awake() {
        source = GetComponent<AudioSource>();
    }

    private void Update() {
        if (source.isPlaying) return;
        Destroy(this.gameObject);
    }
}
