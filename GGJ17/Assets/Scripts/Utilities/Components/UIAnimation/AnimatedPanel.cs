﻿﻿using DG.Tweening;
using IoCPlus;
using System;
using UnityEngine;

public class AnimatedPanel : AnimatedWidget {

    public readonly Signal PoppedInSignal = new Signal();
    public readonly Signal PoppedOutSignal = new Signal();

    private const float DURATION = .3f;

    [SerializeField] private bool popInOnAwake = true;

    public void PopIn(Action onPopIn = null) {
        transform.DOKill();
        animatingWidgets.AddDistinct(this);
        transform.localScale = Vector3.zero;
        transform.DOScale(Vector3.one, DURATION)
                 .SetEase(Ease.OutBack)
                 .OnComplete(() => {
                     animatingWidgets.Remove(this);
                     PoppedInSignal.Dispatch();
                     if (onPopIn != null) {
                         onPopIn();
                     }
                 });
    }

    public void PopOut(Action onPopOut = null) {
        transform.DOKill();
        animatingWidgets.AddDistinct(this);
        transform.DOScale(Vector3.zero, DURATION)
                 .SetEase(Ease.InBack)
                 .OnComplete(() => {
                     animatingWidgets.Remove(this);
                     PoppedOutSignal.Dispatch();
                     if (onPopOut != null) {
                         onPopOut();
                     }
                 });
    }

    private void Awake() {
        if (popInOnAwake) {
            PopIn();
        }
    }

}