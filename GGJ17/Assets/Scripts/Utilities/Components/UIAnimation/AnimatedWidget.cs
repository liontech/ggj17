﻿using System.Collections.Generic;
using UnityEngine;

public class AnimatedWidget : MonoBehaviour {

    public static bool IsAnyWidgetAnimating {
        get {
            return animatingWidgets.Count > 0;
        }
    }

    protected readonly static List<AnimatedWidget> animatingWidgets = new List<AnimatedWidget>();

}