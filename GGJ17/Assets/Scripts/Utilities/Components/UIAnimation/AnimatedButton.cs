﻿using DG.Tweening;
using IoCPlus;
using UnityEngine;
using UnityEngine.UI;

public class AnimatedButton : AnimatedWidget {

    public readonly Signal ClickSignal = new Signal();

    private const float SCALE_FACTOR = .1f;
    private const float DURATION = .4f;
    private const int PUNCH_FREQUENCY = 10;
    private const float PUNCH_ELASTICITY = .8f;

    private Button button;

    private void Awake() {
        button = GetComponent<Button>();
        button.onClick.AddListener(OnButtonClick);
    }

    private void OnDestroy() {
        button.onClick.RemoveListener(OnButtonClick);
    }

    private void OnButtonClick() {
        if (IsAnyWidgetAnimating) { return; }
        animatingWidgets.Add(this);
        transform.DOPunchScale(-Vector3.one * SCALE_FACTOR,
                               DURATION, PUNCH_FREQUENCY,
                               PUNCH_ELASTICITY)
                 .OnComplete(() => {
                     animatingWidgets.Remove(this);
                     ClickSignal.Dispatch();
                 });
    }

}