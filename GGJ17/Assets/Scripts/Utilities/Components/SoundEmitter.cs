﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SoundEmitter : MonoBehaviour {

    private readonly Dictionary<string, AudioSource> loopingSounds = new Dictionary<string, AudioSource>();

    public void PlayOnce(AudioSource prefab, bool parent = true) {
        PlaySound(prefab, true,parent);
    }

    public void PlayLoop(AudioSource prefab, string loopName) {
        if (loopingSounds.ContainsKey(loopName)) {
            if (loopingSounds[loopName] != null) {
                return;
            }
        }
        AudioSource sound = PlaySound(prefab,false);
        sound.loop = true;
        loopingSounds.Add(loopName, sound);
    }

    public void StopLoop(string loopName) {
        if (!loopingSounds.ContainsKey(loopName)) { return; }
        if (loopingSounds[loopName] != null) {
            Destroy(loopingSounds[loopName].gameObject);
        }
        loopingSounds.Remove(loopName);
    }

    private AudioSource PlaySound(AudioSource prefab, bool deleteOnFinish = true, bool parent = true) {
        AudioSource source = Instantiate<AudioSource>(prefab);

        if (deleteOnFinish) {
            source.gameObject.AddComponent<DestroyClipOnFinish>();
        }

        if (parent) {
            source.transform.SetParent(transform);
            source.transform.position = Vector3.zero;
        } else {
            source.transform.position = transform.position;
        }
        return source;
    }

}
