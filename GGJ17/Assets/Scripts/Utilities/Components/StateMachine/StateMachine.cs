﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class StateMachine : State {

    protected State currentState { get; private set; }

    [SerializeField] bool stateMachineVerbose;

    List<State> states;

    public void GotoState<T>() where T : State {
        if (currentState != null) {
            currentState.Deactivate();
        }
        currentState = GetOrCreateState<T>();
        currentState.Activate();
        if (stateMachineVerbose) {
            Debug.Log("StateMachine '" + GetType() + "' to State '" + typeof(T) + "'");
        }
    }

    public void GotoState(Type stateType) {
        if (!stateType.IsSubclassOf(typeof(State))) {
            Debug.LogWarning("Can't go to state of type '" + stateType.Name + "' as it is not a subclass of State!");
            return;
        }
        if (currentState != null) {
            currentState.Deactivate();
            currentState = GetOrCreateState(stateType);
        }
        currentState.Activate();
        if (stateMachineVerbose) {
            Debug.Log("StateMachine '" + GetType() + "' to State '" + stateType + "'");
        }
    }

    public void LeaveCurrentState() {
        if (currentState) {
            currentState.Deactivate();
            currentState = null;
        }
    }

    public T GetOrCreateState<T>() where T : State {
        T state = GetState<T>();
        if (state == null) {
            state = AddState<T>();
        }
        return state;
    }

    public State GetOrCreateState(Type stateType) {
        State state = GetState(stateType);
        if (state == null) {
            state = AddState(stateType);
        }
        return state;
    }

    public T AddState<T>() where T : State {
        T state = gameObject.AddComponent<T>();
        state.InitializeState(this);
        return state;
    }

    public State AddState(Type stateType) {
        State state = gameObject.AddComponent(stateType) as State;
        state.InitializeState(this);
        return state;
    }

    public T GetState<T>() where T : State {
        return gameObject.GetComponent<T>();
    }

    public State GetState(Type stateType) {
        return gameObject.GetComponent(stateType) as State;
    }

    public bool HasState<T>() where T : State {
        return states.Find(x => x.GetType() == typeof(T)) != null;
    }

    protected virtual void Awake() {
        states = new List<State>();
    }

}
