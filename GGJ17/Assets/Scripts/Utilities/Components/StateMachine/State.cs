﻿using UnityEngine;
using System.Collections;

public abstract class State : MonoBehaviour {

    protected float timeInState { get; private set; }
    protected StateMachine stateMachine { get; private set; }
    protected bool isActive { get; private set; }

    public void InitializeState(StateMachine stateMachine) {
        this.stateMachine = stateMachine;
    }

    public void Activate() {
        timeInState = 0.0f;
        isActive = true;
        OnActivate();
    }

    public void Deactivate() {
        OnDeactivate();
        isActive = false;
    }

    protected virtual void OnActivate() { }
    protected virtual void OnDeactivate() { }

    protected virtual void OnUpdateActive() { }
    protected virtual void OnUpdateUnactive() { }

    protected virtual void OnFixedUpdateActive() { }
    protected virtual void OnFixedUpdateUnactive() { }

    private void Update() {
        if (isActive) {
            timeInState += Time.deltaTime;
            OnUpdateActive();
        } else {
            OnUpdateUnactive();
        }
    }

    private void FixedUpdate() {
        if (isActive) {
            OnFixedUpdateActive();
        } else {
            OnFixedUpdateUnactive();
        }
    }

}
