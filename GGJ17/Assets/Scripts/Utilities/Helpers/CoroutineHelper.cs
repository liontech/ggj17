﻿using UnityEngine;
using System.Collections;
using System;

public class CoroutineHelper : MonoBehaviour {

    private static CoroutineHelper instance;

    public static void Start(IEnumerator routine) {
        GetInstance().StartLocalCoroutine(routine);
    }

    public static void Delay(Action action, float delay) {
        GetInstance().StartCoroutine("InstanceDelay", new object[] { action, delay });
    }

    public void StartLocalCoroutine(IEnumerator routine) {
        StartCoroutine(routine);
    }

    private static CoroutineHelper GetInstance() {
        if (instance == null) {
            GameObject go = new GameObject("CoroutineHelper");
            instance = go.AddComponent<CoroutineHelper>();
        }
        return instance;
    }

    private IEnumerator InstanceDelay(object[] parameters) {
        Action action = parameters[0] as Action;
        float delay = (float)parameters[1];
        yield return new WaitForSeconds(delay);
        action();
    }

}