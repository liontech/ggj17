using System;
using UnityEngine;

public static class ColorHelper
{
    public static Color HexToColor(string hex)
    {
        hex = hex.ToLower();
        hex = hex.Replace("0x", string.Empty);
        hex = hex.Replace("x", string.Empty);
        hex = hex.Replace("#", string.Empty);

        uint rUint = Convert.ToUInt32(hex.Substring(0, 2), 16);
        uint gUint = Convert.ToUInt32(hex.Substring(2, 2), 16);
        uint bUint = Convert.ToUInt32(hex.Substring(4, 2), 16);

        if (hex.Length > 6)
        {
            uint aUint = Convert.ToUInt32(hex.Substring(6, 2), 16);
            return new Color((float)rUint / 255f, (float)gUint / 255f, (float)bUint / 255f, (float)aUint / 255f);
        }

        return new Color((float)rUint / 255f, (float)gUint / 255f, (float)bUint / 255f);
    }

    public static uint HexToUint(string hex)
    {
        hex = hex.ToLower();
        hex = hex.Replace("0x", string.Empty);
        hex = hex.Replace("x", string.Empty);
        hex = hex.Replace("#", string.Empty);
        return Convert.ToUInt32(hex, 16);
    }
}