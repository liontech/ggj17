﻿using System;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

public static class GameObjectHelper {


    public static GameObject InstantiateAsChild(this GameObject parent, GameObject original, bool setTransform = false) {
        if (original == null) {
            Debug.LogError("Can't instantiate from null", parent);
            return null;
        }
        GameObject g = GameObject.Instantiate(original);
        g.transform.SetParent(parent.transform, setTransform);
        return g;
    }

    public static T InstantiateAsChild<T>(this GameObject parent, GameObject original, bool setTransform = false) where T : Component {
        if (original == null) {
            Debug.LogError("Can't instantiate from null", parent);
            return null;
        }
        GameObject g = parent.InstantiateAsChild(original, setTransform);
        T component = g.GetComponent<T>();
        if (component != null) { return component; }
        return g.GetComponentInChildren<T>();
    }

    public static GameObject InstantiateAsChildAtLocation(this GameObject parent, GameObject original, string locatorName) {
        if (original == null) {
            Debug.LogError("Can't instantiate from null", parent);
            return null;
        }
        GameObject g = parent.InstantiateAsChild(original);
        GameObject l = parent.FindChildByName(locatorName);
        g.transform.position = l.transform.position;
        g.transform.rotation = l.transform.rotation;
        return g;
    }

    public static T InstantiateAsChildAtLocation<T>(this GameObject parent, T original, string locatorName) where T : Component {
        if (original == null) {
            Debug.LogError("Can't instantiate from null", parent);
            return null;
        }
        GameObject g = parent.InstantiateAsChildAtLocation(original.gameObject, locatorName);
        return g.GetComponent<T>();
    }

    public static void SetLayerRecursively(this GameObject parent, string layerName) {
        Transform[] transforms = GetAllChildren(parent.transform);
        int layer = LayerMask.NameToLayer(layerName);;
        parent.layer = layer;
        foreach (Transform t in transforms) {
            t.gameObject.layer = layer;
        }
    }

    public static List<Transform> FindAllRootObjects() {
        return FindAllRootObjects(null);
    }

    public static List<Transform> FindAllRootObjects(this GameObject finder) {
        List<Transform> roots = new List<Transform>();
        Transform[] allTransforms = GameObject.FindObjectsOfType<Transform>();
        foreach (Transform t in allTransforms) {
            if (t.parent != null) { continue; }
            roots.Add(t);
        }
        return roots;
    }

    public static T HardFindObjectOfType<T>(this GameObject finder) where T : Component {
        List<Transform> roots = finder.FindAllRootObjects();
        foreach (Transform t in roots) {
            T component = t.gameObject.HardGetComponentInChildren<T>();
            if (component) {
                return component;
            }
        }
        return null;
    }

    public static Component HardFindObjectOfType(Type type) {
        List<Transform> roots = FindAllRootObjects();
        foreach (Transform t in roots) {
            Component c = t.GetComponent(type);
            if (c) { return c; }
            c = t.gameObject.HardGetComponentInChildren(type);
            if (c) { return c; }
        }
        return null;
    }

    public static Component HardGetComponentInChildren(this GameObject go, Type type) {
        Transform[] children = GetAllChildren(go.transform, true);
        for (int i = 0; i < children.Length; i++) {
            Component c = children[i].GetComponent(type);
            if (c) { return c; }
        }
        return null;
    }

    public static T HardGetComponentInChildren<T>(this GameObject go) where T : Component {
        Transform[] children = GetAllChildren(go.transform, true);
        for (int i = 0; i < children.Length; i++) {
            Component c = children[i].GetComponent<T>();
            if (c) { return (T)c; }
        }
        return null;
    }

    public static T FindChildComponentByName<T>(this GameObject go, string name) where T : Component {
        Transform[] children = GetAllChildren(go.transform, true);

        for (int i = 0; i < children.Length; i++) {
            if (children[i].name.Equals(name)) {
                Component c = children[i].GetComponent<T>();
                if (c) { return (T)c; }
            }
        }
        return null;
    }

    public static GameObject FindChildByName(this GameObject go, string name) {
        Transform transform = go.FindChildComponentByName<Transform>(name);
        if (transform == null) {
            return null;
        }
        return transform.gameObject;
    }

    public static List<GameObject> FindChildrenByName(this GameObject go, string name) {
        Transform[] results = (Transform[])go.GetComponentsInChildren<Transform>();
        List<GameObject> childrenWithName = new List<GameObject>();
        for (int i = 0; i < results.Length; i++) {
            if (results[i].name.Equals(name)) {
                childrenWithName.Add(results[i].gameObject);
            }
        }
        return childrenWithName;
    }

    public static Transform FindChild(Transform findIn, string name, bool strictMatch = false) {
        return FindChild(findIn, new string[] { name }, strictMatch);
    }

    public static Transform FindChild(Transform findIn, string[] names, bool strictMatch = false) {
        foreach (string name in names) {
            Transform result = findIn.Find(name);
            if (result != null)
                return result;

            //Als een directe hit niet gemaakt is, gaan we naar een match zoeken
            if (!strictMatch) {
                string nameLower = name.ToLower();
                foreach (Transform child in findIn) {
                    if (child.name.ToLower().IndexOf(nameLower) != -1)
                        return child;
                }
            }

            //Als er niets gevonden is komt ie hier; hier kijken we naar alle subchildren
            foreach (Transform child in findIn) {
                result = FindChild(child, names, strictMatch);
                if (result != null)
                    return result;
            }
        }

        return null;
    }

    public static Transform[] FindChildren(Transform findIn, string name, bool strictMatch = false) {
        return FindChildren(findIn, new string[] { name }, strictMatch);
    }
    public static Transform[] FindChildren(Transform findIn, string[] names, bool strictMatch = false) {
        List<Transform> result = new List<Transform>();
        if (strictMatch) {
            foreach (Transform child in findIn) {
                foreach (string name in names) {
                    if (child.name == name)
                        result.Add(child);
                }

                result.AddRange(FindChildren(child, names, strictMatch));
            }
        } else {
            foreach (Transform child in findIn) {
                foreach (string name in names) {
                    string nameLower = name.ToLower();
                    if (child.name.ToLower().IndexOf(nameLower) != -1)
                        result.Add(child);
                }

                result.AddRange(FindChildren(child, names, strictMatch));
            }
        }

        return result.ToArray();
    }

    public static Transform[] GetAllChildren(this GameObject from, bool recursive = true) {
        return GetAllChildren(from.transform, recursive);
    }

    public static Transform[] GetAllChildren(Transform from, bool recursive = true) {
        List<Transform> result = new List<Transform>();
        Transform trans = from.transform;


        int i = trans.childCount;
        while (--i > -1) {
            Transform child = trans.GetChild(i);
            result.Add(child);
            if (recursive) {
                result.AddRange(GetAllChildren(child));
            }
        }

        return result.ToArray();
    }

}