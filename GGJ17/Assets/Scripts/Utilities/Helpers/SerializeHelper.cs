﻿using System.Xml.Serialization;
using System.IO;
using UnityEngine;

public static class SerializeHelper {

    public static void Serialize<T>(string path, T content) where T : class {
        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Create);
        serializer.Serialize(stream, content);
        stream.Close();
    }

    public static T Deserialize<T>(string path) where T : class {
        if (!File.Exists(path)) {
            Debug.LogWarning("SerializeHelper: Unable to find file at path: " + path);
            return default(T);
        }

        var serializer = new XmlSerializer(typeof(T));
        var stream = new FileStream(path, FileMode.Open);
        T instance = serializer.Deserialize(stream) as T;
        stream.Close();
        return instance;
    }

    public static T DeserializeString<T>(string data) where T : class {
        var serializer = new XmlSerializer(typeof(T));
        using (var reader = new StringReader(data)) {
            return serializer.Deserialize(reader) as T;
        }
    }

}