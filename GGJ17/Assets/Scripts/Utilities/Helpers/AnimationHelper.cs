﻿using UnityEngine;
using System.Collections;

public static class AnimationHelper {

	public static void Play(this Animation animation, AnimationClip clip) {
        animation.AddClipDistinct(clip);
        animation.Play(clip.name);
    }

    public static void PlayQueued(this Animation animation, AnimationClip clip) {
        animation.AddClipDistinct(clip);
        animation.PlayQueued(clip.name);
    }

    public static void AddClipDistinct(this Animation animation, AnimationClip clip) {
        if (animation.GetClip(clip.name)) { return; }
        animation.AddClip(clip, clip.name);
    }

}
