﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum KeyState {
    Down,
    Up,
    Hold
}

public static class InputHelper {

    public static List<Touch> GetTouches() {
        return new List<Touch>(Input.touches);
    }

    public static List<Touch> GetTaps() {
        List<Touch> touches = new List<Touch>(Input.touches);
        touches.RemoveAll(x => x.phase != TouchPhase.Began);
        return touches;
    }

    public static bool QueryAnyKeyState(List<KeyCode> keyCodes, KeyState keyStyle) {
        foreach (KeyCode keyCode in keyCodes) {
            switch (keyStyle) {
                case KeyState.Down:
                    if (Input.GetKeyDown(keyCode)) { return true; }
                    break;
                case KeyState.Up:
                    if (Input.GetKeyUp(keyCode)) { return true; }
                    break;
                case KeyState.Hold:
                    if (Input.GetKey(keyCode)) { return true; }
                    break;
            }
        }
        return false;
    }
	
}
