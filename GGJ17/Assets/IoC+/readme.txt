## IoC+

IoC+ is a framework written in C#, specially designed to be the most complete Inversion of Control solution for Unity. 

Coming with the insightful IoC+ Monitor, a Unity Editor extension, providing a live feed of what's happening in game. This includes context hierarchies, bindings, visualized command executions and signal dispatches. All interactive to open associated code files, making it much easier for new programmers to hop in on your project and understand the code. 

IoC+ is indeed a big addition to the standard IoC pattern. Contexts can be nested to have full control over isolated injections. It's easy to add, remove and switch contexts to change behavior. IoC+ even integrates the Finite State Machine pattern for contexts, a big benefit for games.

## IoC+ Monitor

To open the Ioc+ Monitor window, go to the editor's topbar and select Window/IoC+ Monitor. More information about the IoC+ Monitor: http://iocplus.com/monitor/

## Documentation

The root folder of the IoC+ contains a pdf providing all information to get started with IoC+.

More tutorials, documentation and additional information is available online at:
http://www.iocplus.com

## Support

If you have any questions or stumbled upon a bug, you can post it at the IoC+ Unity Thread and we'll get back to you as soon as possible!
https://forum.unity3d.com/threads/434417/