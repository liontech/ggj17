/// <copyright file="DynamicContext.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;
using System.Collections.Generic;

namespace IoCPlus {

    /// <summary>
    /// Mimics the injection process of an actual context. Use this class for unit testing purposes.
    /// </summary>
    [ScriptDoc("Editor Only", "DynamicContext", "Mimics the injection process of an actual context. Use this class for unit testing purposes.")]
    public class DynamicContext {

        private Dictionary<Type, object> injectionBindings = new Dictionary<Type, object>();
        Dictionary<string, Dictionary<Type, object>> labeledInjectionBindings = new Dictionary<string, Dictionary<Type, object>>();
        private Dictionary<Type, object> parameterBindings = new Dictionary<Type, object>();
        private Dictionary<Type, Type> mediatorBindings = new Dictionary<Type, Type>();

        /// <summary>
        /// Binds the given type to a new instance of that type.
        /// </summary>
        /// <typeparam name="T">Injection type</typeparam>
        /// <returns>The instantiated instance of the given type.</returns>
        [ScriptDoc("Adds an injection binding of given type.")]
        public T Bind<T>() where T : class, new() {
            return Bind<T, T>();
        }

        /// <summary>
        /// Binds the given type to a new instance of a second given type.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <typeparam name="U">Injection instance type.</typeparam>
        /// <returns>The instantiated instance of the given instance type.</returns>
        public T Bind<T, U>() where T : class where U : T, new() {
            return Bind<T>(new U());
        }

        /// <summary>
        /// Binds the given type to the given instance.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <param name="singleton">Injection instance.</param>
        /// <returns>The instance cast to the given injection type.</returns>
        public T Bind<T>(object singleton) where T : class {
            Type keyType = typeof(T);
            injectionBindings.Remove(keyType);
            injectionBindings.Add(keyType, singleton);
            return singleton as T;
        }

        /// <summary>
        /// Binds the given type to a new instance of that type, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type</typeparam>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instantiated instance of the given type.</returns>
        [ScriptDoc("Adds a labeled injection binding of given type.")]
        public T BindLabeled<T>(object label) where T : class, new() {
            return BindLabeled<T, T>(label);
        }

        /// <summary>
        /// Binds the given type to a new instance of a second given type, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <typeparam name="U">Injection instance type.</typeparam>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instantiated instance of the given instance type.</returns>
        public T BindLabeled<T, U>(object label) where T : class where U : T, new() {
            return BindLabeled<T>(new U(), label);
        }

        /// <summary>
        /// Binds the given type to the given instance, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <param name="singleton">Injection instance.</param>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instance cast to the given injection type.</returns>
        public T BindLabeled<T>(object singleton, object label) where T : class {
            Type keyType = typeof(T);

            string labelString = label.GetType() == typeof(string) ? label as string : label.ToString();
            if (!labeledInjectionBindings.ContainsKey(labelString)) {
                labeledInjectionBindings.Add(labelString, new Dictionary<Type, object>());
            }

            Dictionary<Type, object> labeledBindings = labeledInjectionBindings[labelString];
            labeledBindings.Remove(keyType);
            labeledBindings.Add(keyType, singleton);
            return singleton as T;
        }

        /// <summary>
        /// Binds the given type to a new instance of that type, to be injected as if injected by a signal. Use this for command testing.
        /// </summary>
        /// <typeparam name="T">Injection type</typeparam>
        /// <returns>The instantiated instance of the given type.</returns>
        [ScriptDoc("Adds a parameter injection binding of given type.")]
        public T BindParameter<T>() where T : new() {
            return BindParameter<T, T>();
        }

        /// <summary>
        /// Binds the given type to a new instance of a second given type, to be injected as if injected by a signal. Use this for command testing.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <typeparam name="U">Injection instance type.</typeparam>
        /// <returns>The instantiated instance of the given instance type.</returns>
        public T BindParameter<T, U>() where U : T, new() {
            return BindParameter<T>(new U());
        }

        /// <summary>
        /// Binds the given type to the given instance, to be injected as if injected by a signal. Use this for command testing.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <param name="singleton">Injection instance.</param>
        /// <returns>The instance cast to the given injection type.</returns>
        public T BindParameter<T>(object value) {
            Type keyType = typeof(T);
            parameterBindings.Remove(keyType);
            parameterBindings.Add(keyType, value);
            return (T)value;
        }

        /// <summary>
        /// Binds the given mediator type to the given view type.
        /// </summary>
        /// <typeparam name="T">Mediator type</typeparam>
        /// <typeparam name="U">View Type</typeparam>
        [ScriptDoc("Adds a mediator binding of given mediator and view types.")]
        public void BindMediator<T, U>() where T : Mediator where U : View {
            mediatorBindings.Remove(typeof(U));
            mediatorBindings.Add(typeof(U), typeof(T));
        }

        /// <summary>
        /// Injects values based on bindings into the given instance.
        /// </summary>
        /// <param name="instance">Instance with field that have the inject or injectParameter attribute.</param>
        [ScriptDoc("Injects values based on bindings into the given instance.")]
        public void InjectInto(object instance) {
            Injector.Inject<Inject>(instance, injectionBindings);
            Injector.Inject<InjectParameter>(instance, parameterBindings);
            Injector.Inject<Inject>(instance, labeledInjectionBindings);
        }

    }

}