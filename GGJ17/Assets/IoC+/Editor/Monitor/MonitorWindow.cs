/// <copyright file="MonitorWindow.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using UnityEditor;
using System;
using System.Diagnostics;
using IoCPlus.Internal;

namespace IoCPlus.Editor {

    public class MonitorWindow : EditorWindow {

        public static bool IsOpen { get { return instance != null; } }

        public static Vector2 WindowOffset { get; private set; }
        public static Vector2 WindowPadding { get { return new Vector2(20, 20); } }
        public static Rect WindowRect { get; private set; }
        public static Rect MiniMapWindowRect { get; private set; }
        public static float MiniMapScaleFactor { get; private set; }
        public static Vector2 MiniMapOffset { get { return WindowPadding * MiniMapScaleFactor; } }
        public static Vector2 MousePosition { get; private set; }
        public static float DeltaTime { get; private set; }
        public static bool MiniMapEnabled { get; private set; }
        public static bool IsHoveringMiniMap { get; private set; }

        private const int PanButton = 0;
        private const float ScrollSpeed = 4.0f;
        private const float TopBarHeight = 30;
        private const float TopBarMargin = 5;
        private const float WindowMargin = 5;

        private static bool ShowPeformance = false;

        private static MonitorWindow instance;

        private static bool hasInit = false;

        private static MonitorViewport viewport;

        private static bool isPanning = false;
        private static bool isPanningMiniMap = false;
        private static Vector2 panDragStart = Vector2.zero;

        private static Texture2D backgroundTexture;

        private static double previousTimeSinceStartup;

        [MenuItem("Window/IoC+ Monitor")]
        private static void Init() {
            if (instance) { Deinit(); }
            hasInit = true;
            instance = GetWindow(typeof(MonitorWindow)) as MonitorWindow;

            instance.titleContent = new GUIContent("IoC+ Monitor");
            instance.minSize = new Vector2(417, 200);

            previousTimeSinceStartup = EditorApplication.timeSinceStartup;

            WindowOffset = WindowPadding;

            ContextLogger.ResetActions();

            viewport = new MonitorViewport();

            if (!backgroundTexture) {
                backgroundTexture = new Texture2D(1, 1, TextureFormat.ARGB32, true);
                backgroundTexture.hideFlags = HideFlags.DontSave;
                backgroundTexture.SetPixel(0, 1, new Color(0.05f, 0.05f, 0.05f));
                backgroundTexture.Apply();
            }
        }

        private static void InitIfNeeded() {
            if (hasInit) { return; }
            Init();
        }

        private static void Deinit() {
            instance = null;
        }

        private void OnDestroy() {
            Deinit();
        }

        private void Update() {
            Repaint();
        }

        private void OnGUI() {
            InitIfNeeded();

            DeltaTime = (float)(EditorApplication.timeSinceStartup - previousTimeSinceStartup);
            previousTimeSinceStartup = EditorApplication.timeSinceStartup;

            Stopwatch updateWatch = Stopwatch.StartNew();

            UpdateWindowRects();

            MousePosition = Event.current.mousePosition;

            UpdateIsHoveringMiniMap();

            HandlePanning();

            viewport.Update();

            updateWatch.Stop();
            Stopwatch renderWatch = Stopwatch.StartNew();

            DrawTopBar();

            GUI.DrawTexture(WindowRect, backgroundTexture);

            ButtonWidget.Tooltip = "";

            BeginWindows();

            GUI.skin.window.normal.background = backgroundTexture;
            GUI.skin.window.focused.background = backgroundTexture;
            GUI.skin.window.hover.background = backgroundTexture;
            GUI.skin.window.onActive.background = backgroundTexture;
            GUI.skin.window.onFocused.background = backgroundTexture;
            GUI.skin.window.onHover.background = backgroundTexture;
            GUI.skin.window.onNormal.background = backgroundTexture;

            GUI.Window(0, WindowRect, RenderViewport, "");

            EndWindows();

            if (!string.IsNullOrEmpty(ButtonWidget.Tooltip) && !IsHoveringMiniMap) {
                GUI.TextArea(new Rect(MousePosition.x + 15, MousePosition.y + 10, 80, 16), ButtonWidget.Tooltip);
            }

            renderWatch.Stop();

            EditorStyles.ResetLabelStyle();

            if (ShowPeformance) {
                GUI.Label(new Rect(10, position.height - 40, position.width - 10, 30), "Update: " + updateWatch.ElapsedMilliseconds.ToString() + " ms");
                GUI.Label(new Rect(10, position.height - 25, position.width - 10, 30), "Render: " + renderWatch.ElapsedMilliseconds.ToString() + " ms");
            }
        }

        private void HandlePanning() {
            if (Event.current.type == EventType.MouseDown && Event.current.button == PanButton && WindowRect.Contains(MousePosition) && !IsHoveringMiniMap) {
                isPanning = true;
                panDragStart = MousePosition;
            }

            if (Event.current.rawType == EventType.MouseUp) {
                isPanning = false;
            }

            if (!isPanning && !isPanningMiniMap && Event.current.type == EventType.ScrollWheel && WindowRect.Contains(MousePosition)) {
                WindowOffset -= Vector2.up * Event.current.delta.y * ScrollSpeed;
            }

            if (isPanning) {
                WindowOffset += MousePosition - panDragStart;
                panDragStart = MousePosition;
            } else if (isPanningMiniMap) {
                WindowOffset = -((Event.current.mousePosition - WindowRect.position - WindowPadding * MiniMapScaleFactor) - MiniMapWindowRect.position) / MiniMapScaleFactor + WindowRect.size * .5f;
                KeepWindowOffsetWithinBoundaries(false);
            } else {
                KeepWindowOffsetWithinBoundaries(true);
            }
        }

        private void KeepWindowOffsetWithinBoundaries(bool soft) {
            Rect windowRect = WindowRect;

            Vector2 newWindowOffset = WindowOffset;

            bool overLeft = WindowOffset.x < viewport.BoundingBox.x + WindowPadding.x;
            bool overRight = WindowOffset.x + viewport.BoundingBox.width > windowRect.width - WindowPadding.x;
            bool overTop = WindowOffset.y < viewport.BoundingBox.y + WindowPadding.y;
            bool overBottom = WindowOffset.y + viewport.BoundingBox.height > windowRect.height - WindowPadding.y;

            bool insideBoundingBoxHor = viewport.BoundingBox.width > windowRect.width;
            bool insideBoundingBoxVert = viewport.BoundingBox.height > windowRect.height;

            if (overLeft && !overRight) {
                float endValue = insideBoundingBoxHor ? WindowRect.width - viewport.BoundingBox.width - WindowPadding.x : viewport.BoundingBox.x + WindowPadding.x;
                newWindowOffset.x = Mathf.Lerp(WindowOffset.x, endValue, soft ? 0.1f : 1.0f);
            }
            if (overRight && !overLeft) {
                float endValue = insideBoundingBoxHor ? viewport.BoundingBox.x + WindowPadding.x : WindowRect.width - viewport.BoundingBox.width - WindowPadding.x;
                newWindowOffset.x = Mathf.Lerp(WindowOffset.x, endValue, soft ? 0.1f : 1.0f);
            }

            if (overTop && !overBottom) {
                float endValue = insideBoundingBoxVert ? WindowRect.height - viewport.BoundingBox.height - WindowPadding.y : viewport.BoundingBox.y + WindowPadding.y;
                newWindowOffset.y = Mathf.Lerp(WindowOffset.y, endValue, soft ? 0.1f : 1.0f);
            }
            if (overBottom && !overTop) {
                float endValue = insideBoundingBoxVert ? viewport.BoundingBox.y + WindowPadding.y : WindowRect.height - viewport.BoundingBox.height - WindowPadding.y;
                newWindowOffset.y = Mathf.Lerp(WindowOffset.y, endValue, soft ? 0.1f : 1.0f);
            }

            WindowOffset = newWindowOffset;
        }

        private void DrawTopBar() {
            GUILayout.BeginHorizontal();

            int buttonWidth = 75;
            Color enabledColor = Color.white;
            Color timeControlDisabledColor = new Color(.7f, .7f, .7f);

            DrawTopBarButton("Sync", buttonWidth, enabledColor, timeControlDisabledColor, MonitorSettings.TimeControlSetting == MonitorSettings.TimeControl.Synced, delegate {
                MonitorSettings.TimeControlSetting = MonitorSettings.TimeControl.Synced;
            });

            DrawTopBarButton("Delay", buttonWidth, enabledColor, timeControlDisabledColor, MonitorSettings.TimeControlSetting == MonitorSettings.TimeControl.Delayed, delegate {
                MonitorSettings.TimeControlSetting = MonitorSettings.TimeControl.Delayed;
            });

            bool loggerIsOnMaxLogs = ContextLogger.LogCountWithInterval >= ContextLogger.MAX_LOG_AMOUNT;
            DrawTopBarButton("Pause", buttonWidth, loggerIsOnMaxLogs ? new Color(1.0f, .5f, .5f) : enabledColor, timeControlDisabledColor,

                MonitorSettings.TimeControlSetting == MonitorSettings.TimeControl.Paused, delegate {
                    MonitorSettings.TimeControlSetting = MonitorSettings.TimeControl.Paused;
                });

            DrawTopBarButton("Step (" + ContextLogger.LogCountWithInterval + ")", buttonWidth, enabledColor, enabledColor, true, delegate {
                if (MonitorSettings.TimeControlSetting != MonitorSettings.TimeControl.Paused) { return; }
                ContextLogger.Step();
            });

            GUILayout.FlexibleSpace();

            DrawTopBarButton("Options", buttonWidth, enabledColor, enabledColor, true, delegate {
                GenericMenu menu = new GenericMenu();

                menu.AddItem(new GUIContent("Show All Bindings"), false, delegate () {
                    MonitorSettings.ShowInjectionBindings = true;
                    MonitorSettings.ShowDefaultBindings = true;
                    MonitorSettings.ShowMediatorBindings = true;
                    MonitorSettings.ShowCommandBindings = true;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.AddItem(new GUIContent("Hide All Bindings"), false, delegate () {
                    MonitorSettings.ShowInjectionBindings = false;
                    MonitorSettings.ShowDefaultBindings = false;
                    MonitorSettings.ShowMediatorBindings = false;
                    MonitorSettings.ShowCommandBindings = false;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.AddSeparator("");

                menu.AddItem(new GUIContent("Show Injection Bindings"), MonitorSettings.ShowInjectionBindings, delegate () {
                    MonitorSettings.ShowInjectionBindings = !MonitorSettings.ShowInjectionBindings;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                if (MonitorSettings.ShowInjectionBindings) {
                    menu.AddItem(new GUIContent("Show Default Bindings"), MonitorSettings.ShowDefaultBindings, delegate () {
                        MonitorSettings.ShowDefaultBindings = !MonitorSettings.ShowDefaultBindings;
                        MonitorViewport.SetBindingWidgetsDirty();
                    });
                } else {
                    menu.AddDisabledItem(new GUIContent("Show Default Bindings"));
                }

                menu.AddItem(new GUIContent("Show Mediator Bindings"), MonitorSettings.ShowMediatorBindings, delegate () {
                    MonitorSettings.ShowMediatorBindings = !MonitorSettings.ShowMediatorBindings;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.AddItem(new GUIContent("Show Command Bindings"), MonitorSettings.ShowCommandBindings, delegate () {
                    MonitorSettings.ShowCommandBindings = !MonitorSettings.ShowCommandBindings;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.AddSeparator("");

                menu.AddItem(new GUIContent("Show Namespaces"), MonitorSettings.ShowNamespaces, delegate () {
                    MonitorSettings.ShowNamespaces = !MonitorSettings.ShowNamespaces;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.AddItem(new GUIContent("Show Mediator Instances"), MonitorSettings.ShowMediatorInstances, delegate () {
                    MonitorSettings.ShowMediatorInstances = !MonitorSettings.ShowMediatorInstances;
                    MonitorViewport.SetBindingWidgetsDirty();
                });

                menu.ShowAsContext();
            });

            GUILayout.EndHorizontal();
        }

        private void DrawTopBarButton(string text, int width, Color enabledColor, Color disabledColor, bool setting, Action onClick) {
            GUI.color = setting ? enabledColor : disabledColor;
            if (GUILayout.Button(text, GUILayout.Width(width), GUILayout.Height(TopBarHeight - 3))) {
                if (onClick != null) {
                    onClick();
                }
            }
            GUI.color = Color.white;
        }

        private void RenderViewport(int id) {
            Rect windowRect = WindowRect;
            MiniMapEnabled = windowRect.size.x < viewport.BoundingBox.size.x || windowRect.size.y < viewport.BoundingBox.size.y;
            if (MiniMapEnabled) {
                GUI.color = new Color(0.0f, 0.0f, 0.0f, 0.0f);
                isPanningMiniMap = GUI.RepeatButton(MiniMapWindowRect, "");
                GUI.color = Color.white;
            }

            viewport.Render();

            if (MiniMapEnabled) {
                RenderingHelper.RenderRect(MiniMapWindowRect, new Color(.5f, .5f, .5f, .3f * (IsHoveringMiniMap ? 1.0f : EditorStyles.MiniMapMouseOutAlpha)));

                viewport.RenderMiniMap();

                RenderingHelper.RenderRect(new Rect(MiniMapWindowRect.x - (WindowOffset.x - WindowPadding.x) * MiniMapScaleFactor,
                                            MiniMapWindowRect.y - (WindowOffset.y - WindowPadding.y) * MiniMapScaleFactor,
                                            windowRect.width * MiniMapScaleFactor,
                                            windowRect.height * MiniMapScaleFactor), new Color(1.0f, 1.0f, 1.0f, .2f * (IsHoveringMiniMap ? 1.0f : EditorStyles.MiniMapMouseOutAlpha)), MiniMapWindowRect);
            } else {
                isPanningMiniMap = false;
            }
        }

        private void UpdateWindowRects() {
            WindowRect = new Rect(WindowMargin, TopBarHeight + TopBarMargin, position.width - WindowMargin * 2, position.height - (TopBarHeight + TopBarMargin) - WindowMargin);

            Rect boundingBox = viewport.BoundingBox;
            const float maxSize = 150;
            Vector2 size = new Vector2();
            if (boundingBox.width > boundingBox.height) {
                size.x = maxSize;
                size.y = maxSize / boundingBox.width * boundingBox.height;
                MiniMapScaleFactor = maxSize / (boundingBox.width + WindowPadding.x * 2);
            } else {
                size.y = maxSize;
                size.x = maxSize / boundingBox.height * boundingBox.width;
                MiniMapScaleFactor = maxSize / (boundingBox.height + WindowPadding.y * 2);
            }
            MiniMapWindowRect = new Rect(WindowRect.width - WindowMargin - size.x, WindowRect.height - WindowMargin - size.y, size.x, size.y);
        }

        private void UpdateIsHoveringMiniMap() {
            IsHoveringMiniMap = MiniMapEnabled && MiniMapWindowRect.Contains(MousePosition - WindowRect.position);
        }

    }

}