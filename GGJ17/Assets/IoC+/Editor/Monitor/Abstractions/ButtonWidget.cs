/// <copyright file="ButtonWidget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using System.Collections;
using UnityEditor;

namespace IoCPlus.Editor {

    public class ButtonWidget : Widget {

        public float Width {
            get { return width; }
            protected set {
                width = value;
                UpdateRect();
            }
        }

        public float Height {
            get { return height; }
            protected set {
                height = value;
                UpdateRect();
            }
        }

        public static string Tooltip;

        public bool IsHovered {
            get {
                return rect.Contains(MonitorWindow.MousePosition - MonitorWindow.WindowOffset - MonitorWindow.WindowRect.position);
            }
        }

        public override Vector2 Position {
            get {
                return new Vector2(rect.x + .5f * Width, rect.y + .5f * Height);
            }
            set {
                rect.x = value.x - .5f * Width;
                rect.y = value.y - .5f * Height;
            }
        }

        private const float TooltipHoverDuration = 1.2f;
        private const float TooltipShowDuration = 2.5f;
        private const float PulseDuration = 2.0f;

        private static GUIStyle buttonRightClickStyle;
        private static GUIStyle buttonStyle;

        private float width = 200;
        private float height = 30;

        private Rect rect;
        private float durationHovered;

        private bool forcePulse;
        private Color pulseColor = Color.green;
        private float pulseDurationLeft;

        public ButtonWidget() {
            UpdateRect();
            durationHovered = 0.0f;
        }

        public override void Update() {
            base.Update();

            if (pulseDurationLeft > 0.0f && !forcePulse) {
                pulseDurationLeft -= MonitorWindow.DeltaTime;
            }
        }

        public virtual void Render() {
            if (buttonRightClickStyle == null) {
                buttonRightClickStyle = new GUIStyle(GUI.skin.button);
                buttonRightClickStyle.active.textColor = Color.white;
                buttonRightClickStyle.active.background = buttonRightClickStyle.normal.background;

                buttonStyle = new GUIStyle(buttonRightClickStyle);
                buttonStyle.active.textColor = buttonStyle.normal.textColor;
            }

            Rect buttonRect = GetButtonRect();

            if (buttonRect.Contains(MonitorWindow.MousePosition - MonitorWindow.WindowRect.position)) {
                durationHovered += MonitorWindow.DeltaTime;
                if (durationHovered >= TooltipHoverDuration && durationHovered <= TooltipHoverDuration + TooltipShowDuration) {
                    Tooltip = "Right click...";
                }
            } else {
                durationHovered = 0.0f;
            }

            GUIStyle style = Event.current.button == 1 ? buttonRightClickStyle : buttonStyle;
            style.alignment = TextAnchor.MiddleLeft;

            float pulseLerp = pulseDurationLeft / PulseDuration;
            if (pulseLerp < .9f) { pulseLerp *= .7f; }
            GUI.color = Color.Lerp(Color.white, pulseColor, pulseLerp);

            if (GUI.Button(buttonRect, GetText(), style) && Event.current.button == 1) {
                Tooltip = "";
                ShowContextMenu();
            }

            GUI.color = Color.white;
        }

        public virtual void RenderMiniMap() {
            Vector2 position = MonitorWindow.MiniMapWindowRect.position + (rect.position + MonitorWindow.WindowPadding) * MonitorWindow.MiniMapScaleFactor;
            Vector2 size = rect.size * MonitorWindow.MiniMapScaleFactor;
            RenderingHelper.RenderRect(new Rect(position.x, position.y, size.x, size.y),
                                       new Color(1.0f, 1.0f, 1.0f, .5f * (MonitorWindow.IsHoveringMiniMap ? 1.0f : EditorStyles.MiniMapMouseOutAlpha)),
                                       MonitorWindow.MiniMapWindowRect);
        }

        public void Pulse(Color pulseColor, bool forcePulse = false) {
            this.pulseColor = pulseColor;
            this.forcePulse = forcePulse;
            pulseDurationLeft = PulseDuration;
        }

        protected virtual void ShowContextMenu() { }

        protected virtual string GetText() {
            return "";
        }

        protected Rect GetButtonRect() {
            return new Rect(rect.x + MonitorWindow.WindowOffset.x, rect.y + MonitorWindow.WindowOffset.y, rect.width, rect.height);
        }

        private void UpdateRect() {
            rect = new Rect(0, 0, Width, Height);
        }

    }

}