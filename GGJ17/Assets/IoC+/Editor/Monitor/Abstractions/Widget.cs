/// <copyright file="Widget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus.Editor {

    public class Widget {

        public virtual Vector2 Position { get; set; }

        public virtual void Update() {

        }

    }

}