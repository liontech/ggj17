/// <copyright file="MonitorViewport.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using IoCPlus.Internal;

namespace IoCPlus.Editor {

    public class MonitorViewport {

        public Rect BoundingBox {
            get {
                return new Rect(0.0f, 0.0f, cachedWidth, cachedHeight);
            }
        }

        public string Title {
            get {
                return "Contexts";
            }
        }

        private const float labelHeight = 45.0f;
        private const float widgetMarginVer = 30.0f;

        private float cachedWidth = 0.0f;
        private float cachedHeight = 0.0f;

        private List<ContextWidget> contextWidgets = new List<ContextWidget>();
        private Dictionary<Context, ContextWidget> allContextWidgets = new Dictionary<Context, ContextWidget>();

        private static bool isBoundingBoxDirty;
        private static bool isBindingWidgetsDirty;

        public static void SetBoundingBoxDirty() {
            isBoundingBoxDirty = true;
        }

        public static void SetBindingWidgetsDirty() {
            isBindingWidgetsDirty = true;
        }

        public MonitorViewport() {
            ContextLogger.OnContextInstantiated += OnContextInstantiated;
            ContextLogger.OnContextRemoved += OnContextRemoved;
            ContextLogger.OnContextParentSet += OnContextParentSet;
        }

        public void Deinit() {
            ContextLogger.OnContextInstantiated -= OnContextInstantiated;
            ContextLogger.OnContextRemoved -= OnContextRemoved;
            ContextLogger.OnContextParentSet -= OnContextParentSet;
        }

        public void ShowContextMenu() {
            GenericMenu menu = new GenericMenu();

            menu.AddItem(new GUIContent("What is this?"), false, delegate () {
                Application.OpenURL("http://google.com/");
            });

            menu.ShowAsContext();
        }

        public void Update() {
            if (isBoundingBoxDirty) {
                RepositionWidgets();
                isBoundingBoxDirty = false;
            }

            if (isBindingWidgetsDirty) {
                RegenerateBindingWidgets();
                isBindingWidgetsDirty = false;
            }

            ContextLogger.Update();
            foreach (ContextWidget widget in contextWidgets) {
                widget.Update();
            }
        }

        public void Render() {
            if (contextWidgets.Count > 0) {
                foreach (ContextWidget widget in contextWidgets) {
                    widget.Render();
                }
            } else {
                RenderDefaultMessage("No contexts notified.");
            }
        }

        public void RenderMiniMap() {
            if (!Application.isPlaying) { return; }
            foreach (ContextWidget widget in contextWidgets) {
                widget.RenderMiniMap();
            }
        }

        private void RenderDefaultMessage(string message) {
            EditorStyles.SetLabelStyle(EditorStyles.LabelStyle.DefaultMessage);
            GUI.color = EditorStyles.DefaultMessageColor;
            GUI.Label(new Rect(18, 15, 600, 100), message);
            EditorStyles.ResetLabelStyle();
        }

        private void RepositionWidgets() {
            float totalHeight = 0;
            cachedWidth = 0;
            foreach (ContextWidget widget in contextWidgets) {
                cachedWidth = Mathf.Max(cachedWidth, widget.TotalWidth);
                widget.SetPosition(Vector2.up * totalHeight);
                totalHeight += widget.TotalHeight + widgetMarginVer;
            }
            cachedHeight = totalHeight - widgetMarginVer;
        }

        private void SortContextWidgets() {
            contextWidgets.Sort(delegate (ContextWidget a, ContextWidget b) {
                return Prioritize.Compare(a.Context, b.Context);
            });
        }

        private void RegenerateBindingWidgets() {
            contextWidgets.ForEach(x => x.RegenerateBindingWidgets());
        }

        private void OnContextInstantiated(Context context) {
            ContextWidget contextWidget = new ContextWidget(context);
            contextWidgets.Add(contextWidget);
            SortContextWidgets();
            allContextWidgets.Add(context, contextWidget);
            RepositionWidgets();
            ContextLogger.NotifyLogReaction();
        }

        private void OnContextRemoved(Context context) {
            ContextWidget contextWidget = contextWidgets.Find(x => x.Context == context);
            if (contextWidget == null) { return; }
            contextWidgets.Remove(contextWidget);
            allContextWidgets.Remove(context);
            contextWidget.Deinitialize();
            RepositionWidgets();
            ContextLogger.NotifyLogReaction();
        }

        private void OnContextParentSet(Context context, IContext parentContext) {
            ContextWidget childWidget = allContextWidgets[context];

            foreach (KeyValuePair<Context, ContextWidget> pair in allContextWidgets) {
                if (pair.Key == parentContext) {
                    pair.Value.AddChild(childWidget);
                } else {
                    pair.Value.RemoveChild(childWidget);
                }
            }

            if (parentContext == null) {
                contextWidgets.Add(childWidget);
                SortContextWidgets();
            } else {
                contextWidgets.Remove(childWidget);
            }

            RepositionWidgets();
            ContextLogger.NotifyLogReaction();
        }

    }

}