/// <copyright file="EditorStyles.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus.Editor {

    public static class EditorStyles {

        public enum LabelStyle {
            DefaultMessage,
            BindingsTitle,
            StateTitle
        }

        public static readonly Color DefaultMessageColor = Color.white;
        public static readonly float MiniMapMouseOutAlpha = .2f;

        public static readonly Color ContextChildLineColor = Color.white;
        public static readonly Color ContextStateLineColor = Color.green;

        public static readonly Color PulseContextInitializedColor = Color.green;
        public static readonly Color PulseSignalDispatchColor = Color.green;
        public static readonly Color PulseCommandExecuteColor = Color.yellow;
        public static readonly Color PulseCommandReleaseColor = Color.green;
        public static readonly Color PulseCommandAbortColor = Color.red;
        public static readonly Color PulseCommandRevertColor = Color.blue;

        public static void SetLabelStyle(LabelStyle style) {
            ResetLabelStyle();
            switch (style) {
                case LabelStyle.DefaultMessage:
                    GUI.skin.label.fontSize = 20;
                    GUI.skin.label.normal.textColor = new Color(.8f, .8f, .8f, 1.0f);
                    break;
                case LabelStyle.BindingsTitle:
                    GUI.skin.label.fontSize = 10;
                    GUI.skin.label.alignment = TextAnchor.UpperCenter;
                    GUI.skin.label.normal.textColor = new Color(.8f, .8f, .8f, 1.0f);
                    break;
                case LabelStyle.StateTitle:
                    GUI.skin.label.fontSize = 10;
                    GUI.skin.label.alignment = TextAnchor.UpperCenter;
                    GUI.skin.label.normal.textColor = ContextStateLineColor;
                    break;
            }
        }

        public static void ResetLabelStyle() {
            GUI.skin.label.fontSize = 11;
            GUI.skin.label.alignment = TextAnchor.UpperLeft;
            GUI.skin.label.normal.textColor = Color.black;
        }

    }

}