/// <copyright file="EditorHelper.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using System.Reflection;

namespace IoCPlus.Editor {

    public static class EditorHelper {

        public static bool IsDerived(Type derivedClass, Type baseClass) {
            Type it = derivedClass;
            while (it.BaseType != null) {
                if (it.BaseType == baseClass) { return true; }
                it = it.BaseType;
            }
            return false;
        }

        public static List<Type> GetAllDerivedTypes<T>(bool includeEditorAssembly = false) {
            List<Type> types = new List<Type>();
            foreach (var asm in AppDomain.CurrentDomain.GetAssemblies()) {
                if (!includeEditorAssembly) {
                    if (asm == Assembly.GetExecutingAssembly()) { continue; }
                }
                foreach (Type type in asm.GetTypes()) {
                    if (IsDerived(type, typeof(T))) {
                        types.Add(type);
                    }
                }
            }
            return types;
        }

        public static string GetActualTypeName(Type rawType, bool excludeNamespaces = true) {
            return GetActualTypeName(rawType.ToString(), excludeNamespaces);
        }

        public static string GetActualTypeName(string rawTypeName, bool excludeNamespaces = true) {
            char[] chars = new char[] { '`', '+' };
            string actualTypeName = "";
            if (rawTypeName.IndexOfAny(chars) != -1) {
                actualTypeName = rawTypeName.Split(chars)[0];
            } else {
                actualTypeName = rawTypeName;
            }

            if (!excludeNamespaces) {
                return actualTypeName;
            }

            int lastDotPos = actualTypeName.LastIndexOf(".");
            if (lastDotPos >= 0) {
                return actualTypeName.Substring(lastDotPos + 1, actualTypeName.Length - 1 - lastDotPos);
            }

            return actualTypeName;
        }

        public static bool OpenCodeOfType(Type type) {
            UnityEngine.Object asset = GetCodeAssetOfType(type);

            if (asset == null) {
                Debug.LogError("Could not find file of type '" + type + "'. Make sure the filename matches its type.");
                return false;
            }

            AssetDatabase.OpenAsset(asset);

            return true;
        }

        public static UnityEngine.Object GetCodeAssetOfType(Type type) {
            DirectoryInfo directory = new DirectoryInfo(Application.dataPath);

            string typeName = type.ToString();

            // Make sure we don't include namespaces
            int lastDotPos = typeName.LastIndexOf(".");
            if (lastDotPos >= 0) {
                typeName = typeName.Substring(lastDotPos + 1, typeName.Length - 1 - lastDotPos);
            }

            typeName = GetActualTypeName(typeName);

            FileInfo[] fields = directory.GetFiles(typeName + ".cs", SearchOption.AllDirectories);

            for (int i = 0; i < fields.Length; i++) {
                FileInfo field = fields[i];
                if (field == null) { continue; }

                string filePath = field.FullName;
                filePath = filePath.Replace(@"\", "/").Replace(Application.dataPath, "Assets");

                UnityEngine.Object asset = AssetDatabase.LoadAssetAtPath(filePath, typeof(UnityEngine.Object)) as UnityEngine.Object;
                if (asset == null) { continue; }

                return asset;
            }

            return null;
        }

    }

}