/// <copyright file="MediatorInstanceWidget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using UnityEditor;
using UnityEngine;

namespace IoCPlus.Editor {

    public class MediatorInstanceWidget : BindingWidget {

        public Mediator Mediator { get; private set; }

        public MediatorInstanceWidget(Mediator mediator, Context context)
            : base(mediator.GetType(), context) {
            Mediator = mediator;
        }

        protected override void ShowContextMenu() {
            GenericMenu menu = new GenericMenu();

            if (EditorHelper.GetCodeAssetOfType(KeyType) == null) {
                menu.AddDisabledItem(new GUIContent("Open " + typeName + ".cs"));
            } else {
                menu.AddItem(new GUIContent("Open " + typeName + ".cs"), false, delegate () {
                    EditorHelper.OpenCodeOfType(KeyType);
                });
            }

            menu.AddSeparator("");

            if (Application.isPlaying && Mediator.GetView() != null) {
                menu.AddItem(new GUIContent("Select View"), false, delegate () {
                    GameObject viewGameObject = Mediator.GetView().GetGameObject();
                    Selection.activeGameObject = viewGameObject;
                    EditorGUIUtility.PingObject(viewGameObject);
                });
            } else {
                menu.AddDisabledItem(new GUIContent("Select View"));
            }

            menu.ShowAsContext();
        }

    }

}