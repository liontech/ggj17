/// <copyright file="ContextWidget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System;
using IoCPlus.Internal;

namespace IoCPlus.Editor {

    public class ContextWidget : ButtonWidget {

        public const float CONTEXT_MARGIN_HOR = 50.0f;
        public const float CONTEXT_MARGIN_VER = 35.0f;

        public const float BINDING_MARGIN_VER = 1.0f;
        public const float BINDING_LIST_MARGIN_VER = 20.0f;

        public readonly Context Context;

        public float TotalWidth {
            get {
                float totalWidth = Width;

                foreach (ContextWidget childContext in childContextWidgets) {
                    totalWidth = Mathf.Max(totalWidth, Width + CONTEXT_MARGIN_HOR + childContext.TotalWidth);
                }

                return totalWidth;
            }
        }

        public float TotalHeight {
            get {
                float totalHeight = Height;

                if (MonitorSettings.ShowInjectionBindings) {
                    injectionBindingWidgets.ForEach(x => totalHeight += x.TotalHeight + BINDING_MARGIN_VER);
                    if (injectionBindingWidgets.Count > 0) { totalHeight += BINDING_LIST_MARGIN_VER; }
                }

                if (MonitorSettings.ShowMediatorBindings) {
                    mediatorBindingWidgets.ForEach(x => totalHeight += x.TotalHeight + BINDING_MARGIN_VER);
                    if (mediatorBindingWidgets.Count > 0) { totalHeight += BINDING_LIST_MARGIN_VER; }
                }

                if (MonitorSettings.ShowCommandBindings) {
                    commandBindingWidgets.ForEach(x => totalHeight += x.TotalHeight + BINDING_MARGIN_VER);
                    if (commandBindingWidgets.Count > 0) { totalHeight += BINDING_LIST_MARGIN_VER; }
                }

                if (MonitorSettings.ShowMediatorInstances) {
                    mediatorInstanceWidgets.ForEach(x => totalHeight += x.TotalHeight + BINDING_MARGIN_VER);
                    if (mediatorInstanceWidgets.Count > 0) { totalHeight += BINDING_LIST_MARGIN_VER; }
                }

                if (injectionBindingWidgets.Count > 0 ||
                    mediatorBindingWidgets.Count > 0 ||
                    commandBindingWidgets.Count > 0 ||
                    mediatorInstanceWidgets.Count > 0) {
                    totalHeight += BINDING_LIST_MARGIN_VER;
                }

                foreach (ContextWidget childContext in childContextWidgets) {
                    float childOffsetHeight = Mathf.Abs(childContext.Position.y - Position.y);
                    totalHeight = Mathf.Max(totalHeight, childOffsetHeight + childContext.TotalHeight);
                }

                return totalHeight;
            }
        }

        private readonly List<ContextWidget> childContextWidgets = new List<ContextWidget>();
        private readonly List<BindingWidget> injectionBindingWidgets = new List<BindingWidget>();
        private readonly List<BindingWidget> mediatorBindingWidgets = new List<BindingWidget>();
        private readonly List<BindingWidget> commandBindingWidgets = new List<BindingWidget>();
        private readonly List<BindingWidget> mediatorInstanceWidgets = new List<BindingWidget>();

        private string typeName;
        private readonly List<Mediator> mediatorInstances = new List<Mediator>();
        private ContextWidget stateContextWidget;

        public ContextWidget(Context context) : base() {
            Context = context;

            UpdateTypeName();

            ContextLogger.OnContextInitialized += OnContextInitialized;
            ContextLogger.OnContextStateSet += OnContextStateSet;
            ContextLogger.OnSignalDispatch += OnSignalDispatched;
            ContextLogger.OnCommandExecute += OnCommandExecute;
            ContextLogger.OnCommandRelease += OnCommandRelease;
            ContextLogger.OnCommandAbort += OnCommandAbort;
            ContextLogger.OnCommandRevert += OnCommandRevert;
            ContextLogger.OnAbortCommandExecute += OnAbortCommandExecute;
            ContextLogger.OnAbortCommandRelease += OnAbortCommandRelease;
            ContextLogger.OnFinishCommandExecute += OnFinishCommandExecute;
            ContextLogger.OnFinishCommandRelease += OnFinishCommandRelease;
            ContextLogger.OnMediatorAdd += OnMediatorAdd;
            ContextLogger.OnMediatorRemove += OnMediatorRemove;
        }

        public void Deinitialize() {
            ContextLogger.OnContextInitialized -= OnContextInitialized;
            ContextLogger.OnContextStateSet -= OnContextStateSet;
            ContextLogger.OnSignalDispatch -= OnSignalDispatched;
            ContextLogger.OnCommandExecute -= OnCommandExecute;
            ContextLogger.OnCommandRelease -= OnCommandRelease;
            ContextLogger.OnCommandAbort -= OnCommandAbort;
            ContextLogger.OnCommandRevert -= OnCommandRevert;
            ContextLogger.OnAbortCommandExecute -= OnAbortCommandExecute;
            ContextLogger.OnAbortCommandRelease -= OnAbortCommandRelease;
            ContextLogger.OnFinishCommandExecute -= OnFinishCommandExecute;
            ContextLogger.OnFinishCommandRelease -= OnFinishCommandRelease;
            ContextLogger.OnMediatorAdd -= OnMediatorAdd;
            ContextLogger.OnMediatorRemove -= OnMediatorRemove;
        }

        public void AddChild(ContextWidget childContextWidget) {
            childContextWidgets.Add(childContextWidget);
            SortChildContextWidgets();
        }

        public void RemoveChild(ContextWidget childContextWidget) {
            childContextWidgets.Remove(childContextWidget);
            SortChildContextWidgets();
        }

        public void SetPosition(Vector2 position) {
            Position = position + Vector2.right * Width * .5f;
            RepositionWidgets();
        }

        public void RegenerateBindingWidgets() {
            UpdateTypeName();
            childContextWidgets.ForEach(x => x.RegenerateBindingWidgets());
            RegenerateWidgets();
        }

        public override void Update() {
            base.Update();
            childContextWidgets.ForEach(x => x.Update());
            injectionBindingWidgets.ForEach(x => x.Update());
            mediatorBindingWidgets.ForEach(x => x.Update());
            commandBindingWidgets.ForEach(x => x.Update());
        }

        public override void Render() {
            RenderChildContextLines(false);
            childContextWidgets.ForEach(x => x.Render());

            if (MonitorSettings.ShowInjectionBindings) {
                RenderBindingsTitle(injectionBindingWidgets, "Injections");
                injectionBindingWidgets.ForEach(x => x.Render());
            }

            if (MonitorSettings.ShowMediatorBindings) {
                RenderBindingsTitle(mediatorBindingWidgets, "Mediators");
                mediatorBindingWidgets.ForEach(x => x.Render());
            }

            if (MonitorSettings.ShowCommandBindings) {
                RenderBindingsTitle(commandBindingWidgets, "Commands");
                commandBindingWidgets.ForEach(x => x.Render());
            }

            if (MonitorSettings.ShowMediatorInstances) {
                RenderBindingsTitle(mediatorInstanceWidgets, "Mediator Instances");
                mediatorInstanceWidgets.ForEach(x => x.Render());
            }

            base.Render();
        }

        public override void RenderMiniMap() {
            RenderChildContextLines(true);
            childContextWidgets.ForEach(x => x.RenderMiniMap());

            if (MonitorSettings.ShowInjectionBindings) {
                injectionBindingWidgets.ForEach(x => x.RenderMiniMap());
            }

            if (MonitorSettings.ShowMediatorBindings) {
                mediatorBindingWidgets.ForEach(x => x.RenderMiniMap());
            }

            if (MonitorSettings.ShowCommandBindings) {
                commandBindingWidgets.ForEach(x => x.RenderMiniMap());
            }

            if (MonitorSettings.ShowMediatorInstances) {
                mediatorInstanceWidgets.ForEach(x => x.RenderMiniMap());
            }

            base.RenderMiniMap();
        }

        protected override string GetText() {
            return typeName;
        }

        protected override void ShowContextMenu() {
            GenericMenu menu = new GenericMenu();

            menu.AddItem(new GUIContent("Open " + typeName + ".cs"), false, delegate () {
                EditorHelper.OpenCodeOfType(Context.GetType());
            });

            menu.AddSeparator("");

            if (Application.isPlaying) {

                List<Type> allContextTypes = EditorHelper.GetAllDerivedTypes<Context>();
                for (int i = 0; i < allContextTypes.Count; i++) {
                    int j = i;
                    Type contextType = allContextTypes[j];

                    string contextName = EditorHelper.GetActualTypeName(contextType.Name);

                    bool isNameUnique = allContextTypes.Find(x => x != contextType && contextName == EditorHelper.GetActualTypeName(x.Name)) == null;
                    if (!isNameUnique) {
                        contextName = contextType.FullName;
                    }

                    menu.AddItem(new GUIContent("Switch Context/" + contextName), false, delegate () {
                        if (Context.IsRemoved) { return; }
                        Context.Switch(contextType);
                    });
                    menu.AddItem(new GUIContent("Goto State/" + contextName), false, delegate () {
                        if (Context.IsRemoved) { return; }
                        Context.SetContextState(contextType);
                    });
                    menu.AddItem(new GUIContent("Switch State/" + contextName), false, delegate () {
                        if (Context.IsRemoved) { return; }
                        Context.SwitchContextState(contextType);
                    });
                    menu.AddItem(new GUIContent("Add Context/" + contextName), false, delegate () {
                        if (Context.IsRemoved) { return; }
                        Context.InstantiateContext(contextType);
                    });
                }

                menu.AddItem(new GUIContent("Remove Context"), false, delegate () {
                    if (Context.IsRemoved) { return; }
                    Context.Remove();
                });

            } else {
                menu.AddDisabledItem(new GUIContent("Switch Context"));
                menu.AddDisabledItem(new GUIContent("Goto State"));
                menu.AddDisabledItem(new GUIContent("Switch State"));
                menu.AddDisabledItem(new GUIContent("Add Context"));
                menu.AddDisabledItem(new GUIContent("Remove Context"));
            }

            menu.ShowAsContext();
        }

        private void UpdateTypeName() {
            typeName = EditorHelper.GetActualTypeName(Context.GetType() , !MonitorSettings.ShowNamespaces);
        }

        private void RenderBindingsTitle(List<BindingWidget> bindingWidgets, string title) {
            if (bindingWidgets.Count == 0) { return; }
            Rect buttonRect = GetButtonRect();
            float offsetY = bindingWidgets[0].Position.y - Position.y;
            Rect injectionBindingsTitleRect = new Rect(buttonRect.position.x,
                                                       buttonRect.position.y + offsetY - 10,
                                                       buttonRect.width, buttonRect.height);
            EditorStyles.SetLabelStyle(EditorStyles.LabelStyle.BindingsTitle);
            GUI.Label(injectionBindingsTitleRect, title);
            EditorStyles.ResetLabelStyle();
        }

        private void RenderChildContextLines(bool inMiniMap) {
            if (childContextWidgets.Count == 0) { return; }

            Vector2 contextLineStartPosition = Position + Vector2.right * Width * .5f;
            Vector2 contextLineHalfwayPosition = contextLineStartPosition + Vector2.right * CONTEXT_MARGIN_HOR * .5f;

            if (inMiniMap) {
                RenderingHelper.RenderLineInMiniMap(contextLineStartPosition,
                                                    contextLineHalfwayPosition,
                                                    Color.white, 1);
            } else {
                bool hasState = stateContextWidget != null;
                Color color = hasState ? EditorStyles.ContextStateLineColor : EditorStyles.ContextChildLineColor;
                RenderingHelper.RenderLineInMonitorWindow(contextLineStartPosition,
                                                          contextLineHalfwayPosition,
                                                          color, 1);
            }

            foreach (ContextWidget childContextWidget in childContextWidgets) {
                Vector2 childContextLineEndPosition = childContextWidget.Position + Vector2.left * Width * .5f;
                Vector2 childContextLineHalfwayPosition = childContextLineEndPosition + Vector2.left * CONTEXT_MARGIN_HOR * .5f;

                if (inMiniMap) {
                    RenderingHelper.RenderLineInMiniMap(contextLineHalfwayPosition,
                                                        childContextLineHalfwayPosition,
                                                        Color.white, 1);
                    RenderingHelper.RenderLineInMiniMap(childContextLineHalfwayPosition,
                                                        childContextLineEndPosition,
                                                        Color.white, 1);
                } else {
                    bool isState = childContextWidget == stateContextWidget;
                    Color color = isState ? EditorStyles.ContextStateLineColor : EditorStyles.ContextChildLineColor;
                    RenderingHelper.RenderLineInMonitorWindow(contextLineHalfwayPosition,
                                                              childContextLineHalfwayPosition,
                                                              color, 1);
                    RenderingHelper.RenderArrowInMonitorWindow(childContextLineHalfwayPosition,
                                                               childContextLineEndPosition,
                                                               color, 1);

                    if (isState) {
                        Rect buttonRect = GetButtonRect();
                        Rect stateLabelRect = new Rect(buttonRect.position.x + Width - 5,
                                                       buttonRect.position.y,
                                                       CONTEXT_MARGIN_HOR, buttonRect.height);
                        EditorStyles.SetLabelStyle(EditorStyles.LabelStyle.StateTitle);
                        GUI.Label(stateLabelRect, "State");
                        EditorStyles.ResetLabelStyle();
                    }
                }
            }
        }

        private void SortChildContextWidgets() {
            childContextWidgets.Sort(delegate (ContextWidget a, ContextWidget b) {
                int isStateComparison = (a == stateContextWidget).CompareTo(b == stateContextWidget);
                if (isStateComparison != 0) { return isStateComparison; }
                return Prioritize.Compare(a.Context, b.Context);
            });
            RepositionWidgets();
        }

        private void RepositionWidgets() {
            float totalHeight = 0;
            foreach (ContextWidget widget in childContextWidgets) {
                widget.SetPosition(Position + Vector2.right * (Width * .5f + CONTEXT_MARGIN_HOR) + Vector2.up * totalHeight);
                totalHeight += widget.TotalHeight + CONTEXT_MARGIN_VER;
            }

            totalHeight = Height - 5 + BINDING_LIST_MARGIN_VER;

            Action<List<BindingWidget>> repositionBindingWidgets = delegate (List<BindingWidget> widgets) {
                foreach (BindingWidget widget in widgets) {
                    widget.SetPosition(Position + Vector2.up * totalHeight);
                    totalHeight += widget.TotalHeight + BINDING_MARGIN_VER;
                }
                if (widgets.Count > 0) {
                    totalHeight += BINDING_LIST_MARGIN_VER;
                }
            };

            if (MonitorSettings.ShowInjectionBindings) {
                repositionBindingWidgets(injectionBindingWidgets);
            }

            if (MonitorSettings.ShowMediatorBindings) {
                repositionBindingWidgets(mediatorBindingWidgets);
            }

            if (MonitorSettings.ShowCommandBindings) {
                repositionBindingWidgets(commandBindingWidgets);
            }

            if (MonitorSettings.ShowMediatorInstances) {
                repositionBindingWidgets(mediatorInstanceWidgets);
            }

            MonitorViewport.SetBoundingBoxDirty();
        }

        private void RegenerateWidgets() {
            GenerateInjectionBindingWidgets();
            GenerateMediatorBindingWidgets();
            GenerateCommandBindingWidgets();
            GenerateMediatorInstanceWidgets();

            RepositionWidgets();
        }

        private void GenerateInjectionBindingWidgets() {
            injectionBindingWidgets.Clear();

            List<Dictionary<Type, object>> allInjectionBindings = GetLabeledAndUnlabeledInjectionBindings();
            foreach (Dictionary<Type, object> injectionBindingList in allInjectionBindings) {
                foreach (KeyValuePair<Type, object> injectionBinding in injectionBindingList) {

                    if ((injectionBinding.Key == typeof(IContext) ||
                         injectionBinding.Key == typeof(EnterContextSignal) ||
                         injectionBinding.Key == typeof(LeaveContextSignal)) &&
                        !MonitorSettings.ShowDefaultBindings) {
                        continue;
                    }

                    List<BindingWidget> implementations = new List<BindingWidget>();
                    if (injectionBinding.Key != injectionBinding.Value.GetType()) {
                        BindingWidget objectBindingWidget = new BindingWidget(injectionBinding.Value.GetType(), Context);
                        implementations.Add(objectBindingWidget);
                    }

                    BindingWidget injectionBindingWidget = new BindingWidget(injectionBinding.Key, Context, implementations);
                    injectionBindingWidgets.Add(injectionBindingWidget);
                }
            }
            injectionBindingWidgets.Sort((x, y) => x.ImplementationCount.CompareTo(y.ImplementationCount));
        }

        private void GenerateMediatorBindingWidgets() {
            mediatorBindingWidgets.Clear();
            foreach (KeyValuePair<Type, Type> mediatorBinding in Context.MediatorBindings) {
                BindingWidget mediatorBindingWidget = new BindingWidget(mediatorBinding.Value, Context);
                BindingWidget mediatorWidget = new BindingWidget(mediatorBinding.Key, Context, new List<BindingWidget>() { mediatorBindingWidget });
                mediatorBindingWidgets.Add(mediatorWidget);
            }
        }

        private void GenerateCommandBindingWidgets() {
            commandBindingWidgets.Clear();

            foreach (KeyValuePair<AbstractSignal, List<SignalResponse>> commandBinding in Context.CommandBindings) {
                foreach (SignalResponse signalResponse in commandBinding.Value) {
                    List<BindingWidget> commandWidgets = new List<BindingWidget>();

                    List<AbstractCommand> allCommands = new List<AbstractCommand>(signalResponse.Commands);
                    if (signalResponse.AbortCommand != null) {
                        allCommands.Add(signalResponse.AbortCommand);
                    }
                    if (signalResponse.FinishCommand != null) {
                        allCommands.Add(signalResponse.FinishCommand);
                    }

                    BindingWidget abortCommandWidget = null;
                    BindingWidget finishCommandWidget = null;

                    foreach (AbstractCommand command in allCommands) {
                        BindingWidget commandWidget = null;

                        if (command == signalResponse.AbortCommand) {
                            string displayText = "OnAbort: " + EditorHelper.GetActualTypeName(command.GetType());
                            commandWidget = new BindingWidget(command.GetType(), Context, displayText);
                            abortCommandWidget = commandWidget;
                        } else if (command == signalResponse.FinishCommand) {
                            string displayText = "OnFinish: " + EditorHelper.GetActualTypeName(command.GetType());
                            commandWidget = new BindingWidget(command.GetType(), Context, displayText);
                            finishCommandWidget = commandWidget;
                        } else {
                            Type commandType = command.GetType();
                            if (commandType.IsGenericType) {

                                Type genericType = commandType.GetGenericTypeDefinition();
                                Type genericArgumentType = commandType.GetGenericArguments()[0];
                                string genericArgumentTypeName = EditorHelper.GetActualTypeName(genericArgumentType.Name);

                                string displayText = null;

                                if (genericType == typeof(DispatchSignalCommand<>)) {
                                    displayText = "Dispatch: " + genericArgumentTypeName;
                                } else if (genericType == typeof(GotoStateCommand<>)) {
                                    displayText = "GotoState: " + genericArgumentTypeName;
                                } else if (genericType == typeof(SwitchStateCommand<>)) {
                                    displayText = "SwitchState: " + genericArgumentTypeName;
                                } else if (genericType == typeof(SwitchContextCommand<>)) {
                                    displayText = "SwitchContext: " + genericArgumentTypeName;
                                } else if (genericType == typeof(AddContextCommand<>)) {
                                    displayText = "AddContext: " + genericArgumentTypeName;
                                } else if (genericType == typeof(InstantiateViewCommand<>)) {
                                    displayText = "Instantiate: " + genericArgumentTypeName;
                                }

                                commandWidget = new BindingWidget(genericArgumentType, Context, displayText);
                            }
                        }

                        if (commandWidget == null) {
                            commandWidget = new BindingWidget(command.GetType(), Context);
                        }

                        commandWidgets.Add(commandWidget);
                    }

                    Type signalType = commandBinding.Key.GetType();
                    CommandBindingWidget commandBindingWidget = new CommandBindingWidget(signalType, commandWidgets, Context, abortCommandWidget, finishCommandWidget);
                    commandBindingWidgets.Add(commandBindingWidget);
                }
            }
        }

        private void GenerateMediatorInstanceWidgets() {
            mediatorInstanceWidgets.Clear();
            foreach (Mediator mediator in mediatorInstances) {
                MediatorInstanceWidget widget = new MediatorInstanceWidget(mediator, Context);
                mediatorInstanceWidgets.Add(widget);
            }
        }

        private List<Dictionary<Type, object>> GetLabeledAndUnlabeledInjectionBindings() {
            List<Dictionary<Type, object>> allInjectionBindings = new List<Dictionary<Type, object>>();

            Dictionary<Type, object> injectionBindings = new Dictionary<Type, object>();
            foreach (KeyValuePair<Type, object> injectionBinding in Context.InjectionBindings) {
                injectionBindings.Add(injectionBinding.Key, injectionBinding.Value);
            }
            allInjectionBindings.Add(injectionBindings);

            foreach (KeyValuePair<string, Dictionary<Type, object>> labeledBindings in Context.LabeledInjectionBindings) {
                Dictionary<Type, object> labeledInjectionBindings = new Dictionary<Type, object>();
                foreach (KeyValuePair<Type, object> injectionBinding in labeledBindings.Value) {
                    labeledInjectionBindings.Add(injectionBinding.Key, injectionBinding.Value);
                }
                allInjectionBindings.Add(labeledInjectionBindings);
            }

            return allInjectionBindings;
        }

        private void OnContextInitialized(Context context) {
            if (context != Context) { return; }
            RegenerateWidgets();
            Pulse(EditorStyles.PulseContextInitializedColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnContextStateSet(Context context, IContext stateContext) {
            if (context != Context) { return; }
            stateContextWidget = childContextWidgets.Find(x => x.Context == stateContext);
            SortChildContextWidgets();
            ContextLogger.NotifyLogReaction();
        }

        private void OnSignalDispatched(AbstractSignal signal) {
            Type keyType = null;
            foreach (KeyValuePair<Type, object> injectionBinding in Context.InjectionBindings) {
                if (injectionBinding.Value != signal) { continue; }
                keyType = injectionBinding.Key;
                break;
            }
            if (keyType == null) {
                foreach (KeyValuePair<string, Dictionary<Type, object>> labeledBindings in Context.LabeledInjectionBindings) {
                    foreach (KeyValuePair<Type, object> injectionBinding in labeledBindings.Value) {
                        if (injectionBinding.Value != signal) { continue; }
                        keyType = injectionBinding.Key;
                        break;
                    }
                    if (keyType != null) {
                        break;
                    }
                }
            }
            if (keyType != null) {
                BindingWidget injectionBindingWidget = injectionBindingWidgets.Find(x => x.KeyType == keyType);
                if (injectionBindingWidget != null) {
                    injectionBindingWidget.Pulse(EditorStyles.PulseSignalDispatchColor);
                    ContextLogger.NotifyLogReaction();
                }
            }
            
            foreach (KeyValuePair<AbstractSignal, List<SignalResponse>> commandBinding in Context.CommandBindings) {
                if (commandBinding.Key != signal) { continue; }
                List<BindingWidget> commandBindingWidgets = this.commandBindingWidgets.FindAll(x => x.KeyType == signal.GetType());
                foreach (BindingWidget commandBindingWidget in commandBindingWidgets) {
                    commandBindingWidget.Pulse(EditorStyles.PulseSignalDispatchColor);
                    ContextLogger.NotifyLogReaction();
                }
            }
        }

        private void OnCommandExecute(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            bindingWidget.PulseImplementation(commandIndex, EditorStyles.PulseCommandExecuteColor, true);
            ContextLogger.NotifyLogReaction();
        }

        private void OnCommandRelease(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            bindingWidget.PulseImplementation(commandIndex, EditorStyles.PulseCommandReleaseColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnCommandAbort(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            bindingWidget.PulseImplementation(commandIndex, EditorStyles.PulseCommandAbortColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnCommandRevert(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            bindingWidget.PulseImplementation(commandIndex, EditorStyles.PulseCommandRevertColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnAbortCommandExecute(Context context, AbstractSignal signal, int responseIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            (bindingWidget as CommandBindingWidget).AbortCommandWidget.Pulse(EditorStyles.PulseCommandExecuteColor, true);
            ContextLogger.NotifyLogReaction();
        }

        private void OnAbortCommandRelease(Context context, AbstractSignal signal, int responseIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            (bindingWidget as CommandBindingWidget).AbortCommandWidget.Pulse(EditorStyles.PulseCommandReleaseColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnFinishCommandExecute(Context context, AbstractSignal signal, int responseIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            (bindingWidget as CommandBindingWidget).FinishCommandWidget.Pulse(EditorStyles.PulseCommandExecuteColor, true);
            ContextLogger.NotifyLogReaction();
        }

        private void OnFinishCommandRelease(Context context, AbstractSignal signal, int responseIndex) {
            if (context != Context) { return; }
            BindingWidget bindingWidget = GetCommandBindingWidget(signal, responseIndex);
            if (bindingWidget == null) { return; }
            (bindingWidget as CommandBindingWidget).FinishCommandWidget.Pulse(EditorStyles.PulseCommandReleaseColor);
            ContextLogger.NotifyLogReaction();
        }

        private void OnMediatorAdd(Context context, Mediator mediator) {
            if (context != Context) { return; }
            mediatorInstances.Add(mediator);
            if (!MonitorSettings.ShowMediatorInstances) { return; }
            GenerateMediatorInstanceWidgets();
            MonitorViewport.SetBoundingBoxDirty();
            ContextLogger.NotifyLogReaction();
        }

        private void OnMediatorRemove(Context context, Mediator mediator) {
            if (context != Context) { return; }
            mediatorInstances.Remove(mediator);
            if (!MonitorSettings.ShowMediatorInstances) { return; }
            GenerateMediatorInstanceWidgets();
            MonitorViewport.SetBoundingBoxDirty();
            ContextLogger.NotifyLogReaction();
        }
        
        private BindingWidget GetCommandBindingWidget(AbstractSignal signal, int responseIndex) {
            List<BindingWidget> bindingWidgets = commandBindingWidgets.FindAll(x => x.KeyType == signal.GetType());
            if (responseIndex >= bindingWidgets.Count) { return null; }
            return bindingWidgets[responseIndex];
        }

    }

}