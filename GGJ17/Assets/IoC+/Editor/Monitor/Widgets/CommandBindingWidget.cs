/// <copyright file="CommandBindingWidget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;

namespace IoCPlus.Editor {

    public class CommandBindingWidget : BindingWidget {

        public BindingWidget AbortCommandWidget;
        public BindingWidget FinishCommandWidget;

        public CommandBindingWidget(Type keyType, List<BindingWidget> implementations,
                                    Context context, BindingWidget abortCommandWidget, BindingWidget finishCommandWidget)
            : base(keyType, context, implementations, null) {
            AbortCommandWidget = abortCommandWidget;
            FinishCommandWidget = finishCommandWidget;
        }

    }

}