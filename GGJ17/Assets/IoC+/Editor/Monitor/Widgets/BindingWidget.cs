/// <copyright file="BindingWidget.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace IoCPlus.Editor {

    public class BindingWidget : ButtonWidget {

        public float TotalHeight {
            get {
                float totalHeight = Height;
                implementations.ForEach(x => totalHeight += x.TotalHeight + MARGIN_VERT);
                return totalHeight;
            }
        }

        public int ImplementationCount { get { return implementations.Count; } }

        public readonly Type KeyType;

        protected readonly Context context;
        protected readonly string typeName;

        private const float HEIGHT = 20.0f;
        private const float INDENTATION = 10.0f;
        private const float MARGIN_VERT = 0.0f;

        private readonly string displayText;
        private readonly List<BindingWidget> implementations;

        public BindingWidget(Type keyType, Context context) : this(keyType, context, new List<BindingWidget>()) { }
        public BindingWidget(Type keyType, Context context, string displayText) : this(keyType, context, new List<BindingWidget>(), displayText) { }
        public BindingWidget(Type keyType, Context context, List<BindingWidget> implementations) : this(keyType, context, implementations, null) { }
        public BindingWidget(Type keyType, Context context, List<BindingWidget> implementations, string displayText) : base() {
            Height = HEIGHT;

            KeyType = keyType;

            this.context = context;
            typeName = EditorHelper.GetActualTypeName(KeyType, !MonitorSettings.ShowNamespaces);

            if (string.IsNullOrEmpty(displayText)) {
                this.displayText = typeName;
            } else {
                this.displayText = displayText;
            }

            this.implementations = implementations;
        }

        public void SetPosition(Vector2 position) {
            Position = position;
            RepositionWidgets();
        }

        public void PulseImplementation(int index, Color pulseColor, bool forcePulse = false) {
            if (index >= implementations.Count) { return; }
            implementations[index].Pulse(pulseColor, forcePulse);
        }

        public override void Update() {
            RepositionWidgets();
            implementations.ForEach(x => x.Update());
            base.Update();
        }

        public override void Render() {
            implementations.ForEach(x => x.Render());
            base.Render();
        }

        public override void RenderMiniMap() {
            implementations.ForEach(x => x.RenderMiniMap());
            base.RenderMiniMap();
        }

        protected override void ShowContextMenu() {
            GenericMenu menu = new GenericMenu();

            menu.AddItem(new GUIContent("Open " + typeName + ".cs"), false, delegate () {
                EditorHelper.OpenCodeOfType(KeyType);
            });

            if (KeyType.IsSubclassOf(typeof(AbstractSignal))) {
                menu.AddSeparator(null);
                if (Application.isPlaying && KeyType.IsSubclassOf(typeof(Signal))) {
                    menu.AddItem(new GUIContent("Dispatch " + typeName), false, delegate () {
                        Dictionary<Type, object> injectionBindings = context.GetCulumativeInjectionBindings();
                        if (!injectionBindings.ContainsKey(KeyType)) { return; }
                        if (!KeyType.IsSubclassOf(typeof(Signal))) { return; }
                        Signal signal = injectionBindings[KeyType] as Signal;
                        signal.Dispatch();
                    });
                } else {
                    menu.AddDisabledItem(new GUIContent("Dispatch " + typeName));
                }
            }

            menu.ShowAsContext();
        }

        protected override string GetText() {
            return displayText;
        }

        private void RepositionWidgets() {
            float totalHeight = Height;
            foreach (BindingWidget widget in implementations) {
                widget.SetPosition(Position + Vector2.right * INDENTATION + Vector2.up * totalHeight);
                totalHeight += widget.TotalHeight + MARGIN_VERT;
            }
        }

    }

}