/// <copyright file="AssetCreationHelper.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections.Generic;

namespace IoCPlus.Editor {

    public class AssetCreationHelper : UnityEditor.Editor {

        private enum AssetCreationType {
            AddToDirectory,
            RewriteAsset
        }

        private enum AssetQuery {
            ScriptFilesOnly,
            DirectoriesOnly
        }

        [MenuItem("Assets/Create/IoC+/Empty Signal")]
        public static void CreateEmptySignal() {
            if (GetSelectedPaths(AssetQuery.ScriptFilesOnly) == null) { return; }
            RewriteTextAssetAtSelectedScripts("SignalTemplate");
        }

        [MenuItem("Assets/Create/IoC+/Empty Command")]
        public static void CreateEmptyCommand() {
            if (GetSelectedPaths(AssetQuery.ScriptFilesOnly) == null) { return; }
            RewriteTextAssetAtSelectedScripts("CommandTemplate");
        }

        [MenuItem("Assets/Create/IoC+/View and Mediator")]
        public static void CreateViewAndMediatorAssets() {
            if (GetSelectedPaths(AssetQuery.DirectoriesOnly) == null) { return; }
            CreateTextAssetAtSelectedDirectories("MediatorTemplate", "", "Mediator");
            CreateTextAssetAtSelectedDirectories("ViewTemplate", "", "View");
        }

        [MenuItem("Assets/Create/IoC+/View, IView and Mediator")]
        public static void CreateViewAndIViewAndMediatorAssets() {
            if (GetSelectedPaths(AssetQuery.DirectoriesOnly) == null) { return; }
            CreateTextAssetAtSelectedDirectories("ViewOfIViewTemplate", "", "View");
            CreateTextAssetAtSelectedDirectories("MediatorOfIViewTemplate", "", "Mediator");
            CreateTextAssetAtSelectedDirectories("IViewTemplate", "I", "View");
        }

        [MenuItem("Assets/Create/IoC+/Context")]
        public static void CreateContextAssets() {
            CreateTextAssetAtSelectedDirectories("ContextTemplate", "", "Context");
            CreateTextAssetAtSelectedDirectories("ContextRootTemplate", "", "Root");
            CreateDirectoryAtSelectedDirectories("Signals");
            CreateDirectoryAtSelectedDirectories("Commands");
            CreateDirectoryAtSelectedDirectories("Contexts");
            CreateDirectoryAtSelectedDirectories("Views");
        }

        [MenuItem("Assets/Create/IoC+/View and Mediator", true)]
        [MenuItem("Assets/Create/IoC+/View, IView and Mediator", true)]
        [MenuItem("Assets/Create/IoC+/Context", true)]
        public static bool CreateDirectoryAssetsValidator() {
            return GetSelectedPaths(AssetQuery.DirectoriesOnly, false).Count > 0;
        }

        [MenuItem("Assets/Create/IoC+/Empty Signal", true)]
        [MenuItem("Assets/Create/IoC+/Empty Command", true)]
        public static bool CreateScriptAssetsValidator() {
            return GetSelectedPaths(AssetQuery.ScriptFilesOnly, false).Count > 0;
        }

        private static void CreateTextAssetAtSelectedDirectories(string templatePath, string resultFilenamePrefix, string resultFilenamePostfix) {
            List<string> paths = GetSelectedPaths(AssetQuery.DirectoriesOnly);
            foreach (string path in paths) {
                CreateTextAsset(path, templatePath, resultFilenamePrefix, resultFilenamePostfix, AssetCreationType.AddToDirectory);
            }
        }

        private static void RewriteTextAssetAtSelectedScripts(string templatePath) {
            List<string> paths = GetSelectedPaths(AssetQuery.ScriptFilesOnly);
            foreach (string path in paths) {
                CreateTextAsset(path, templatePath, string.Empty, string.Empty, AssetCreationType.RewriteAsset);
                AssetDatabase.ImportAsset(path, ImportAssetOptions.ForceUpdate);
            }
        }

        private static void CreateTextAsset(string path, string templatePath, string resultFilenamePrefix, string resultFilenamePostfix, AssetCreationType creationType) {
            TextAsset templateAsset = Resources.Load<TextAsset>(templatePath);
            if (templateAsset == null) {
                Debug.LogError("Could not find code file template!");
                return;
            }

            string[] directories = path.Split('/');
            string selectionName = directories.Length == 0 ? path : directories[directories.Length - 1];

            if (selectionName.EndsWith(".cs")) {
                selectionName = selectionName.Substring(0, selectionName.Length - 3);
            }
                    
            string template = templateAsset.text;
            string render = template.Replace("{{name}}", selectionName);
            
            if (creationType == AssetCreationType.AddToDirectory) {
                string filename = resultFilenamePrefix + selectionName + resultFilenamePostfix + ".cs";
                File.WriteAllText(path + "/" + filename, render);
            } else {
                File.WriteAllText(path, render);
            }
        }

        private static void CreateDirectoryAtSelectedDirectories(string directoryName) {
            List<string> paths = GetSelectedPaths(AssetQuery.DirectoriesOnly);
            if (paths.Count == 0) { return; }
            foreach (string path in paths) {
                string directoryPath = path + "/" + directoryName;
                if (Directory.Exists(directoryPath)) { return; }
                Directory.CreateDirectory(directoryPath);
            }
            AssetDatabase.Refresh();
        }        

        private static List<string> GetSelectedPaths(AssetQuery query, bool showWarning = true) {
            if (Selection.instanceIDs.Length == 0) {
                if (showWarning) {
                    Debug.LogWarning("Selected object is not valid");
                }
                return null;
            }

            List<string> paths = new List<string>();
            foreach (int instanceId in Selection.instanceIDs) {
                string path = AssetDatabase.GetAssetPath(instanceId);
                if (path.Length == 0) { continue; }
                if (query == AssetQuery.ScriptFilesOnly && !path.EndsWith(".cs")) { continue; }
                if (query == AssetQuery.DirectoriesOnly && !Directory.Exists(path)) { continue; }
                paths.Add(path);
            }

            if (paths.Count == 0) {
                if (showWarning) {
                    Debug.LogWarning("Selected object is not valid");
                }
            }
            
            return paths;
        }

    }

}