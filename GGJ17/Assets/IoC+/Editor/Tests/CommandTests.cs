/// <copyright file="CommandTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;

namespace IoCPlus {

    public class CommandTests {

        private class TestTypes {
            public class EmptyCommand : Command { }
            public class AbortCommand : Command {
                protected override void Execute() {
                    Abort();
                }
            }
            public class OneExecuteParameterCommand : Command<string> {
                public string Parameter1 { get; private set; }
                protected override void Execute(string parameter1) {
                    Parameter1 = parameter1;
                }
                protected override void Revert(string parameter1) {
                    Parameter1 = parameter1;
                }
            }
            public class TwoExecuteParametersCommand : Command<string, int> {
                public string Parameter1 { get; private set; }
                public int Parameter2 { get; private set; }
                protected override void Execute(string parameter1, int parameter2) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                }
                protected override void Revert(string parameter1, int parameter2) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                }
            }
            public class ThreeExecuteParametersCommand : Command<string, int, Command> {
                public string Parameter1 { get; private set; }
                public int Parameter2 { get; private set; }
                public Command Parameter3 { get; private set; }
                protected override void Execute(string parameter1, int parameter2, Command parameter3) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                    Parameter3 = parameter3;
                }
                protected override void Revert(string parameter1, int parameter2, Command parameter3) {
                    Parameter1 = parameter1;
                    Parameter2 = parameter2;
                    Parameter3 = parameter3;
                }
            }
        }

        [Test]
        public void EmptyCommandReleases_OnExecute() {
            //Arrange
            int releaseCount = 0;

            TestTypes.EmptyCommand command = new TestTypes.EmptyCommand();
            command.OnRelease = delegate {
                releaseCount++;
            };

            //Act
            command.PerformExecution();

            //Assert
            Assert.AreEqual(1, releaseCount);
        }

        [Test]
        public void AbortCommandReleases_OnExecute() {
            //Arrange
            int releaseCount = 0;

            TestTypes.AbortCommand command = new TestTypes.AbortCommand();
            command.OnRelease = delegate {
                releaseCount++;
            };

            //Act
            command.PerformExecution();

            //Assert
            Assert.AreEqual(1, releaseCount);
        }

        [Test]
        public void AbortCommandAborts_OnExecute() {
            //Arrange
            int abortCount = 0;

            TestTypes.AbortCommand command = new TestTypes.AbortCommand();
            command.OnAbort = delegate {
                abortCount++;
            };

            //Act
            command.PerformExecution();

            //Assert
            Assert.AreEqual(1, abortCount);
        }

        [Test]
        public void EmptyCommandReleases_OnRevert() {
            //Arrange
            int releaseCount = 0;

            TestTypes.EmptyCommand command = new TestTypes.EmptyCommand();
            command.OnRelease = delegate {
                releaseCount++;
            };

            //Act
            command.PerformRevertion();

            //Assert
            Assert.AreEqual(1, releaseCount);
        }

        [Test]
        [TestCase("test")]
        [TestCase("another test")]
        public void CommandWithOneExecuteParameterExecutesWithParameter_OnExecute(string parameter) {
            //Arrange
            TestTypes.OneExecuteParameterCommand command = new TestTypes.OneExecuteParameterCommand();
            command.SetParameters(parameter);

            //Assert
            Assert.That(command.Parameter1 == null);

            //Act
            command.PerformExecution();

            //Assert
            Assert.That(command.Parameter1 == parameter);
        }

        [Test]
        [TestCase("test")]
        [TestCase("another test")]
        public void CommandWithOneExecuteParameterRevertsWithParameter_OnExecute(string parameter) {
            //Arrange
            TestTypes.OneExecuteParameterCommand command = new TestTypes.OneExecuteParameterCommand();
            command.SetParameters(parameter);

            //Assert
            Assert.That(command.Parameter1 == null);

            //Act
            command.PerformRevertion();

            //Assert
            Assert.That(command.Parameter1 == parameter);
        }

        [Test]
        [TestCase("test", 0)]
        [TestCase("another test", 1337)]
        public void CommandWithTwoExecuteParametersExecutesWithParameter_OnExecute(string parameter1, int parameter2) {
            //Arrange
            TestTypes.TwoExecuteParametersCommand command = new TestTypes.TwoExecuteParametersCommand();
            command.SetParameters(parameter1, parameter2);

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);

            //Act
            command.PerformExecution();

            //Assert
            Assert.That(command.Parameter1 == parameter1);
            Assert.That(command.Parameter2 == parameter2);
        }

        [Test]
        [TestCase("test", 0)]
        [TestCase("another test", 1337)]
        public void CommandWithTwoExecuteParametersRevertsWithParameter_OnExecute(string parameter1, int parameter2) {
            //Arrange
            TestTypes.TwoExecuteParametersCommand command = new TestTypes.TwoExecuteParametersCommand();
            command.SetParameters(parameter1, parameter2);

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);

            //Act
            command.PerformRevertion();

            //Assert
            Assert.That(command.Parameter1 == parameter1);
            Assert.That(command.Parameter2 == parameter2);
        }

        [Test]
        [TestCase("test", 0)]
        [TestCase("another test", 1337)]
        public void CommandWithThreeExecuteParametersExecutesWithParameter_OnExecute(string parameter1, int parameter2) {
            //Arrange
            TestTypes.ThreeExecuteParametersCommand command = new TestTypes.ThreeExecuteParametersCommand();

            Command parameter3 = new TestTypes.AbortCommand();
            command.SetParameters(parameter1, parameter2, parameter3);

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);
            Assert.That(command.Parameter3 == null);

            //Act
            command.PerformExecution();

            //Assert
            Assert.That(command.Parameter1 == parameter1);
            Assert.That(command.Parameter2 == parameter2);
            Assert.That(command.Parameter3 == parameter3);
        }

        [Test]
        [TestCase("test", 0)]
        [TestCase("another test", 1337)]
        public void CommandWithThreeExecuteParametersRevertsWithParameter_OnExecute(string parameter1, int parameter2) {
            //Arrange
            TestTypes.ThreeExecuteParametersCommand command = new TestTypes.ThreeExecuteParametersCommand();

            Command parameter3 = new TestTypes.EmptyCommand();
            command.SetParameters(parameter1, parameter2, parameter3);

            //Assert
            Assert.That(command.Parameter1 == null);
            Assert.That(command.Parameter2 == 0);
            Assert.That(command.Parameter3 == null);

            //Act
            command.PerformRevertion();

            //Assert
            Assert.That(command.Parameter1 == parameter1);
            Assert.That(command.Parameter2 == parameter2);
            Assert.That(command.Parameter3 == parameter3);
        }

    }

}