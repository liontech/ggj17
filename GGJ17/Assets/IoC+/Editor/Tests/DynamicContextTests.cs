/// <copyright file="DynamicContextTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;

namespace IoCPlus {

    public class DynamicContextTests {

        const string TEST_LABEL = "label";

        private interface ITestInterface { }
        private class TestClass : ITestInterface { }
        private class TestCommand : Command {
            public string BasicInjection { get { return basicInjection; } }
            public ITestInterface LabeledInjection { get { return labeledInjection; } }
            public string ParameterInjection { get { return parameterInjection; } }

            [Inject] string basicInjection;
            [Inject(TEST_LABEL)] ITestInterface labeledInjection;
            [InjectParameter] string parameterInjection;
        }

        [Test]
        public void DynamicContextInjectsInjectionBinding_OnInjectInto() {
            //Arrange
            const string testValue = "test";

            TestCommand command = new TestCommand();

            DynamicContext dynamicContext = new DynamicContext();
            dynamicContext.Bind<string>(testValue);

            //Assert
            Assert.That(command.BasicInjection != testValue);

            //Act
            dynamicContext.InjectInto(command);

            //Assert
            Assert.That(command.BasicInjection == testValue);
        }

        [Test]
        public void DynamicContextInjectsLabeledInjectionBinding_OnInjectInto() {
            //Arrange
            TestCommand command = new TestCommand();

            DynamicContext dynamicContext = new DynamicContext();
            ITestInterface instance = dynamicContext.BindLabeled<ITestInterface, TestClass>(TEST_LABEL);

            //Assert
            Assert.That(command.LabeledInjection != instance);

            //Act
            dynamicContext.InjectInto(command);

            //Assert
            Assert.That(command.LabeledInjection == instance);
        }

        [Test]
        public void DynamicContextInjectsParameterInjectionBinding_OnInjectInto() {
            //Arrange
            const string testValue = "test";

            TestCommand command = new TestCommand();

            DynamicContext dynamicContext = new DynamicContext();
            dynamicContext.BindParameter<string>(testValue);

            //Assert
            Assert.That(command.ParameterInjection != testValue);

            //Act
            dynamicContext.InjectInto(command);

            //Assert
            Assert.That(command.ParameterInjection == testValue);
        }

        [Test]
        public void DynamicContextInjectsAllInjectionBindingTypes_OnInjectInto() {
            //Arrange
            const string basicInjectionValue = "test1";
            const string parameterInjectionValue = "test2";

            TestCommand command = new TestCommand();

            DynamicContext dynamicContext = new DynamicContext();

            dynamicContext.Bind<string>(basicInjectionValue);
            ITestInterface labeledInjectionValue = dynamicContext.BindLabeled<ITestInterface, TestClass>(TEST_LABEL);
            dynamicContext.BindParameter<string>(parameterInjectionValue);

            //Assert
            Assert.That(command.ParameterInjection != basicInjectionValue);

            //Act
            dynamicContext.InjectInto(command);

            //Assert
            Assert.That(command.BasicInjection == basicInjectionValue);
            Assert.That(command.LabeledInjection == labeledInjectionValue);
            Assert.That(command.ParameterInjection == parameterInjectionValue);
        }

    }

}