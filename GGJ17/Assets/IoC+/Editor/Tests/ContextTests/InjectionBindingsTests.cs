/// <copyright file="InjectionBindingsTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;
using System.Collections.Generic;
using System;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class InjectionBindingsTests {

        private class TestContext : Context { }
        private class TestTypes {
            public interface IService { }
            public class Service : IService { }
            public class AlternativeService : IService { }
            public class TestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<IService, Service>();
                }
            }
            public class AlternativeTestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<IService, AlternativeService>();
                }
            }
            public class InjectBindingInSetBindingsContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<IService, Service>();
                }
            }
            public class LabeledInjectBindingInSetBindingsContext : Context {
                public const string LABEL = "label";
                protected override void SetBindings() {
                    base.SetBindings();
                    BindLabeled<IService, Service>(LABEL);
                }
            }
        }

        [Test]
        public void ContextHasNoBindings_OnConstruction() {
            //Act
            TestContext context = new TestContext();

            //Assert
            Assert.That(context.InjectionBindings.Count() == 0);
        }

        [Test]
        public void ContextBindsIContext_OnInitialize() {
            //Arrange
            TestContext context = new TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.InjectionBindings.Any(x => x.Key == typeof(IContext)));
        }

        [Test]
        public void ContextBindsEnterContextSignal_OnInitialize() {
            //Arrange
            TestContext context = new TestContext();

            //Act
            context.Initialize();

            //Assert

            Assert.That(context.InjectionBindings.Any(x => x.Key == typeof(EnterContextSignal)));
        }

        [Test]
        public void ContextBindsLeaveContextSignal_OnInitialize() {
            //Arrange
            TestContext context = new TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.InjectionBindings.Any(x => x.Key == typeof(LeaveContextSignal)));
        }

        [Test]
        public void ContextSubclassBindsService_OnInitialize() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.InjectionBindings.Any(x => x.Key == typeof(TestTypes.IService) &&
                                                           x.Value.GetType() == typeof(TestTypes.Service)));
        }

        [Test]
        public void ContextOverwritesParentBinding_OnSetParent() {
            //Arrange
            TestTypes.TestContext parentContext = new TestTypes.TestContext();
            TestTypes.AlternativeTestContext childContext = new TestTypes.AlternativeTestContext();

            parentContext.Initialize();
            childContext.Initialize();

            //Act
            childContext.SetParent(parentContext);

            //Assert
            Dictionary<Type, object> injectionBindings = childContext.GetCulumativeInjectionBindings();
            Type interfaceType = typeof(TestTypes.IService);
            Assert.That(injectionBindings.ContainsKey(interfaceType));
            Assert.That(injectionBindings[interfaceType].GetType() == typeof(TestTypes.AlternativeService));
        }

        [Test]
        public void ContextSetsInjectionBindingInSetBindings_OnInitialize() {
            //Arrange
            TestTypes.InjectBindingInSetBindingsContext context = new TestTypes.InjectBindingInSetBindingsContext();

            //Assert
            Assert.That(context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.IService)).Value == null);

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(TestTypes.IService)).Value != null);
        }

        [Test]
        public void ContextSetsLabeledInjectionBindingInSetBindings_OnInitialize() {
            //Arrange
            TestTypes.LabeledInjectBindingInSetBindingsContext context = new TestTypes.LabeledInjectBindingInSetBindingsContext();
            string label = TestTypes.LabeledInjectBindingInSetBindingsContext.LABEL;

            //Assert
            Assert.That(context.LabeledInjectionBindings.FirstOrDefault(x => x.Key == label).Value == null);

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.LabeledInjectionBindings.FirstOrDefault(x => x.Key == label).Value != null);
        }

    }

}