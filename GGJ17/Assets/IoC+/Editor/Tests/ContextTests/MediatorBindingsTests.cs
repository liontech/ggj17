/// <copyright file="MediatorBindingsTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using NUnit.Framework;
using UnityEngine;
using NSubstitute;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class MediatorBindingsTests {

        private class TestTypes {
            public class TestView : View, IView { }
            public class TestMediator : Mediator<TestView> {
                public override void Initialize() { }
                public override void Dispose() { }
            }
            public class AlternativeMediator : Mediator<TestView> {
                public override void Initialize() { }
                public override void Dispose() { }
            }
            public class NoBindContext : Context { }
            public class BindMediatorInSetBindingsContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    BindMediator<TestMediator, TestView>();
                }
            }
            public class ParentContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    BindMediator<AlternativeMediator, TestView>();
                }
            }

        }

        [Test]
        public void ContextBindsMediatorToView_OnInitialize() {
            //Arrange
            TestTypes.BindMediatorInSetBindingsContext context = new TestTypes.BindMediatorInSetBindingsContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.MediatorBindings.Any(x => x.Key == typeof(TestTypes.TestView) &&
                                                          x.Value == typeof(TestTypes.TestMediator)));
            Assert.That(context.MediatorBindings.Count() == 1);
        }

        [Test]
        public void ChildContextBindingOverwritesParentBinding_OnInitialize() {
            //Arrange
            TestTypes.ParentContext parentContext = new TestTypes.ParentContext();
            TestTypes.BindMediatorInSetBindingsContext context = new TestTypes.BindMediatorInSetBindingsContext();

            parentContext.Initialize();
            context.SetParent(parentContext);

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.MediatorBindings.Any(x => x.Key == typeof(TestTypes.TestView) &&
                                                          x.Value == typeof(TestTypes.TestMediator)));
            Assert.That(context.MediatorBindings.Count() == 1);
        }

    }

}