/// <copyright file="DispatchSignalsTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class DispatchSignalsTests {

        private class TestTypes {
            public class TestContext : Context { }
        }

        [Test]
        public void ContextDispatchesEnterContextSignal_OnInitialize() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            KeyValuePair<Type, object> signalInjectionBinding = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(EnterContextSignal));
            EnterContextSignal signal = signalInjectionBinding.Value as EnterContextSignal;
            Assert.That(signal.DispatchCount == 1);
        }

        [Test]
        public void ContextDispatchesLeaveContextSignal_OnRemove() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();
            context.Initialize();

            KeyValuePair<Type, object> signalInjectionBinding = context.InjectionBindings.FirstOrDefault(x => x.Key == typeof(LeaveContextSignal));
            LeaveContextSignal signal = signalInjectionBinding.Value as LeaveContextSignal;

            //Assert
            Assert.That(signal.DispatchCount == 0);

            //Act
            context.Remove();

            //Assert
            Assert.That(signal.DispatchCount == 1);
        }

    }

}