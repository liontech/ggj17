/// <copyright file="ParentChildTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;
using NSubstitute;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class ParentChildTests {

        private class TestContext : Context { }
        private class AlternativeTestContext : Context { }

        [Test]
        public void ContextHasNoChild_OnInitialize() {
            //Arrange
            Context context = new TestContext();

            //Act
            context.Initialize();

            //Assert
            Assert.That(context.Children.Count() == 0);
        }

        [Test]
        public void ContextAddsChildContext_OnInstantiateContextGeneric() {
            //Arrange
            Context context = Substitute.For<Context>();

            //Act
            context.InstantiateContext<TestContext>();

            //Assert
            foreach (IContext c in context.Children) {
                Assert.That(c.GetType() == typeof(TestContext));
            }
        }

        [Test]
        public void ContextAddsChildContext_OnInstantiateContextTyped() {
            //Arrange
            Context context = Substitute.For<Context>();

            //Act
            context.InstantiateContext(typeof(TestContext));

            //Assert
            foreach (IContext c in context.Children) {
                Assert.That(c.GetType() == typeof(TestContext));
            }
        }

        [Test]
        public void ContextBecomesChildOfParent_OnSetParent() {
            //Arrange
            Context parentContext = Substitute.For<Context>();
            Context childContext = Substitute.For<Context>();

            //Act
            childContext.SetParent(parentContext);

            //Assert
            foreach (IContext c in parentContext.Children) {
                Assert.That(c == childContext);
            }
        }

        [Test]
        public void ContextBecomesParentOfChild_OnSetParent() {
            //Arrange
            Context parentContext = Substitute.For<Context>();
            Context childContext = Substitute.For<Context>();

            //Act
            childContext.SetParent(parentContext);

            //Assert
            Assert.That(childContext.Parent == parentContext);
        }

        [Test]
        public void ContextLosesParent_OnSetNullParent() {
            //Arrange
            Context parentContext = Substitute.For<Context>();
            Context childContext = Substitute.For<Context>();
            childContext.SetParent(parentContext);

            //Act
            childContext.SetParent(null);

            //Assert
            Assert.That(childContext.Parent == null);
        }

        [Test]
        public void ContextRemovesChild_OnSetNullParent() {
            //Arrange
            Context parentContext = Substitute.For<Context>();
            Context childContext = Substitute.For<Context>();
            childContext.SetParent(parentContext);

            //Act
            childContext.SetParent(null);

            //Assert
            Assert.That(parentContext.ChildContextsCount == 0);
        }

        [Test]
        public void ContextRemovesAllChildren_OnRemoveContext() {
            //Arrange
            TestContext parentContext = new TestContext();
            TestContext grandChildContext1 = new TestContext();
            TestContext grandChildContext2 = new TestContext();
            Context childContext = new TestContext();

            parentContext.Initialize();
            grandChildContext1.Initialize();
            grandChildContext2.Initialize();
            childContext.Initialize();

            grandChildContext1.SetParent(childContext);
            grandChildContext2.SetParent(childContext);
            childContext.SetParent(parentContext);

            //Assert
            Assert.That(parentContext.ChildContextsCount == 1);
            Assert.That(childContext.ChildContextsCount == 2);

            //Act
            childContext.Remove();

            //Assert
            Assert.That(parentContext.ChildContextsCount == 0);
            Assert.That(childContext.IsRemoved);
            Assert.That(grandChildContext1.IsRemoved);
            Assert.That(grandChildContext2.IsRemoved);
        }

        [Test]
        public void ContextPassesChildToSwitchedContext_OnSwitchContext() {
            //Arrange
            Context childContext = Substitute.For<Context>();

            Context parentContext = new TestContext();
            parentContext.Initialize();

            childContext.SetParent(parentContext);

            //Act
            IContext switchedContext = parentContext.Switch<AlternativeTestContext>();

            //Assert
            Assert.That(childContext.Parent == switchedContext);
        }

        [Test]
        public void ContextPassesParentToSwitchedContext_OnSwitchContext() {
            //Arrange
            Context parentContext = Substitute.For<Context>();

            Context childContext = new TestContext();
            childContext.Initialize();

            childContext.SetParent(parentContext);

            //Act
            IContext switchedContext = childContext.Switch<AlternativeTestContext>();

            //Assert
            Assert.That(switchedContext.Parent == parentContext);
        }

    }

}