/// <copyright file="ViewTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;
using NSubstitute;
using System;
using System.Linq;

namespace IoCPlus.ContextTests {

    public class ViewTests {

        private class TestTypes {
            public class TestSignal : Signal { }
            public class TestView : View { }
            public class TestMediator : Mediator<TestView> {
                public override void Initialize() { }
                public override void Dispose() { }
            }
            public class TestContext : Context {
                public TestSignal TestSignal { get; private set; }
                protected override void SetBindings() {
                    base.SetBindings();
                    TestSignal = new TestSignal();
                    Bind<TestSignal>(TestSignal);
                    BindMediator<TestMediator, TestView>();
                    On<TestSignal>().InstantiateView<TestView>();
                }
            }
        }

        [Test]
        public void ContextInstantiatesView_OnSignal() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();
            context.Initialize();

            //Act
            context.TestSignal.Dispatch();

            //Assert
            Assert.That(context.Mediators.First() != null);
            Assert.That(context.Mediators.First().GetView().GetType() == typeof(TestTypes.TestView));
        }

    }

}