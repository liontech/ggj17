/// <copyright file="InjectorTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;
using NUnit.Framework;
using System.Collections.Generic;
using IoCPlus.Internal;

namespace IoCPlus {

    public class InjectorTests {

        private class TestTypes {
            public class OrdinaryFieldClass {
                public int TestValue { get { return testValue; } }
                int testValue = 0;
            }
            public class SingleFieldTestClass {
                public int TestValue { get { return testValue; } }
                [Inject]
                int testValue;
            }
            public enum FieldLabel {
                LabelOne,
                LabelTwo
            }
            public class LabeledFieldsTestClass {
                public int TestValueLabeledOne { get { return testValueLabeledOne; } }
                public int TestValueLabeledTwo { get { return testValueLabeledTwo; } }
                [Inject(FieldLabel.LabelOne)] int testValueLabeledOne;
                [Inject(FieldLabel.LabelTwo)] int testValueLabeledTwo;
            }
        }

        [Test]
        public void InjectorDoesNotAffectOrdinaryField_OnInject() {
            //Arrange
            const int testValue = 1337;
            TestTypes.OrdinaryFieldClass test = new TestTypes.OrdinaryFieldClass();
            Dictionary<Type, object> injectionBindings = new Dictionary<Type, object>();
            injectionBindings.Add(typeof(int), testValue);

            int valueBeforeInjection = test.TestValue;

            //Act
            Injector.Inject<Inject>(test, injectionBindings, Injector.CONTEXT_INJECTION_BINDING_MISSING_MESSAGE);

            //Assert
            Assert.That(test.TestValue == valueBeforeInjection);
        }

        [Test]
        public void InjectorInjectsUnlabeledField_OnInject() {
            //Arrange
            const int testValue = 1337;

            TestTypes.SingleFieldTestClass test = new TestTypes.SingleFieldTestClass();
            Dictionary<Type, object> injectionBindings = new Dictionary<Type, object>();
            injectionBindings.Add(typeof(int), testValue);

            //Act
            Injector.Inject<Inject>(test, injectionBindings, Injector.CONTEXT_INJECTION_BINDING_MISSING_MESSAGE);

            //Assert
            Assert.That(test.TestValue == testValue);
        }

        [Test]
        public void InjectorInjectsLabeledFields_OnInject() {
            //Arrange
            const int testValueOne = 1337;
            const int testValueTwo = 1234;

            TestTypes.LabeledFieldsTestClass test = new TestTypes.LabeledFieldsTestClass();
            Dictionary<string, Dictionary<Type, object>> labeledInjectionBindings = new Dictionary<string, Dictionary<Type, object>>();

            Dictionary<Type, object> injectionBindingsLabeledOne = new Dictionary<Type, object>();
            injectionBindingsLabeledOne.Add(typeof(int), testValueOne);
            labeledInjectionBindings.Add(TestTypes.FieldLabel.LabelOne.ToString(), injectionBindingsLabeledOne);

            Dictionary<Type, object> injectionBindingsLabeledTwo = new Dictionary<Type, object>();
            injectionBindingsLabeledTwo.Add(typeof(int), testValueTwo);
            labeledInjectionBindings.Add(TestTypes.FieldLabel.LabelTwo.ToString(), injectionBindingsLabeledTwo);

            //Act
            Injector.Inject<Inject>(test, labeledInjectionBindings);

            //Assert
            Assert.That(test.TestValueLabeledOne == testValueOne);
            Assert.That(test.TestValueLabeledTwo == testValueTwo);
        }
    }

}