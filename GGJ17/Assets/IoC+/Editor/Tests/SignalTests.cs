﻿using NUnit.Framework;
using System.Linq;

namespace IoCPlus {

    public class SignalTests {

        private class TestTypes {
            public class DuplicatedParameterTypeSignal : Signal<string, string> { }
            public class TestCommand : Command {
                protected override void Execute() { }
            }
            public class TestContext : Context {
                protected override void SetBindings() {
                    base.SetBindings();
                    Bind<DuplicatedParameterTypeSignal>();
                    On<DuplicatedParameterTypeSignal>().Do<TestCommand>();
                }
            }
        }

        [Test]
        public void SignalWithDuplicateParameterTypeDispatchesNormally_OnDispatch() {
            //Arrange
            TestTypes.DuplicatedParameterTypeSignal signal = new TestTypes.DuplicatedParameterTypeSignal();

            //Act
            signal.Dispatch("hello", "world");

            //Assert
            Assert.That(signal.DispatchCount == 1);
        }

        [Test]
        public void SignalWithDuplicateParameterTypeBindsNormally_OnBind() {
            //Arrange
            TestTypes.TestContext context = new TestTypes.TestContext();

            //Act
            context.Initialize();

            //Assert
            bool hasSetSignalBinding = context.InjectionBindings.Any(x => x.Key == typeof(TestTypes.DuplicatedParameterTypeSignal));
            Assert.That(hasSetSignalBinding);
        }

    }

}