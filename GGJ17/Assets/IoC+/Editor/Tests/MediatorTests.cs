/// <copyright file="MediatorTests.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using NUnit.Framework;
using NSubstitute;

namespace IoCPlus {

    public class MediatorTests {

        private class TestTypes {
            public interface ITestView : IView { }
            public interface IIncorrectTestView : IView { }
            public class TestMediator : Mediator<ITestView> {
                public override void Initialize() { }
                public override void Dispose() { }
            }
        }

        [Test]
        public void MediatorSetsViewOfCorrectType_OnSetView() {
            //Arrange
            TestTypes.ITestView view = Substitute.For<TestTypes.ITestView>();
            Signal onDeleteSignal = new Signal();
            view.DeleteSignal.Returns(onDeleteSignal);

            TestTypes.TestMediator mediator = new TestTypes.TestMediator();

            //Act
            mediator.SetView(view);

            //Assert
            Assert.That(mediator.GetView() == (view as IView));
        }

        [Test]
        public void MediatorDoesNotSetViewOfIncorrectType_OnSetView() {
            //Arrange
            TestTypes.IIncorrectTestView view = Substitute.For<TestTypes.IIncorrectTestView>();
            Signal onDeleteSignal = new Signal();
            view.DeleteSignal.Returns(onDeleteSignal);

            TestTypes.TestMediator mediator = new TestTypes.TestMediator();

            //Act
            mediator.SetView(view);

            //Assert
            Assert.That(mediator.GetView() == null);
        }
    }

}