/// <copyright file="Pool.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System.Collections.Generic;

namespace IoCPlus.Internal {

    public static class Pool<T> where T : new() {

        const int CAPACITY = 100;

        static List<T> activeInstances = new List<T>();
        static List<T> retiredInstances = new List<T>();

        public static T Create() {
            T instance = default(T);

            if (retiredInstances.Count == 0) {
                instance = new T();
            } else {
                instance = retiredInstances[0];
                retiredInstances.RemoveAt(0);
            }

            activeInstances.Add(instance);
            return instance;
        }

        public static void Retire(T instance) {
            activeInstances.Remove(instance);
            if (retiredInstances.Count < CAPACITY) {
                retiredInstances.Add(instance);
            }
        }

    }

}