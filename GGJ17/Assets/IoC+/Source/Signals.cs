/// <copyright file="Signals.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;

namespace IoCPlus {

    /// <summary>
    /// Dispatched when the context is entered.
    /// </summary>
    public class EnterContextSignal : Signal { }

    /// <summary>
    /// Dispatched when the context is left.
    /// </summary>
    public class LeaveContextSignal : Signal { }

}