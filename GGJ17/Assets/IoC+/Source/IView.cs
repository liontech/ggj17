/// <copyright file="IView.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus {

    public interface IView {

        Signal DeleteSignal { get; }
        bool IsDestroyed { get; }

        void Destroy();
        GameObject GetGameObject();

    }

}