/// <copyright file="MonitorSettings.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace IoCPlus.Internal {

    public static class MonitorSettings {

        public enum TimeControl {
            Paused,
            Delayed,
            Synced
        }

#if UNITY_EDITOR
        public static bool ShowDefaultBindings {
            get { return EditorPrefs.GetBool("ShowDefaultBindings", false); }
            set { EditorPrefs.SetBool("ShowDefaultBindings", value); }
        }

        public static bool ShowInjectionBindings {
            get { return EditorPrefs.GetBool("ShowInjectionBindings", false); }
            set { EditorPrefs.SetBool("ShowInjectionBindings", value); }
        }

        public static bool ShowMediatorBindings {
            get { return EditorPrefs.GetBool("ShowMediatorBindings", false); }
            set { EditorPrefs.SetBool("ShowMediatorBindings", value); }
        }

        public static bool ShowCommandBindings {
            get { return EditorPrefs.GetBool("ShowCommandBindings", true); }
            set { EditorPrefs.SetBool("ShowCommandBindings", value); }
        }

        public static bool ShowMediatorInstances {
            get { return EditorPrefs.GetBool("ShowMediatorInstances", true); }
            set { EditorPrefs.SetBool("ShowMediatorInstances", value); }
        }

        public static bool ShowNamespaces {
            get { return EditorPrefs.GetBool("ShowNamespaces", false); }
            set { EditorPrefs.SetBool("ShowNamespaces", value); }
        }

        public static TimeControl TimeControlSetting {
            get { return (TimeControl)Enum.Parse(typeof(TimeControl), EditorPrefs.GetString("TimeControl", TimeControl.Delayed.ToString())); }
            set { EditorPrefs.SetString("TimeControl", value.ToString()); }
        }
#else
        public static bool ShowDefaultBindings { get { return false; } }
        public static bool ShowInjectionBindings { get { return false; } }
        public static bool ShowMediatorBindings { get { return false; } }
        public static bool ShowCommandBindings { get { return false; } }
        public static bool ShowMediatorInstances { get { return false; } }
        public static TimeControl TimeControlSetting { get { return TimeControl.Synced; } }
#endif

    }

}