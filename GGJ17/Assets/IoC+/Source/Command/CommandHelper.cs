/// <copyright file="CommandHelper.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus.Internal {

    public static class CommandHelper {

        public static bool ValidateParameterAmount(this AbstractCommand command, int givenAmount, int requiredAmount) {
            if (givenAmount < requiredAmount) {
                Debug.LogError("Given amount of execution parameters for '" + command.GetType().Name + "' does not match the required amount. " + requiredAmount + " parameter(s) are required but received " + givenAmount + ".");
                return false;
            }

            if (givenAmount > requiredAmount) {
                Debug.LogWarning("Given amount of execution parameters for '" + command.GetType().Name + "' exceeds the required amount. " + requiredAmount + " parameter(s) are required but received " + givenAmount + ".");
            }

            return true;
        }

        public static bool ValidateParameterType<T>(this AbstractCommand command, object parameter) {
            if (parameter == null) {
                Debug.Log("Can't set the execution parameter of '" + command.GetType().Name + "' as the given parameter value is null.");
                return false;
            }
            if (!(parameter is T)) {
                Debug.LogError("Can't set the execution parameter of '" + command.GetType().Name + "' as the given parameter does not match the required type. '" + typeof(T).Name + "' is required but '" + parameter.GetType().Name + "' is given.");
                return false;
            }
            return true;
        }

    }

}