/// <copyright file="Command.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;

namespace IoCPlus {

    /// <summary>
    /// A command can be executed as a response to a signal, set in a context. Use the inject attribute on fields to inject values from context. Use the injectParameter attribute to inject values from signal parameters.
    /// </summary>
    [ScriptDoc("Classes", "Command", "Can be executed as a response to a signal, set in a context.", "A command can be executed as a response to a signal, set in a context. Use the inject attribute on fields to inject values from context. Use the injectParameter attribute to inject values from signal parameters. Use Command<T>, Command<T,U> or Command<T,U,I> to make use of execution parameters, set in the Do<T>(...) in the context.")]
    public class Command : AbstractCommand {

        /// <summary>
        /// Sets the execution parameters.
        /// </summary>
        [ScriptDoc("Sets the execution parameters.")]
        public override sealed void SetParameters(params object[] parameters) { }

        /// <summary>
        /// Executes the command.
        /// </summary>
        [ScriptDoc("Executes the command.")]
        public override sealed void PerformExecution() {
            base.PerformExecution();
            Execute();
            if (!isAborted && !isReleased) {
                ExecuteOverTime();
            }
        }

        /// <summary>
        /// Reverts the command.
        /// </summary>
        [ScriptDoc("Reverts the command.")]
        public override sealed void PerformRevertion() {
            base.PerformRevertion();
            Revert();
            if (!isReleased) {
                RevertOverTime();
            }
        }

        /// <summary>
        /// Override this method to implement execution.
        /// </summary>
        [ScriptDoc("Override this method to implement execution.")]
        protected virtual void Execute() { }

        /// <summary>
        /// Override this method to implement execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        [ScriptDoc("Override this method to implement execution without releasing immediately. Be sure to not call this base method when you do so.")]
        protected virtual void ExecuteOverTime() {
            Release();
        }

        /// <summary>
        /// Override this method to implement a revert execution.
        /// </summary>
        [ScriptDoc("Override this method to implement a revert execution.")]
        protected virtual void Revert() { }

        /// <summary>
        /// Override this method to implement a revert execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        [ScriptDoc("Override this method to implement a revert execution without releasing immediately. Be sure to not call this base method when you do so.")]
        protected virtual void RevertOverTime() {
            Release();
        }

    }

    /// <summary>
    /// A command can be executed as a response to a signal, set in a context. Use the inject attribute on fields to inject values from context. Use the injectParameter attribute to inject values from signal parameters.
    /// <typeparam name="T">Type of the execution parameter.</typeparam>
    /// </summary>
    public abstract class Command<T> : AbstractCommand {

        private T parameter1;

        /// <summary>
        /// Sets the execution parameters.
        /// </summary>
        public override sealed void SetParameters(params object[] parameters) {
            if (!this.ValidateParameterAmount(parameters.Length, 1)) { return; }
            if (!this.ValidateParameterType<T>(parameters[0])) { return; }
            parameter1 = (T)parameters[0];
        }

        /// <summary>
        /// Executes the command.
        /// </summary>
        public override sealed void PerformExecution() {
            base.PerformExecution();
            Execute(parameter1);
            if (!isAborted && !isReleased) {
                ExecuteOverTime(parameter1);
            }
        }

        /// <summary>
        /// Reverts the command.
        /// </summary>
        public override sealed void PerformRevertion() {
            base.PerformRevertion();
            Revert(parameter1);
            if (!isReleased) {
                RevertOverTime(parameter1);
            }
        }

        /// <summary>
        /// Override this method to implement execution.
        /// </summary>
        protected virtual void Execute(T parameter1) { }

        /// <summary>
        /// Override this method to implement execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void ExecuteOverTime(T parameter1) {
            Release();
        }

        /// <summary>
        /// Override this method to implement a revert execution.
        /// </summary>
        protected virtual void Revert(T parameter1) { }

        /// <summary>
        /// Override this method to implement a revert execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void RevertOverTime(T parameter1) {
            Release();
        }

    }

    /// <summary>
    /// A command can be executed as a response to a signal, set in a context. Use the inject attribute on fields to inject values from context. Use the injectParameter attribute to inject values from signal parameters.
    /// <typeparam name="T">Type of the first execution parameter.</typeparam>
    /// <typeparam name="U">Type of the second execution parameter.</typeparam>
    /// </summary>
    public abstract class Command<T, U> : AbstractCommand {

        private T parameter1;
        private U parameter2;

        //// <summary>
        /// Sets the execution parameters.
        /// </summary>
        public override sealed void SetParameters(params object[] parameters) {
            if (!this.ValidateParameterAmount(parameters.Length, 2)) { return; }
            if (!this.ValidateParameterType<T>(parameters[0])) { return; }
            if (!this.ValidateParameterType<U>(parameters[1])) { return; }
            parameter1 = (T)parameters[0];
            parameter2 = (U)parameters[1];
        }

        /// <summary>
        /// Executes the command.
        /// </summary>
        public override sealed void PerformExecution() {
            base.PerformExecution();
            Execute(parameter1, parameter2);
            if (!isAborted && !isReleased) {
                ExecuteOverTime(parameter1, parameter2);
            }
        }

        /// <summary>
        /// Reverts the command.
        /// </summary>
        public override sealed void PerformRevertion() {
            base.PerformRevertion();
            Revert(parameter1, parameter2);
            if (!isReleased) {
                RevertOverTime(parameter1, parameter2);
            }
        }

        /// <summary>
        /// Override this method to implement execution.
        /// </summary>
        protected virtual void Execute(T parameter1, U parameter2) { }

        /// <summary>
        /// Override this method to implement execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void ExecuteOverTime(T parameter1, U parameter2) {
            Release();
        }

        /// <summary>
        /// Override this method to implement a revert execution.
        /// </summary>
        protected virtual void Revert(T parameter1, U parameter2) { }

        /// <summary>
        /// Override this method to implement a revert execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void RevertOverTime(T parameter1, U parameter2) {
            Release();
        }

    }

    /// <summary>
    /// A command can be executed as a response to a signal, set in a context. Use the inject attribute on fields to inject values from context. Use the injectParameter attribute to inject values from signal parameters.
    /// <typeparam name="T">Type of the first execution parameter.</typeparam>
    /// <typeparam name="U">Type of the second execution parameter.</typeparam>
    /// <typeparam name="I">Type of the third execution parameter.</typeparam>
    /// </summary>
    public abstract class Command<T, U, I> : AbstractCommand {

        private T parameter1;
        private U parameter2;
        private I parameter3;

        /// <summary>
        /// Sets the execution parameters.
        /// </summary>
        public override void SetParameters(params object[] parameters) {
            if (!this.ValidateParameterAmount(parameters.Length, 3)) { return; }
            if (!this.ValidateParameterType<T>(parameters[0])) { return; }
            if (!this.ValidateParameterType<U>(parameters[1])) { return; }
            if (!this.ValidateParameterType<I>(parameters[2])) { return; }
            parameter1 = (T)parameters[0];
            parameter2 = (U)parameters[1];
            parameter3 = (I)parameters[2];
        }

        /// <summary>
        /// Executes the command.
        /// </summary>
        public override sealed void PerformExecution() {
            base.PerformExecution();
            Execute(parameter1, parameter2, parameter3);
            if (!isAborted && !isReleased) {
                ExecuteOverTime(parameter1, parameter2, parameter3);
            }
        }

        /// <summary>
        /// Reverts the command.
        /// </summary>
        public override sealed void PerformRevertion() {
            base.PerformRevertion();
            Revert(parameter1, parameter2, parameter3);
            if (!isReleased) {
                RevertOverTime(parameter1, parameter2, parameter3);
            }
        }

        /// <summary>
        /// Override this method to implement execution.
        /// </summary>
        protected virtual void Execute(T parameter1, U parameter2, I parameter3) { }

        /// <summary>
        /// Override this method to implement execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void ExecuteOverTime(T parameter1, U parameter2, I parameter3) {
            Release();
        }

        /// <summary>
        /// Override this method to implement a revert execution.
        /// </summary>
        protected virtual void Revert(T parameter1, U parameter2, I parameter3) { }

        /// <summary>
        /// Override this method to implement a revert execution without releasing immediately. Be sure to not call this base method when you do so.
        /// </summary>
        protected virtual void RevertOverTime(T parameter1, U parameter2, I parameter3) {
            Release();
        }

    }

}