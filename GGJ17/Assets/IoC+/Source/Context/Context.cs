/// <copyright file="Context.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;
using System.Collections.Generic;
using UnityEngine;

namespace IoCPlus {

    [ScriptDoc("Classes", "Context", "An IoC container with injection, mediator and command bindings.", "An IoC container in which injection, mediator and command bindings can be set. Subclass this class to create your own contexts.")]
    public abstract class Context : IContext {

        public IContext Parent { get; private set; }
        public IEnumerable<IContext> Children { get { return children; } }
        public IContext State { get { return state; } }

        public IEnumerable<KeyValuePair<Type, object>> InjectionBindings { get { return injectionBinder.InjectionBindings; } }
        public IEnumerable<KeyValuePair<string, Dictionary<Type, object>>> LabeledInjectionBindings { get { return injectionBinder.LabeledInjectionBindings; } }
        public IEnumerable<KeyValuePair<Type, Type>> MediatorBindings { get { return mediatorBinder.MediatorBindings; } }
        public IEnumerable<Mediator> Mediators { get { return mediatorBinder.Mediators; } }
        public IEnumerable<KeyValuePair<AbstractSignal, List<SignalResponse>>> CommandBindings { get { return commandBinder.CommandBindings; } }

        public int ChildContextsCount { get { return children.Count; } }
        public bool IsRemoved { get; private set; }

        readonly List<IContext> children = new List<IContext>();
        readonly InjectionBinder injectionBinder = new InjectionBinder();
        readonly MediatorBinder mediatorBinder = new MediatorBinder();
        readonly CommandBinder commandBinder = new CommandBinder();

        IContext state;

        EnterContextSignal enterSignal;
        LeaveContextSignal leaveSignal;

        public Context() {
            injectionBinder.Initialize(this, injectionBinder, commandBinder);
            mediatorBinder.Initialize(this, injectionBinder, commandBinder);
            commandBinder.Initialize(this, injectionBinder, commandBinder);

            ContextLogger.Log(InstantiateContextLog.Create(this));
        }
        
        public void Initialize(bool dispatchEnterSignal = true) {
            SetBindings();

            commandBinder.AddListeners();

            ContextLogger.Log(InitializedContextLog.Create(this));

            if (dispatchEnterSignal) {
                enterSignal.Dispatch();
            }
        }
        
        public void Remove(bool dispatchLeaveSignal = true) {
            if (IsRemoved) { return; }
            IsRemoved = true;

            if (dispatchLeaveSignal) {
                leaveSignal.Dispatch();
            }

            commandBinder.RemoveListeners();
            mediatorBinder.AssignAllMediatorsToParentOrRemove();

            while (children.Count > 0) {
                children[0].Remove();
            }

            if (Parent != null) {
                SetParent(null);
            }

            state = null;

            ContextLogger.Log(RemoveContextLog.Create(this));
        }

        public void SetParent(IContext parentContext) {
            if (Parent != null) {
                (Parent as Context).RemoveChild(this);
            }
            Parent = parentContext;
            if (Parent != null) {
                (Parent as Context).AddChild(this);
            }

            ContextLogger.Log(SetParentContextLog.Create(this, parentContext));

            if (!IsRemoved) {
                UpdateBindingsAndInjections();
            }
        }

        public IContext Switch<T>() where T : Context {
            return Switch(typeof(T));
        }

        public IContext Switch(Type contextType) {
            IContext newContext;

            if (Parent != null) {
                newContext = Parent.InstantiateContext(contextType, false);
            } else {
                newContext = Activator.CreateInstance(contextType) as IContext;
                (newContext as Context).Initialize(false);
            }

            List<Mediator> mediatorsCopy = new List<Mediator>(Mediators);
            foreach (Mediator mediator in mediatorsCopy) {
                mediatorBinder.RemoveMediator(mediator);
                newContext.AddMediator(mediator);
            }

            List<IContext> childContextsCopy = new List<IContext>(children);
            foreach (IContext childContext in childContextsCopy) {
                childContext.SetParent(newContext);
            }

            newContext.SetContextState(State);

            Remove(false);

            return newContext;
        }

        public IContext InstantiateContext<T>(bool dispatchEnterSignal = true) where T : Context, new() {
            IContext context;
            InstantiateContext<T>(out context, dispatchEnterSignal);
            return context as T;
        }

        public void InstantiateContext<T>(out IContext context, bool dispatchEnterSignal = true) where T : Context, new() {
            context = new T();
            context.SetParent(this);
            (context as Context).Initialize(dispatchEnterSignal);
        }

        public IContext InstantiateContext(Type contextType, bool dispatchEnterSignal = true) {
            IContext context;
            InstantiateContext(contextType, out context, dispatchEnterSignal);
            return context;
        }

        public void InstantiateContext(Type contextType, out IContext context, bool dispatchEnterSignal = true) {
            if (!contextType.IsSubclassOf(typeof(Context))) {
                Debug.Log("Can't instantiate context of type '" + contextType.Name + "' because it is not a subclass of Context.");
                context = null;
                return;
            }
            context = Activator.CreateInstance(contextType) as IContext;
            context.SetParent(this);
            (context as Context).Initialize(dispatchEnterSignal);
        }

        public void SetContextState<T>() where T : Context, new() {
            if (State != null) {
                State.Remove();
            }
            InstantiateContext<T>(out state);
            ContextLogger.Log(SetContextStateLog.Create(this, State));
        }

        public void SetContextState(Type contextType) {
            if (State != null) {
                State.Remove();
            }
            InstantiateContext(contextType, out state);
            ContextLogger.Log(SetContextStateLog.Create(this, State));
        }

        public void SetContextState(IContext newState) {
            if (State != null) {
                State.Remove();
            }
            state = newState;
            ContextLogger.Log(SetContextStateLog.Create(this, State));
        }

        public void SwitchContextState<T>() where T : Context {
            SwitchContextState(typeof(T));
        }

        public void SwitchContextState(Type contextType) {
            if (state == null) {
                SetContextState(contextType);
                return;
            }
            state = state.Switch(contextType);
            ContextLogger.Log(SetContextStateLog.Create(this, State));
        }

        public T InstantiateView<T>() where T : View {
            return InstantiateView<T>(null);
        }

        public T InstantiateView<T>(T prefab) where T : IView {
            return mediatorBinder.InstantiateView(prefab);
        }
        
        public void AddView<T>(T view, bool addChildViews = true) where T : IView {
            mediatorBinder.AddView(view, addChildViews);
        }

        public void AddMediator(Mediator mediator) {
            mediatorBinder.AddMediator(mediator);
        }

        public Dictionary<Type, object> GetCulumativeInjectionBindings() {
            return injectionBinder.GetCulumativeInjectionBindings();
        }

        public Dictionary<string, Dictionary<Type, object>> GetCulumativeLabeledInjectionBindings() {
            return injectionBinder.GetCumulativeLabeledInjectionBindings();
        }

        public Dictionary<Type, Type> GetCulumativeMediatorBindings() {
            return mediatorBinder.GetCulumativeMediatorBindings();
        }

        /// <summary>
        /// Override to set additional injection, mediator and command bindings.
        /// </summary>
        [ScriptDoc("Override to set additional injection, mediator and command bindings.")]
        protected virtual void SetBindings() {
            Bind<IContext>(this);
            enterSignal = Bind<EnterContextSignal>();
            leaveSignal = Bind<LeaveContextSignal>();
        }

        /// <summary>
        /// Binds the given type to a new instance of that type.
        /// </summary>
        /// <typeparam name="T">Injection type</typeparam>
        /// <returns>The instantiated instance of the given type.</returns>
        [ScriptDoc("Binds the given type to a value. All [inject] fields with a matching type in mediators and commands under this context will be set to this value.")]
        protected T Bind<T>() where T : class, new() {
            return injectionBinder.Bind<T, T>();
        }

        /// <summary>
        /// Binds the given type to a new instance of a second given type.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <typeparam name="U">Injection instance type.</typeparam>
        /// <returns>The instantiated instance of the given instance type.</returns>
        protected T Bind<T, U>() where T : class where U : T, new() {
            return injectionBinder.Bind<T>(new U());
        }

        /// <summary>
        /// Binds the given type to the given instance.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <param name="singleton">Injection instance.</param>
        /// <returns>The instance cast to the given injection type.</returns>
        protected T Bind<T>(object singleton) where T : class {
            return injectionBinder.Bind<T>(singleton);
        }

        /// <summary>
        /// Binds the given type to a new instance of that type, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type</typeparam>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instantiated instance of the given type.</returns>
        [ScriptDoc("Binds the given type to a value. All [inject(label)] fields with a matching label and type in mediators and commands under this context will be set to this value.")]
        protected T BindLabeled<T>(object label) where T : class, new() {
            return injectionBinder.BindLabeled<T, T>(label);
        }

        /// <summary>
        /// Binds the given type to a new instance of a second given type, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <typeparam name="U">Injection instance type.</typeparam>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instantiated instance of the given instance type.</returns>
        protected T BindLabeled<T, U>(object label) where T : class where U : T, new() {
            return injectionBinder.BindLabeled<T>(new U(), label);
        }

        /// <summary>
        /// Binds the given type to the given instance, labeled by the given object cast to a string.
        /// </summary>
        /// <typeparam name="T">Injection type.</typeparam>
        /// <param name="singleton">Injection instance.</param>
        /// <param name="label">Object which will be cast to a string and used as label.</param>
        /// <returns>The instance cast to the given injection type.</returns>
        protected T BindLabeled<T>(object singleton, object label) where T : class {
            return injectionBinder.BindLabeled<T>(singleton, label);
        }

        /// <summary>
        /// Binds the given mediator type to the given view type.
        /// </summary>
        /// <typeparam name="T">Mediator type</typeparam>
        /// <typeparam name="U">View Type</typeparam>
        [ScriptDoc("Binds the given mediator type to the given view type. View instances under this context will be assigned an instance of the given mediator.")]
        protected void BindMediator<T, U>() where T : Mediator where U : View {
            mediatorBinder.BindMediator<T, U>();
        }

        /// <summary>
        /// Starts listening to the given signal.
        /// </summary>
        /// <typeparam name="T">The signal type that is to be listened to.</typeparam>
        /// <returns>A command binder that can be used to set response actions.</returns>
        [ScriptDoc("Starts listening to the given signal type. Returns a command binder that can be used to set response actions.")]
        protected ICommandBinder On<T>() where T : AbstractSignal, new() {
            return commandBinder.On<T>();
        }

        private void AddChild(IContext childContext) {
            children.Add(childContext);
        }

        private void RemoveChild(IContext childContext) {
            if (State == childContext) {
                ContextLogger.Log(SetContextStateLog.Create(this, null));
            }
            children.Remove(childContext);
        }

        private void UpdateBindingsAndInjections() {
            commandBinder.UpdateBindings();
            mediatorBinder.UpdateInjections();
            children.ForEach(x => (x as Context).UpdateBindingsAndInjections());
        }

    }

}