/// <copyright file="ContextComponent.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public abstract class ContextComponent {

        protected Context context;
        protected IInjectionBinder injectionBinder;
        protected ICommandBinder commandBinder;

        public void Initialize(Context context, IInjectionBinder injectionBinder, ICommandBinder commandBinder) {
            this.context = context;
            this.injectionBinder = injectionBinder;
            this.commandBinder = commandBinder;
        }

    }

}