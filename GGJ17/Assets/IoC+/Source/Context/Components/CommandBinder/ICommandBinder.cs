/// <copyright file="ICommandBinder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    /// <summary>
    /// An instance of this interface is returned by the context's On<Signal>() method. Use this interface to decorate the given signal's response.
    /// </summary>
    [ScriptDoc("Interfaces", "ICommandBinder", "An instance of this interface is returned by the context's On<Signal>() method.", "An instance of this interface is returned by the context's On<Signal>() method. Use this interface to decorate the given signal's response. All methods return this instance to continue decorating.")]
    public interface ICommandBinder {

        /// <summary>
        /// Adds a command of the given type.
        /// </summary>
        /// <typeparam name="T">Command type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command of the given type.")]
        ICommandBinder Do<T>(params object[] executionParameters) where T : AbstractCommand, new();

        /// <summary>
        /// Adds a command to set this context's state to an instance of the given context type.
        /// </summary>
        /// <typeparam name="T">State context type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to set this context's state to an instance of the given context type.")]
        ICommandBinder GotoState<T>() where T : Context, new();

        /// <summary>
        /// Adds a command to switch this context's state with an instance of the given context type.
        /// </summary>
        /// <typeparam name="T">State context type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to switch this context's state with an instance of the given context type.")]
        ICommandBinder SwitchState<T>() where T : Context, new();

        /// <summary>
        /// Adds a command to instantiate a view of the given view type to this context in form of a new gameobject.
        /// </summary>
        /// <typeparam name="T">View type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to instantiate a view of the given view type to this context in form of a new gameobject.")]
        ICommandBinder InstantiateView<T>() where T : View;

        /// <summary>
        /// Adds a command to dispatch the given parameterless signal type.
        /// </summary>
        /// <typeparam name="T">Signal type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to dispatch the given parameterless signal type.")]
        ICommandBinder Dispatch<T>() where T : Signal;

        /// <summary>
        /// Adds a command to switch this context with an instance of the given context type.
        /// </summary>
        /// <typeparam name="T">Context type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to switch this context with an instance of the given context type.")]
        ICommandBinder SwitchContext<T>() where T : Context, new();

        /// <summary>
        /// Adds a command to add an instance of the given context type as a child to this context.
        /// </summary>
        /// <typeparam name="T">Context type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to add an instance of the given context type as a child to this context.")]
        ICommandBinder AddContext<T>() where T : Context, new();

        /// <summary>
        /// Adds a command to remove this context.
        /// </summary>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command to remove this context.")]
        ICommandBinder Remove();

        /// <summary>
        /// Adds a command of the given type which is called when any of the commands is aborted.
        /// </summary>
        /// <typeparam name="T">Command type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command of the given type which is called when any of the commands is aborted.")]
        ICommandBinder OnAbort<T>() where T : Command, new();

        /// <summary>
        /// Adds a command of the given type which is called when all of the commands are succesfully released.
        /// </summary>
        /// <typeparam name="T">Command type</typeparam>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Adds a command of the given type which is called when all of the commands are succesfully released.")]
        ICommandBinder OnFinish<T>() where T : Command, new();

        /// <summary>
        /// Makes all executed and released commands revert when any of the commands is aborted.
        /// </summary>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Makes all executed and released commands revert when any of the commands is aborted.")]
        ICommandBinder RevertOnAbort();

        /// <summary>
        /// Makes all commands execute at once instead of waiting for them to release before going to the next.
        /// </summary>
        /// <returns>The instance of commandBinder to continue decorating the signal's response.</returns>
        [ScriptDoc("Makes all commands execute at once instead of waiting for them to release before going to the next.")]
        ICommandBinder ExecuteParallel();

    }

}