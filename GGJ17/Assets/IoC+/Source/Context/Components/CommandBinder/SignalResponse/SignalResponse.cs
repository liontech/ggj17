/// <copyright file="SignalResponse.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System.Collections.Generic;

namespace IoCPlus.Internal {

    public class SignalResponse {

        public readonly int Index;

        public List<AbstractCommand> Commands = new List<AbstractCommand>();

        public bool RevertOnAbort = false;
        public AbstractCommand AbortCommand;

        public bool ExecuteParallel;
        public AbstractCommand FinishCommand;

        private SignalResponder signalResponder;

        public SignalResponse(int index) {
            Index = index;
        }

        public void Respond(Context context, AbstractSignal signal) {
            if (signalResponder == null) {
                if (ExecuteParallel) {
                    signalResponder = new ParallelSignalResponder(this, Index);
                } else {
                    signalResponder = new LinearSignalResponder(this, Index);
                }
            }
            signalResponder.Respond(context, signal);
        }

    }

}