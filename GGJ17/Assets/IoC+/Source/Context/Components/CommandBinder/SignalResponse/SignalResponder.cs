/// <copyright file="SignalResponder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public abstract class SignalResponder {

        protected int responseIndex { get; private set; }

        protected readonly SignalResponse signalResponse;
        protected Context context;
        protected AbstractSignal signal;

        public SignalResponder(SignalResponse signalResponse, int responseIndex) {
            this.signalResponse = signalResponse;
            this.responseIndex = responseIndex;
        }

        public virtual void Respond(Context context, AbstractSignal signal) {
            this.context = context;
            this.signal = signal;

            if (signalResponse.AbortCommand != null) {
                signalResponse.AbortCommand.OnRelease = OnAbortCommandReleased;
            }
            if (signalResponse.FinishCommand != null) {
                signalResponse.FinishCommand.OnRelease = OnFinishCommandReleased;
            }
        }

        private void OnAbortCommandReleased(AbstractCommand command) {
            ContextLogger.Log(AbortCommandReleaseLog.Create(context, signal, responseIndex));
        }

        private void OnFinishCommandReleased(AbstractCommand command) {
            ContextLogger.Log(FinishCommandReleaseLog.Create(context, signal, responseIndex));
        }

    }

}