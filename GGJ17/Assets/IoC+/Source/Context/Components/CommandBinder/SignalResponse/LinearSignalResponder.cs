/// <copyright file="LinearSignalResponder.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class LinearSignalResponder : SignalResponder {

        bool isAborted;

        public LinearSignalResponder(SignalResponse signalResponse, int responseIndex) : base(signalResponse, responseIndex) { }

        public override void Respond(Context context, AbstractSignal signal) {
            base.Respond(context, signal);

            isAborted = false;

            for (int i = 0; i < signalResponse.Commands.Count; i++) {
                signalResponse.Commands[i].OnRelease = OnCommandReleased;
                signalResponse.Commands[i].OnAbort = OnCommandAborted;
            }

            ContextLogger.Log(CommandExecuteLog.Create(context, signal, responseIndex, 0));
            signalResponse.Commands[0].PerformExecution();
        }

        private void OnCommandReleased(AbstractCommand command) {
            int index = signalResponse.Commands.IndexOf(command);

            if (isAborted) {
                if (!signalResponse.RevertOnAbort) { return; }
                if (index <= 0) { return; }
                AbstractCommand nextCommand = signalResponse.Commands[index - 1];

                ContextLogger.Log(CommandRevertLog.Create(context, signal, responseIndex, index - 1));

                nextCommand.PerformRevertion();
            } else {
                ContextLogger.Log(CommandReleaseLog.Create(context, signal, responseIndex, index));

                if (index >= signalResponse.Commands.Count - 1) {

                    if (signalResponse.FinishCommand != null) {
                        ContextLogger.Log(FinishCommandExecuteLog.Create(context, signal, responseIndex));
                        signalResponse.FinishCommand.PerformExecution();
                    }

                    return;
                }

                ContextLogger.Log(CommandExecuteLog.Create(context, signal, responseIndex, index + 1));

                AbstractCommand nextCommand = signalResponse.Commands[index + 1];
                nextCommand.PerformExecution();
            }
        }

        private void OnCommandAborted(AbstractCommand command) {
            isAborted = true;
            int index = signalResponse.Commands.IndexOf(command);
            ContextLogger.Log(CommandAbortLog.Create(context, signal, signalResponse.Index, index));
            if (signalResponse.AbortCommand != null) {
                ContextLogger.Log(AbortCommandExecuteLog.Create(context, signal, signalResponse.Index));
                signalResponse.AbortCommand.PerformExecution();
            }
        }

    }

}