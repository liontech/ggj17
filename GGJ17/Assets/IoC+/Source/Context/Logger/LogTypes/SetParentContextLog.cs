/// <copyright file="SetParentContextLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class SetParentContextLog : ContextLog {
        public Context ChildContext { get; private set; }
        public IContext ParentContext { get; private set; }

        public static SetParentContextLog Create(Context childContext, IContext parentContext) {
            if (!ShouldLog()) { return null; }
            SetParentContextLog log = Pool<SetParentContextLog>.Create();
            log.ChildContext = childContext;
            log.ParentContext = parentContext;
            return log;
        }

        public override bool HasProcessInterval() { return false; }
        public override void Trigger() {
            if (ContextLogger.OnContextParentSet == null) { return; }
            ContextLogger.OnContextParentSet(ChildContext, ParentContext);
            Pool<SetParentContextLog>.Retire(this);
        }
    }

}