/// <copyright file="CommandAbortLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class CommandAbortLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }
        public int CommandIndex { get; private set; }

        public static CommandAbortLog Create(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (!ShouldLog()) { return null; }
            CommandAbortLog log = Pool<CommandAbortLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            log.CommandIndex = commandIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnCommandAbort == null) { return; }
            ContextLogger.OnCommandAbort(Context, Signal, ResponseIndex, CommandIndex);
            Pool<CommandAbortLog>.Retire(this);
        }
    }

}