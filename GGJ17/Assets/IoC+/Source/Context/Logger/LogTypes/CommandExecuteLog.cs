/// <copyright file="CommandExecuteLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus.Internal {

    public class CommandExecuteLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }
        public int CommandIndex { get; private set; }

        public static CommandExecuteLog Create(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (!ShouldLog()) { return null; }
            CommandExecuteLog log = Pool<CommandExecuteLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            log.CommandIndex = commandIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnCommandExecute == null) { return; }
            ContextLogger.OnCommandExecute(Context, Signal, ResponseIndex, CommandIndex);
            Pool<CommandExecuteLog>.Retire(this);
        }
    }

}