/// <copyright file="FinishCommandReleaseLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class FinishCommandReleaseLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }

        public static FinishCommandReleaseLog Create(Context context, AbstractSignal signal, int responseIndex) {
            if (!ShouldLog()) { return null; }
            FinishCommandReleaseLog log = Pool<FinishCommandReleaseLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnFinishCommandRelease == null) { return; }
            ContextLogger.OnFinishCommandRelease(Context, Signal, ResponseIndex);
            Pool<FinishCommandReleaseLog>.Retire(this);
        }
    }

}