/// <copyright file="AbortCommandReleaseLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class AbortCommandReleaseLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }

        public static AbortCommandReleaseLog Create(Context context, AbstractSignal signal, int responseIndex) {
            if (!ShouldLog()) { return null; }
            AbortCommandReleaseLog log = Pool<AbortCommandReleaseLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnAbortCommandRelease == null) { return; }
            ContextLogger.OnAbortCommandRelease(Context, Signal, ResponseIndex);
            Pool<AbortCommandReleaseLog>.Retire(this);
        }
    }

}