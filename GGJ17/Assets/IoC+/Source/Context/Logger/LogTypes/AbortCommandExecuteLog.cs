/// <copyright file="AbortCommandExecuteLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class AbortCommandExecuteLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }

        public static AbortCommandExecuteLog Create(Context context, AbstractSignal signal, int responseIndex) {
            if (!ShouldLog()) { return null; }
            AbortCommandExecuteLog log = Pool<AbortCommandExecuteLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnAbortCommandExecute == null) { return; }
            ContextLogger.OnAbortCommandExecute(Context, Signal, ResponseIndex);
            Pool<AbortCommandExecuteLog>.Retire(this);
        }
    }

}