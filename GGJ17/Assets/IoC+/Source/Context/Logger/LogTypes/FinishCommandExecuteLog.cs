/// <copyright file="FinishCommandExecuteLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class FinishCommandExecuteLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }

        public static FinishCommandExecuteLog Create(Context context, AbstractSignal signal, int responseIndex) {
            if (!ShouldLog()) { return null; }
            FinishCommandExecuteLog log = Pool<FinishCommandExecuteLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnFinishCommandExecute == null) { return; }
            ContextLogger.OnFinishCommandExecute(Context, Signal, ResponseIndex);
            Pool<FinishCommandExecuteLog>.Retire(this);
        }
    }

}