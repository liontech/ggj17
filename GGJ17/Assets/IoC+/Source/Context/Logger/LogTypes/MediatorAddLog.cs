/// <copyright file="MediatorAddLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class MediatorAddLog : ContextLog {
        public Context Context { get; private set; }
        public Mediator Mediator { get; private set; }

        public static MediatorAddLog Create(Context context, Mediator mediator) {
            if (!ShouldLog()) { return null; }
            MediatorAddLog log = Pool<MediatorAddLog>.Create();
            log.Context = context;
            log.Mediator = mediator;
            return log;
        }

        public override bool HasProcessInterval() { return false; }

        public override void Trigger() {
            if (ContextLogger.OnMediatorAdd == null) { return; }
            ContextLogger.OnMediatorAdd(Context, Mediator);
            Pool<MediatorAddLog>.Retire(this);
        }
    }

}