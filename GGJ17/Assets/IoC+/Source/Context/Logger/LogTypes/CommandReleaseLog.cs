/// <copyright file="CommandReleaseLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus.Internal {

    public class CommandReleaseLog : ContextLog {
        public Context Context { get; private set; }
        public AbstractSignal Signal { get; private set; }
        public int ResponseIndex { get; private set; }
        public int CommandIndex { get; private set; }

        public static CommandReleaseLog Create(Context context, AbstractSignal signal, int responseIndex, int commandIndex) {
            if (!ShouldLog()) { return null; }
            CommandReleaseLog log = Pool<CommandReleaseLog>.Create();
            log.Context = context;
            log.Signal = signal;
            log.ResponseIndex = responseIndex;
            log.CommandIndex = commandIndex;
            return log;
        }

        public override void Trigger() {
            if (ContextLogger.OnCommandRelease == null) { return; }
            ContextLogger.OnCommandRelease(Context, Signal, ResponseIndex, CommandIndex);
            Pool<CommandReleaseLog>.Retire(this);
        }
    }

}