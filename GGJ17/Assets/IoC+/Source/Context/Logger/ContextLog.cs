/// <copyright file="ContextLog.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace IoCPlus.Internal {

    public abstract class ContextLog {
        protected static bool ShouldLog() {
#if UNITY_EDITOR
            return true;
#else
            return false;
#endif
        }

        public virtual bool HasProcessInterval() { return true; }
        public abstract void Trigger();
    }

}