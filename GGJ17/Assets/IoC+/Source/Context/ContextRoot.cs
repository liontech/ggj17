/// <copyright file="ContextRoot.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using UnityEngine;

namespace IoCPlus {

    /// <summary>
    /// Instantiates a context of the given type on awake.
    /// </summary>
    /// <typeparam name="T">Context type</typeparam>
    [ScriptDoc("Classes", "ContextRoot", "A monobehavior that instantiates a context of the given type on awake.")]
    public class ContextRoot<T> : MonoBehaviour where T : Context, new() {

        private void Awake() {
            T context = new T();
            context.Initialize();
        }

    }

}