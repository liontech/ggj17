/// <copyright file="Signal.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using System;
using System.Collections.Generic;

namespace IoCPlus {

    /// <summary>
    /// Can be dispatched and listened to, with up to three parameters of given types.
    /// </summary>
    public abstract class AbstractSignal {
        
        public readonly Dictionary<Type, object> InjectionBindings = new Dictionary<Type, object>();

        public int DispatchCount { get; private set; }

        Action<AbstractSignal> onTriggerReferenced;
        bool markedAsInjected;

        /// <summary>
        /// Used internally by the Context to make this signal log to the IoC+ Monitor on dispatch.
        /// </summary>
        public void MarkAsInjected() {
            markedAsInjected = true;
        }

        public void AddReferencedListener(Action<AbstractSignal> handler) {
            onTriggerReferenced += handler;
        }

        public void RemoveReferencedListener(Action<AbstractSignal> handler) {
            onTriggerReferenced -= handler;
        }

        public abstract bool HasDuplicateParameterTypes();

        protected void LogDispatch() {
            if (markedAsInjected) {
                ContextLogger.Log(SignalDispatchLog.Create(this));
            }
        }

        protected void OnDispatch() {
            if (onTriggerReferenced != null) {
                onTriggerReferenced(this);
            }
            DispatchCount++;
        }

        protected void AddInjectionBinding<T>() {
            Type type = typeof(T);
            if (InjectionBindings.ContainsKey(type)) { return; }
            InjectionBindings.Add(type, default(T));
        }

    }

    /// <summary>
    /// Can be dispatched and listened to, with up to three parameters of given types.
    /// </summary>
    [ScriptDoc("Classes", "Signal", "Can be dispatched and listened to, with up to three parameters of given types.")]
    public class Signal : AbstractSignal {

        Action onTrigger;

        [ScriptDoc("Dispatches the signal, triggering all that it listening to this signal.")]
        public virtual void Dispatch() {
            LogDispatch();
            if (onTrigger != null) {
                onTrigger();
            }
            OnDispatch();
        }

        [ScriptDoc("Adds a listener to this signal.")]
        public void AddListener(Action handler) {
            onTrigger += handler;
        }

        [ScriptDoc("Removes a listener from this signal.")]
        public void RemoveListener(Action handler) {
            onTrigger -= handler;
        }

        public override bool HasDuplicateParameterTypes() { return false; }

    }

    /// <summary>
    /// Can be dispatched and listened to, with up to three parameters of given types.
    /// </summary>
    /// <typeparam name="T">Type of the first parameter.</typeparam>
    public class Signal<T> : AbstractSignal {

        Action<T> onTrigger;

        public Signal(){
            AddInjectionBinding<T>();
        }

        public void Dispatch(T value) {
            LogDispatch();
            InjectionBindings[typeof(T)] = value;
            if (onTrigger != null) {
                onTrigger(value);
            }
            OnDispatch();
        }

        public void AddListener(Action<T> handler) {
            onTrigger += handler;
        }

        public void RemoveListener(Action<T> handler) {
            onTrigger -= handler;
        }

        public override bool HasDuplicateParameterTypes() { return false; }

    }

    /// <summary>
    /// Can be dispatched and listened to, with up to three parameters of given types.
    /// </summary>
    /// <typeparam name="T">Type of the first parameter.</typeparam>
    /// <typeparam name="U">Type of the second parameter.</typeparam>
    public class Signal<T, U> : AbstractSignal {

        Action<T, U> onTrigger;

        public Signal() {
            AddInjectionBinding<T>();
            AddInjectionBinding<U>();
        }

        public void Dispatch(T value, U value2) {
            LogDispatch();
            InjectionBindings[typeof(T)] = value;
            InjectionBindings[typeof(U)] = value2;
            if (onTrigger != null) {
                onTrigger(value, value2);
            }
            OnDispatch();
        }

        public void AddListener(Action<T, U> handler) {
            onTrigger += handler;
        }

        public void RemoveListener(Action<T, U> handler) {
            onTrigger -= handler;
        }

        public override bool HasDuplicateParameterTypes() {
            return typeof(T) == typeof(U);
        }

    }

    /// <summary>
    /// Can be dispatched and listened to, with up to three parameters of given types.
    /// </summary>
    /// <typeparam name="T">Type of the first parameter.</typeparam>
    /// <typeparam name="U">Type of the second parameter.</typeparam>
    /// <typeparam name="I">Type of the third parameter.</typeparam>
    public class Signal<T, U, I> : AbstractSignal {

        Action<T, U, I> onTrigger;

        public Signal() {
            AddInjectionBinding<T>();
            AddInjectionBinding<U>();
            AddInjectionBinding<I>();
        }

        public void Dispatch(T value, U value2, I value3) {
            LogDispatch();
            InjectionBindings[typeof(T)] = value;
            InjectionBindings[typeof(U)] = value2;
            InjectionBindings[typeof(I)] = value3;
            if (onTrigger != null) {
                onTrigger(value, value2, value3);
            }
            OnDispatch();
        }

        public void AddListener(Action<T, U, I> handler) {
            onTrigger += handler;
        }

        public void RemoveListener(Action<T, U, I> handler) {
            onTrigger -= handler;
        }

        public override bool HasDuplicateParameterTypes() {
            return typeof(T) == typeof(U) ||
                   typeof(T) == typeof(I) ||
                   typeof(U) == typeof(I);
        }

    }

}