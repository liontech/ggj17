/// <copyright file="DispatchSignalCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus {

    public class DispatchSignalCommand<T> : Command where T : Signal {

        [Inject] T signal;

        protected override void Execute() {
            if (signal == null) {
                UnityEngine.Debug.LogError("Can't Do<T>().Dispatch<" + typeof(T).Name + ">() as the given signal injection binding is not set.");
            }
            signal.Dispatch();
        }

    }

}