/// <copyright file="AddContextCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus {

    public class AddContextCommand<T> : Command where T : Context, new() {

        [Inject] IContext context;

        protected override void Execute() {

            context.InstantiateContext<T>();

        }

    }

}