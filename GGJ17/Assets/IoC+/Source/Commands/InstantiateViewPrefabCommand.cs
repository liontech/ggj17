/// <copyright file="InstantiateViewPrefabCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using UnityEngine;

namespace IoCPlus {

    public class InstantiateViewPrefabCommand : Command<string> {

        [Inject] IContext context;

        protected override void Execute(string prefabPath) {
            View prefab = Resources.Load<View>(prefabPath);
            if (prefab == null) {
                Debug.Log("Can't instantiate view prefab as no prefab is found at given path '" + prefabPath + "'.");
                return;
            }
            context.InstantiateView(prefab);
        }

    }

}
