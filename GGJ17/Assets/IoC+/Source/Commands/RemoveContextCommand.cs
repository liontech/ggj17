/// <copyright file="RemoveContextCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus {

    public class RemoveContextCommand : Command {

        [Inject] IContext context;

        protected override void Execute() {
            context.Remove();
        }

    }

}