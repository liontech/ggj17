/// <copyright file="SwitchStateCommand.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

namespace IoCPlus {

    public class SwitchStateCommand<T> : Command where T : Context, new() {

        [Inject] IContext context;

        protected override void Execute() {

            context.SwitchContextState<T>();

        }

    }

}