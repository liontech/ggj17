/// <copyright file="Injector.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using System.Collections.Generic;
using System;
using System.Reflection;
using System.Linq;
using UnityEngine;

namespace IoCPlus.Internal {

    public static class Injector {

        public const string CONTEXT_INJECTION_BINDING_MISSING_MESSAGE = "there is no injection binding set in its context or any parent context";
        public const string PARAMETER_INJECTION_BINDING_MISSING_MESSAGE = "there is no parameter set via the dispatched signal";

        static readonly Dictionary<Type, Dictionary<Type, FieldInfo[]>> cachedFieldInfos = new Dictionary<Type, Dictionary<Type, FieldInfo[]>>();
        static readonly Dictionary<Type, FieldInfo[]> cachedLabeledFieldInfos = new Dictionary<Type, FieldInfo[]>();

        public static void Inject<T>(object subject, Dictionary<Type, object> typeAssociations) where T : Attribute {
            Inject<T>(subject, typeAssociations, null);
        }

        public static void Inject<T>(object subject, Dictionary<Type, object> typeAssociations, string errorMessage) where T : Attribute {
            bool isInjectAttribute = typeof(T) == typeof(Inject);
            FieldInfo[] fields = GetUnlabeledFieldsInTypeWithAttribute<T>(subject.GetType());
            for (int i = 0; i < fields.Length; i++) {
                if (isInjectAttribute) {
                    Inject attribute = Attribute.GetCustomAttribute(fields[i], typeof(Inject)) as Inject;
                    if (attribute.HasLabel) { continue; }
                }
                object associatedObject;
                bool associationFound = typeAssociations.TryGetValue(fields[i].FieldType, out associatedObject);
                if (!associationFound) {
                    if (!string.IsNullOrEmpty(errorMessage) && !Attribute.IsDefined(fields[i], typeof(Optional))) {
                        LogInjectionError(fields[i], subject.GetType(), errorMessage);
                    }
                    continue;
                }
                fields[i].SetValue(subject, associatedObject);
            }
        }

        public static void Inject<T>(object subject, Dictionary<string, Dictionary<Type, object>> labeledTypeAssociations) where T : Inject {
            Inject<T>(subject, labeledTypeAssociations, false);
        }

        public static void Inject<T>(object subject, Dictionary<string, Dictionary<Type, object>> labeledTypeAssociations, bool logError) where T : Inject {
            FieldInfo[] fields = GetLabeledInjectionFieldsInType(subject.GetType());
            for (int i = 0; i < fields.Length; i++) {
                Inject injectAttribute = Attribute.GetCustomAttribute(fields[i], typeof(Inject)) as Inject;
                Dictionary<Type, object> typeAssociations;
                object associatedObject;
                if (!labeledTypeAssociations.TryGetValue(injectAttribute.Label, out typeAssociations) ||
                    !typeAssociations.TryGetValue(fields[i].FieldType, out associatedObject)) {
                    if (logError && !Attribute.IsDefined(fields[i], typeof(Optional))) {
                        LogInjectionError(fields[i], subject.GetType(), "there is no injection binding with label '" + injectAttribute.Label + "' set in its context or any parent context");
                    }
                    continue;
                }
                fields[i].SetValue(subject, associatedObject);
            }
        }

        private static FieldInfo[] GetUnlabeledFieldsInTypeWithAttribute<T>(Type type) where T : Attribute {
            Dictionary<Type, FieldInfo[]> attributedFields;
            cachedFieldInfos.TryGetValue(type, out attributedFields);
            if (attributedFields == null) {
                attributedFields = new Dictionary<Type, FieldInfo[]>();
                cachedFieldInfos.Add(type, attributedFields);
            }

            Type attributeType = typeof(T);
            FieldInfo[] fields;
            attributedFields.TryGetValue(attributeType, out fields);
            if (fields == null) {
                List<FieldInfo> fieldList = new List<FieldInfo>(GetAllFieldsOfType(type));
                fieldList.RemoveAll(x => !Attribute.IsDefined(x, typeof(T)));
                if (typeof(T) == typeof(Inject)) {
                    fieldList.RemoveAll(x => (Attribute.GetCustomAttribute(x, typeof(Inject)) as Inject).HasLabel);
                }
                fields = fieldList.ToArray();
                attributedFields.Add(attributeType, fields);
            }

            return fields;
        }

        private static FieldInfo[] GetLabeledInjectionFieldsInType(Type type) {
            FieldInfo[] labeledFields;
            cachedLabeledFieldInfos.TryGetValue(type, out labeledFields);
            if (labeledFields == null) {
                List<FieldInfo> fieldList = new List<FieldInfo>(GetAllFieldsOfType(type));
                fieldList.RemoveAll(x => !Attribute.IsDefined(x, typeof(Inject)));
                fieldList.RemoveAll(x => !(Attribute.GetCustomAttribute(x, typeof(Inject)) as Inject).HasLabel);
                labeledFields = fieldList.ToArray();
                cachedLabeledFieldInfos.Add(type, labeledFields);
            }
            return labeledFields;
        }

        private static IEnumerable<FieldInfo> GetAllFieldsOfType(Type t) {
            if (t == null) { return Enumerable.Empty<FieldInfo>(); }
            BindingFlags flags = BindingFlags.Public | BindingFlags.NonPublic |
                                 BindingFlags.Static | BindingFlags.Instance;
            return t.GetFields(flags).Concat(GetAllFieldsOfType(t.BaseType));
        }


        private static void LogInjectionError(FieldInfo fieldInfo, Type subjectType, string cause) {
            string fieldName = fieldInfo.Name;
            string fieldTypeName = fieldInfo.FieldType.Name;
            string classTypeName = subjectType.Name;
            string optionalAttributeName = typeof(Optional).Name;
            Debug.LogError("Can't inject '" + fieldName + "' of type '" + fieldTypeName + "' in '" + classTypeName + "' as " + cause + ". Add the [" + optionalAttributeName + "] attribute to this field to allow.");
        }

    }

}