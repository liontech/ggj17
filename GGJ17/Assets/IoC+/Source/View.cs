/// <copyright file="View.cs">Copyright (c) 2016 All Rights Reserved</copyright>
/// <author>Joris van Leeuwen</author>

using IoCPlus.Internal;
using UnityEngine;

namespace IoCPlus {

    /// <summary>
    /// A monobehaviour controlled by a mediator based on the context mediator bindings.
    /// </summary>
    [ScriptDoc("Classes", "View", "A monobehaviour controlled by a mediator based on the context mediator bindings.")]
    public class View : MonoBehaviour, IView {

        /// <summary>
        /// A signal dispatched on deletion of this view.
        /// </summary>
        [ScriptDoc("A signal dispatched on deletion of this view.")]
        public Signal DeleteSignal { get { return deleteSignal; } }

        /// <summary>
        /// True if this view has been destroyed.
        /// </summary>
        [ScriptDoc("True if this view has been destroyed.")]
        public bool IsDestroyed { get; private set; }

        private Signal deleteSignal = new Signal();
        private bool deleteSignalDispatched;

        /// <summary>
        /// Destroys this view and dispatches its delete signal.
        /// </summary>
        [ScriptDoc("Destroys this view and dispatches its delete signal.")]
        public void Destroy() {
            IsDestroyed = true;
            Destroy(gameObject);
            DeleteSignal.Dispatch();
            deleteSignalDispatched = true;
        }

        /// <summary>
        /// Returns this view's gameObject.
        /// </summary>
        /// <returns></returns>
        [ScriptDoc("Returns this view's gameObject.")]
        public GameObject GetGameObject() {
            return gameObject;
        }

        /// <summary>
        /// Dispatches this view delete signal if it hasn't already.
        /// </summary>
        [ScriptDoc("Dispatches this view's delete signal if it hasn't already. Make sure to call this base method when overriding.")]
        protected virtual void OnDestroy() {
            IsDestroyed = true;
            if (!deleteSignalDispatched) {
                DeleteSignal.Dispatch();
            }
        }

    }

}